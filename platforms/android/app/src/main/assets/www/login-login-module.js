(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <div class=\"positionabsolute\">\n    <img src=\"assets/imgs/appbackground.svg\" style=\"position: absolute;width: 100%;\"/>\n   </div>\n    <ion-list  class=\"marginform backgroundcolortransparent\">\n      <img src=\"assets/imgs/logo.jpeg\" class=\"imgwidth backgroundcolortransparent\"/>\n    </ion-list>\n  \n    <ion-card class=\"logincard\">\n      <div>\n    <ion-list  class=\"backgroundcolortransparent\">\n      <ion-grid>\n      <ion-row> \n        <ion-col>\n      <p class=\"colordefault marginzero fontsize13px\"><b>Sign in</b></p>\n    </ion-col>\n    </ion-row>\n  </ion-grid>\n      <form [formGroup]=\"LoginForm\" class=\"backgroundcolortransparent\">\n        <ion-list no-lines class=\"backgroundcolortransparent\">\n          <ion-grid class=\"backgroundcolortransparent\">\n            <ion-row class=\"backgroundcolortransparent\"> \n              <ion-col class=\"backgroundcolortransparent\">\n          <ion-item class=\"backgroundcolortransparent\">  \n            <ion-label position=\"floating\" class=\"colorsegment fontweightlight\" *ngIf=\"roleapp == 'doctor'\">Mobile Number</ion-label>\n            <ion-label position=\"floating\" class=\"colorsegment fontweightlight\" *ngIf=\"roleapp == 'member'\">Mid</ion-label>\n            <ion-input autocapitalize inputmode=\"number\" formControlName=\"mobile\"></ion-input>\n          </ion-item> \n          <div *ngFor=\"let error of errorMessages.mobile\">\n            <ng-container *ngIf=\"mobile.hasError(error.type) && (mobile.dirty || mobile.touched)\">\n              <small class=\"error-message\">{{error.message}}</small>\n            </ng-container>\n          </div>\n          </ion-col>\n          </ion-row>\n            <ion-row class=\"backgroundcolortransparent\"> \n              <ion-col class=\"backgroundcolortransparent\">\n          <ion-item class=\"backgroundcolortransparent\">  \n            <ion-label position=\"floating\" class=\"colorsegment fontweightlight\">Password</ion-label>\n            <ion-input type=\"password\" inputmode=\"password\" formControlName=\"password\"></ion-input>\n          </ion-item>\n          <div *ngFor=\"let error of errorMessages.password\">\n            <ng-container *ngIf=\"password.hasError(error.type) && (password.dirty || password.touched)\">\n              <small class=\"error-message\">{{error.message}}</small>\n            </ng-container>\n          </div>\n          </ion-col>\n          </ion-row>\n          </ion-grid>\n          \n        </ion-list> \n        <div class=\"textaligncenter\">\n        <ion-button  [disabled]=\"!LoginForm.valid\"  class=\"widthbutton\"  type=\"submit\" shape=\"round\" color=\"secondary\" (click)=\"submit()\" >Login </ion-button>\n      </div>\n      </form>\n    </ion-list>\n    \n    <ion-list  class=\"marginzero  registercolor textaligncenter\" *ngIf=\"roleapp == 'doctor'\">\n      <p class=\"signincolor marginzero fontsize11px\">New To Educure?<u class=\" marginzero fontsize13px \" [routerLink]=\"['/doctorregistration']\"><small class=\"fontsize13px\">Click To Create Account.</small></u></p>\n    </ion-list>\n    <ion-list  class=\"marginzero  registercolor textaligncenter\">\n      <p class=\"signincolor marginzero fontsize11px\">{{locationCoords.latitude}}{{locationCoords.longitude}}</p>\n    </ion-list>\n  </div>\n   </ion-card>\n  \n</ion-content>\n");

/***/ }),

/***/ "./src/app/api/login/auth.service.ts":
/*!*******************************************!*\
  !*** ./src/app/api/login/auth.service.ts ***!
  \*******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");



// import { OneSignal } from '@ionic-native/onesignal/ngx';
var AuthService = /** @class */ (function () {
    function AuthService(storage) {
        this.storage = storage;
        this.userid = 'null';
        this.name = 'null';
        this.phone = 'null';
        this.email = 'null';
        this.deviceid = 'null';
        this.ext1 = 'null';
        this.ext2 = 'null';
        this.ext3 = 'null';
        this.set = 'null';
    } //,private oneSignal: OneSignal
    AuthService.prototype.DataSet = function () {
        var _this = this;
        Promise.all([this.storage.get("userid"), this.storage.get("name"),
            this.storage.get("phone"), this.storage.get("email")]).then(function (values) {
            _this.userid = values[0] == undefined ? 'null' : (values[0] != '' ? values[0] : 'null');
            _this.name = values[1] == undefined ? 'null' : (values[1] != '' ? values[1] : 'null');
            _this.phone = values[2] == undefined ? 'null' : (values[2] != '' ? values[2] : 'null');
            _this.email = values[3] == undefined ? 'null' : (values[3] != '' ? values[3] : 'null');
            console.log("All Data SET userid " + _this.userid + " name " + _this.name);
        });
    };
    AuthService.prototype.DeviceSet = function () {
        this.deviceid = '123456789';
        console.log("Set Device ID device3");
        // this.oneSignal.getIds().then(identity => {
        //this.deviceid = identity.userId;
        //  this.deviceid = "device32";
        //});
    };
    Object.defineProperty(AuthService.prototype, "PrepairData", {
        set: function (status) {
            console.log("Set PrepairData ", status);
            this.DataSet();
            this.set = status;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "ReSetData", {
        set: function (status) {
            this.userid = 'null';
            this.name = 'null';
            this.phone = 'null';
            this.email = 'null';
            this.set = status;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "isDeviceid", {
        get: function () {
            if (this.deviceid !== 'null') {
                return this.deviceid;
            }
            else {
                this.DeviceSet();
                return 'No Data';
            }
        },
        set: function (status) {
            this.DeviceSet();
            console.log("Set Device ID ", status);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "isUserid", {
        get: function () {
            if (this.userid !== 'null') {
                return this.userid;
            }
            else {
                this.DataSet();
                return '5e00d0a0000ff000a00000f0';
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "isName", {
        get: function () {
            if (this.name !== 'null') {
                return this.name;
            }
            else {
                this.DataSet();
                return 'No Data';
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "isEmail", {
        get: function () {
            if (this.email !== 'null') {
                return this.email;
            }
            else {
                this.DataSet();
                return 'No Data';
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "isPhone", {
        get: function () {
            if (this.phone !== 'null') {
                return this.phone;
            }
            else {
                this.DataSet();
                return 'No Data';
            }
        },
        enumerable: true,
        configurable: true
    });
    AuthService.ctorParameters = function () { return [
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"] }
    ]; };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/api/login/login.service.ts":
/*!********************************************!*\
  !*** ./src/app/api/login/login.service.ts ***!
  \********************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _path_path_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../path/path.service */ "./src/app/api/path/path.service.ts");
/* harmony import */ var _login_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../login/auth.service */ "./src/app/api/login/auth.service.ts");







var LoginService = /** @class */ (function () {
    function LoginService(_http, _url, _auth) {
        this._http = _http;
        this._url = _url;
        this._auth = _auth;
        this._login = this._url.serverurl + 'signin';
        this._loginmember = this._url.serverurl + 'signinmember';
    }
    LoginService.prototype.getHearder = function () {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json'
        });
        this.httpOptions = { headers: headers };
    };
    LoginService.prototype.login = function (userData, role) {
        if (role == 'member') {
            var formData = new FormData();
            formData.append('mobile', userData.mobile);
            return this._http.post(this._loginmember, userData)
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
        }
        else {
            var formData = new FormData();
            formData.append('mobile', userData.mobile);
            return this._http.post(this._login, userData)
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
        }
    };
    LoginService.prototype.errorHandler = function (error) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(error);
    };
    LoginService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _path_path_service__WEBPACK_IMPORTED_MODULE_5__["PathService"] },
        { type: _login_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"] }
    ]; };
    LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _path_path_service__WEBPACK_IMPORTED_MODULE_5__["PathService"], _login_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]])
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/app/api/path/path.service.ts":
/*!******************************************!*\
  !*** ./src/app/api/path/path.service.ts ***!
  \******************************************/
/*! exports provided: PathService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PathService", function() { return PathService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PathService = /** @class */ (function () {
    function PathService() {
        this.url = 'https://cors-anywhere.herokuapp.com/http://www.planmyhealth.in/planhealthapi/api/';
    }
    Object.defineProperty(PathService.prototype, "serverurl", {
        get: function () {
            return this.url;
        },
        enumerable: true,
        configurable: true
    });
    PathService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PathService);
    return PathService;
}());



/***/ }),

/***/ "./src/app/login/login-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/login/login-routing.module.ts ***!
  \***********************************************/
/*! exports provided: LoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function() { return LoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");




var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
    }
];
var LoginPageRoutingModule = /** @class */ (function () {
    function LoginPageRoutingModule() {
    }
    LoginPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], LoginPageRoutingModule);
    return LoginPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/login/login-routing.module.ts");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");







var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/login/login.page.scss":
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".marginform {\n  margin: 20px;\n}\n\n.imgwidth {\n  width: 153px;\n}\n\n.backgroundcolortransparent {\n  background-color: transparent;\n}\n\n.colorgrey {\n  color: #C8CBCD;\n}\n\n.fontsize {\n  font-size: 11px !important;\n  text-transform: none !important;\n}\n\n.fontalignright {\n  text-align: right !important;\n}\n\nion-button {\n  --border-radius: 7px !important;\n}\n\n.widthbutton {\n  width: 189px;\n  height: 29px;\n}\n\n.signincolor {\n  color: #747474;\n}\n\n.width35px {\n  width: 35px;\n}\n\n.width35pxfloatright {\n  width: 35px;\n  float: right;\n}\n\n.item-native {\n  background-color: transparent !important;\n}\n\n.item .item-input {\n  background-color: transparent;\n}\n\nion-item {\n  --background: transparent;\n}\n\n.logincard {\n  height: 65%;\n  background-color: white;\n  margin: 28px;\n  border-radius: 13px;\n}\n\n.margintopgrid {\n  margin-top: 30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vQzpcXGlvbmljXFxrcnVzaG5hbWFpXFxOZXcgZm9sZGVyL3NyY1xcYXBwXFxsb2dpblxcbG9naW4ucGFnZS5zY3NzIiwic3JjL2FwcC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFBO0FDQ0o7O0FER0E7RUFDSSxZQUFBO0FDQUo7O0FETUE7RUFDSSw2QkFBQTtBQ0hKOztBRE1BO0VBQ0ksY0FBQTtBQ0hKOztBRE1BO0VBQ0ksMEJBQUE7RUFDQSwrQkFBQTtBQ0hKOztBRE1BO0VBQ0ksNEJBQUE7QUNISjs7QURRQTtFQUNJLCtCQUFBO0FDTEo7O0FET0E7RUFDSSxZQUFBO0VBQ0EsWUFBQTtBQ0pKOztBRE1BO0VBQ0ksY0FBQTtBQ0hKOztBRE1BO0VBQ0ksV0FBQTtBQ0hKOztBREtBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUNGSjs7QURLQTtFQUNJLHdDQUFBO0FDRko7O0FESUE7RUFDSSw2QkFBQTtBQ0RKOztBREdBO0VBQ0kseUJBQUE7QUNBSjs7QURJQTtFQUNJLFdBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQ0RKOztBREdBO0VBQ0ksZ0JBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXJnaW5mb3Jte1xyXG4gICAgbWFyZ2luOiAyMHB4O1xyXG59XHJcblxyXG5cclxuLmltZ3dpZHRoe1xyXG4gICAgd2lkdGg6IDE1M3B4O1xyXG59XHJcblxyXG4gXHJcblxyXG5cclxuLmJhY2tncm91bmRjb2xvcnRyYW5zcGFyZW50e1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbn1cclxuXHJcbi5jb2xvcmdyZXl7XHJcbiAgICBjb2xvcjogI0M4Q0JDRDtcclxufVxyXG5cclxuLmZvbnRzaXple1xyXG4gICAgZm9udC1zaXplOiAxMXB4ICFpbXBvcnRhbnQ7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogbm9uZSFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5mb250YWxpZ25yaWdodHtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcblxyXG5cclxuaW9uLWJ1dHRvbntcclxuICAgIC0tYm9yZGVyLXJhZGl1czogN3B4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLndpZHRoYnV0dG9ue1xyXG4gICAgd2lkdGg6IDE4OXB4O1xyXG4gICAgaGVpZ2h0OiAyOXB4O1xyXG59XHJcbi5zaWduaW5jb2xvcntcclxuICAgIGNvbG9yOiAgICM3NDc0NzQ7XHJcbn1cclxuXHJcbi53aWR0aDM1cHh7XHJcbiAgICB3aWR0aDogMzVweDtcclxufVxyXG4ud2lkdGgzNXB4ZmxvYXRyaWdodHtcclxuICAgIHdpZHRoOiAzNXB4O1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcblxyXG4uaXRlbS1uYXRpdmV7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG59XHJcbi5pdGVtIC5pdGVtLWlucHV0IHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG59XHJcbmlvbi1pdGVtIHtcclxuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgXHJcbn1cclxuXHJcbi5sb2dpbmNhcmR7XHJcbiAgICBoZWlnaHQ6IDY1JTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgbWFyZ2luOiAyOHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTNweDtcclxufSAgIFxyXG4ubWFyZ2ludG9wZ3JpZHtcclxuICAgIG1hcmdpbi10b3A6IDMwcHg7XHJcbn1cclxuIiwiLm1hcmdpbmZvcm0ge1xuICBtYXJnaW46IDIwcHg7XG59XG5cbi5pbWd3aWR0aCB7XG4gIHdpZHRoOiAxNTNweDtcbn1cblxuLmJhY2tncm91bmRjb2xvcnRyYW5zcGFyZW50IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG5cbi5jb2xvcmdyZXkge1xuICBjb2xvcjogI0M4Q0JDRDtcbn1cblxuLmZvbnRzaXplIHtcbiAgZm9udC1zaXplOiAxMXB4ICFpbXBvcnRhbnQ7XG4gIHRleHQtdHJhbnNmb3JtOiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5mb250YWxpZ25yaWdodCB7XG4gIHRleHQtYWxpZ246IHJpZ2h0ICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1idXR0b24ge1xuICAtLWJvcmRlci1yYWRpdXM6IDdweCAhaW1wb3J0YW50O1xufVxuXG4ud2lkdGhidXR0b24ge1xuICB3aWR0aDogMTg5cHg7XG4gIGhlaWdodDogMjlweDtcbn1cblxuLnNpZ25pbmNvbG9yIHtcbiAgY29sb3I6ICM3NDc0NzQ7XG59XG5cbi53aWR0aDM1cHgge1xuICB3aWR0aDogMzVweDtcbn1cblxuLndpZHRoMzVweGZsb2F0cmlnaHQge1xuICB3aWR0aDogMzVweDtcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuXG4uaXRlbS1uYXRpdmUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xufVxuXG4uaXRlbSAuaXRlbS1pbnB1dCB7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xufVxuXG5pb24taXRlbSB7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG5cbi5sb2dpbmNhcmQge1xuICBoZWlnaHQ6IDY1JTtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIG1hcmdpbjogMjhweDtcbiAgYm9yZGVyLXJhZGl1czogMTNweDtcbn1cblxuLm1hcmdpbnRvcGdyaWQge1xuICBtYXJnaW4tdG9wOiAzMHB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/login/login.page.ts":
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _api_login_login_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../api/login/login.service */ "./src/app/api/login/login.service.ts");
/* harmony import */ var _api_login_auth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../api/login/auth.service */ "./src/app/api/login/auth.service.ts");
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "./node_modules/@ionic-native/android-permissions/ngx/index.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/location-accuracy/ngx */ "./node_modules/@ionic-native/location-accuracy/ngx/index.js");











var LoginPage = /** @class */ (function () {
    function LoginPage(formBuilder, toastCtrl, router, storage, _login, _auth, menuCtrl, loadingCtrl, androidPermissions, geolocation, locationAccuracy) {
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.storage = storage;
        this._login = _login;
        this._auth = _auth;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.androidPermissions = androidPermissions;
        this.geolocation = geolocation;
        this.locationAccuracy = locationAccuracy;
        this.LoginForm = this.formBuilder.group({
            mobile: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(10)]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
            ]
        });
        this.errorMessages = {
            mobile: [
                { type: 'required', message: 'Mobile Number is required' },
                { type: 'maxlength', message: 'Mobile Number cant be longer than 10 characters' }
            ],
            password: [
                { type: 'required', message: 'Password is required' }
            ]
        };
        this.locationCoords = {
            latitude: "",
            longitude: "",
            accuracy: ""
        };
    }
    LoginPage.prototype.ngOnInit = function () {
    };
    LoginPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.menuCtrl.enable(false);
        this.checkGPSPermission();
        this.storage.get('roleapp').then(function (value) {
            _this.roleapp = value;
        });
    };
    Object.defineProperty(LoginPage.prototype, "mobile", {
        get: function () {
            return this.LoginForm.get("mobile");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LoginPage.prototype, "password", {
        get: function () {
            return this.LoginForm.get("password");
        },
        enumerable: true,
        configurable: true
    });
    LoginPage.prototype.submit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        console.log("hi");
                        this._login.login(this.LoginForm.value, this.roleapp)
                            .subscribe(function (data) {
                            loading.dismiss();
                            if (data['success'] == '1') {
                                if (_this.roleapp == 'member') {
                                    _this.storage.set('membercode', _this.LoginForm.value.mobile);
                                    _this.router.navigate(['/memberdashboard']);
                                }
                                else {
                                    _this.storage.set('provider_no', data['data'][0]['provider_no']);
                                    _this.storage.set('provider_code', data['data'][0]['provider_code']);
                                    _this.storage.set('provider_name', data['data'][0]['provider_name']);
                                    _this.storage.set('speciality_type', data['data'][0]['speciality_type']);
                                    _this.storage.set('speciality_code', data['data'][0]['speciality_code']);
                                    _this.storage.set('category_code', data['data'][0]['category_code']);
                                    _this.storage.set('mobile_no', data['data'][0]['mobile_no']);
                                    _this.storage.set('provider_name', data['data'][0]['provider_name']);
                                    _this.storage.set('e_mail_id', data['data'][0]['e_mail_id']);
                                    _this.router.navigate(['/home']);
                                }
                            }
                            else {
                                alert(data['msg']);
                                _this.LoginForm.reset();
                            }
                        }, function (error) { _this.errorMsg = error.success; });
                        return [2 /*return*/];
                }
            });
        });
    };
    //========================================= Geo Location ============================================
    //Check if application having GPS access permission  
    LoginPage.prototype.checkGPSPermission = function () {
        var _this = this;
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(function (result) {
            if (result.hasPermission) { //If having permission show 'Turn On GPS' dialogue
                _this.askToTurnOnGPS();
            }
            else { //If not having permission ask for permission
                _this.requestGPSPermission();
            }
        }, function (err) { alert(err); });
    };
    LoginPage.prototype.requestGPSPermission = function () {
        var _this = this;
        this.locationAccuracy.canRequest().then(function (canRequest) {
            if (canRequest) {
                console.log("4");
            }
            else { //Show 'GPS Permission Request' dialogue
                _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
                    .then(function () {
                    _this.askToTurnOnGPS();
                }, function (error) {
                    alert('requestPermission Error requesting location permissions ' + error);
                });
            }
        });
    };
    LoginPage.prototype.askToTurnOnGPS = function () {
        var _this = this;
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(function () {
            _this.getLocationCoordinates();
        }, function (error) { return alert('Error requesting location permissions ' + JSON.stringify(error)); });
    };
    // Methos to get device accurate coordinates using device GPS
    LoginPage.prototype.getLocationCoordinates = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.locationCoords.latitude = resp.coords.latitude;
            _this.locationCoords.longitude = resp.coords.longitude;
            _this.locationCoords.accuracy = resp.coords.accuracy;
            console.log(_this.locationCoords);
            //this.storage.set('location', JSON.stringify(this.locationCoords));
            _this.locationvalue = JSON.stringify(_this.locationCoords);
            console.log(_this.locationCoords.latitude);
        }).catch(function (error) {
            alert('Error getting location' + error);
        });
    };
    LoginPage.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
        { type: _api_login_login_service__WEBPACK_IMPORTED_MODULE_6__["LoginService"] },
        { type: _api_login_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
        { type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_8__["AndroidPermissions"] },
        { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__["Geolocation"] },
        { type: _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_10__["LocationAccuracy"] }
    ]; };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.page.scss */ "./src/app/login/login.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
            _api_login_login_service__WEBPACK_IMPORTED_MODULE_6__["LoginService"], _api_login_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"], _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_8__["AndroidPermissions"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__["Geolocation"], _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_10__["LocationAccuracy"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ })

}]);
//# sourceMappingURL=login-login-module.js.map