(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["role-role-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/role/role.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/role/role.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <div class=\"positionabsolute\">\n    <img src=\"assets/imgs/appbackground.svg\" style=\"position: absolute;width: 100%;\"/>\n   </div>\n  <div class=\"margintop5px\">\n  <ion-grid>\n    <ion-row>\n        <p class=\"textaligncenter centerblock\">Are You?</p>\n    </ion-row>\n  </ion-grid>\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"6\" class=\"textaligncenter\" (click)=\"roleapp('member')\">\n        <img src=\"assets/imgs/member.png\" />\n      </ion-col>\n      <ion-col size=\"6\" class=\"textaligncenter\" (click)=\"roleapp('doctor')\">\n        <img src=\"assets/imgs/doctor1.jpg\" />\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n<ion-grid>\n  <ion-row>\n    <ion-col size=\"6\" class=\"textaligncenter\" (click)=\"roleapp('member')\">\n      <p><u>Member</u></p>\n    </ion-col>\n    <ion-col size=\"6\" class=\"textaligncenter\" (click)=\"roleapp('doctor')\">\n      <p><u>Doctor</u></p>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n</div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/role/role-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/role/role-routing.module.ts ***!
  \*********************************************/
/*! exports provided: RolePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RolePageRoutingModule", function() { return RolePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _role_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./role.page */ "./src/app/role/role.page.ts");




var routes = [
    {
        path: '',
        component: _role_page__WEBPACK_IMPORTED_MODULE_3__["RolePage"]
    }
];
var RolePageRoutingModule = /** @class */ (function () {
    function RolePageRoutingModule() {
    }
    RolePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], RolePageRoutingModule);
    return RolePageRoutingModule;
}());



/***/ }),

/***/ "./src/app/role/role.module.ts":
/*!*************************************!*\
  !*** ./src/app/role/role.module.ts ***!
  \*************************************/
/*! exports provided: RolePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RolePageModule", function() { return RolePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _role_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./role-routing.module */ "./src/app/role/role-routing.module.ts");
/* harmony import */ var _role_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./role.page */ "./src/app/role/role.page.ts");







var RolePageModule = /** @class */ (function () {
    function RolePageModule() {
    }
    RolePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _role_routing_module__WEBPACK_IMPORTED_MODULE_5__["RolePageRoutingModule"]
            ],
            declarations: [_role_page__WEBPACK_IMPORTED_MODULE_6__["RolePage"]]
        })
    ], RolePageModule);
    return RolePageModule;
}());



/***/ }),

/***/ "./src/app/role/role.page.scss":
/*!*************************************!*\
  !*** ./src/app/role/role.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".margintop5px {\n  margin-top: 38%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcm9sZS9DOlxcaW9uaWNcXGtydXNobmFtYWlcXE5ldyBmb2xkZXIvc3JjXFxhcHBcXHJvbGVcXHJvbGUucGFnZS5zY3NzIiwic3JjL2FwcC9yb2xlL3JvbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvcm9sZS9yb2xlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXJnaW50b3A1cHh7XHJcbiAgICBtYXJnaW4tdG9wOiAzOCU7XHJcbn0iLCIubWFyZ2ludG9wNXB4IHtcbiAgbWFyZ2luLXRvcDogMzglO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/role/role.page.ts":
/*!***********************************!*\
  !*** ./src/app/role/role.page.ts ***!
  \***********************************/
/*! exports provided: RolePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RolePage", function() { return RolePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var RolePage = /** @class */ (function () {
    function RolePage(storage, router) {
        this.storage = storage;
        this.router = router;
    }
    RolePage.prototype.ngOnInit = function () {
    };
    RolePage.prototype.roleapp = function (value) {
        this.storage.set('roleapp', value);
        this.router.navigate(['/login']);
    };
    RolePage.ctorParameters = function () { return [
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    RolePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-role',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./role.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/role/role.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./role.page.scss */ "./src/app/role/role.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], RolePage);
    return RolePage;
}());



/***/ })

}]);
//# sourceMappingURL=role-role-module.js.map