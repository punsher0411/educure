(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["testingform-testingform-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/testingform/testingform.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/testingform/testingform.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>testingform</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]=\"EyeForm\" >\n  <ion-item *ngFor=\"let question of questionlistdata\">\n    <ion-label  position=\"stacked\">Q.) {{question.que_detail}}:-</ion-label>\n    <div *ngFor=\"let answer of answerlistdata\">\n    <ion-radio-group  *ngIf=\"question.que_code == answer.que_code\" formControlName=\"question.que_code\">\n    <ion-grid>\n      <ion-row>\n        <ion-radio  color=\"tertiary\" value=\"Yes\"></ion-radio><span class=\"fontsize12px\">{{answer.ans_code}}</span>\n      </ion-row>\n    </ion-grid>\n    </ion-radio-group>\n  </div>\n    </ion-item>\n  </form>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/api/path/path.service.ts":
/*!******************************************!*\
  !*** ./src/app/api/path/path.service.ts ***!
  \******************************************/
/*! exports provided: PathService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PathService", function() { return PathService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PathService = /** @class */ (function () {
    function PathService() {
        this.url = 'https://cors-anywhere.herokuapp.com/http://www.planmyhealth.in/planhealthapi/api/';
    }
    Object.defineProperty(PathService.prototype, "serverurl", {
        get: function () {
            return this.url;
        },
        enumerable: true,
        configurable: true
    });
    PathService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PathService);
    return PathService;
}());



/***/ }),

/***/ "./src/app/testingform/testingform-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/testingform/testingform-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: TestingformPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestingformPageRoutingModule", function() { return TestingformPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _testingform_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./testingform.page */ "./src/app/testingform/testingform.page.ts");




var routes = [
    {
        path: '',
        component: _testingform_page__WEBPACK_IMPORTED_MODULE_3__["TestingformPage"]
    }
];
var TestingformPageRoutingModule = /** @class */ (function () {
    function TestingformPageRoutingModule() {
    }
    TestingformPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], TestingformPageRoutingModule);
    return TestingformPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/testingform/testingform.module.ts":
/*!***************************************************!*\
  !*** ./src/app/testingform/testingform.module.ts ***!
  \***************************************************/
/*! exports provided: TestingformPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestingformPageModule", function() { return TestingformPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _testingform_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./testingform-routing.module */ "./src/app/testingform/testingform-routing.module.ts");
/* harmony import */ var _testingform_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./testingform.page */ "./src/app/testingform/testingform.page.ts");







var TestingformPageModule = /** @class */ (function () {
    function TestingformPageModule() {
    }
    TestingformPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _testingform_routing_module__WEBPACK_IMPORTED_MODULE_5__["TestingformPageRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
            ],
            declarations: [_testingform_page__WEBPACK_IMPORTED_MODULE_6__["TestingformPage"]]
        })
    ], TestingformPageModule);
    return TestingformPageModule;
}());



/***/ }),

/***/ "./src/app/testingform/testingform.page.scss":
/*!***************************************************!*\
  !*** ./src/app/testingform/testingform.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Rlc3Rpbmdmb3JtL3Rlc3Rpbmdmb3JtLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/testingform/testingform.page.ts":
/*!*************************************************!*\
  !*** ./src/app/testingform/testingform.page.ts ***!
  \*************************************************/
/*! exports provided: TestingformPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestingformPage", function() { return TestingformPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _api_medicalcamp_medicalcamp_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../api/medicalcamp/medicalcamp.service */ "./src/app/api/medicalcamp/medicalcamp.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var TestingformPage = /** @class */ (function () {
    function TestingformPage(_medicalcamp, formBuilder, toastCtrl, router, storage, menuCtrl, loadingCtrl, _Activatedroute) {
        this._medicalcamp = _medicalcamp;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.storage = storage;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this._Activatedroute = _Activatedroute;
        this.EyeForm = _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormGroup"];
        this.student_id = this._Activatedroute.snapshot.paramMap.get('id');
        console.log(this.student_id);
        this.specilitycode = this._Activatedroute.snapshot.paramMap.get('specilitycode');
        this.questionanswerlist();
        console.log(this.questionlistdata);
    }
    TestingformPage.prototype.ngOnInit = function () {
    };
    TestingformPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(true);
        //this.MemberForm.reset();
    };
    TestingformPage.prototype.questionanswerlist = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this._medicalcamp.questionanswerlist(this.student_id).subscribe(function (data) {
                            loading.dismiss();
                            console.log(data);
                            if (data['success'] == '1') {
                                _this.questionlistdata = data['data']['questions'];
                                _this.answerlistdata = data['data']['answers'];
                                var group_1 = {};
                                _this.questionlistdata.forEach(function (question) {
                                    group_1[question.que_code] = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('');
                                });
                                _this.EyeForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormGroup"](group_1);
                            }
                            else {
                            }
                        }, function (error) { _this.errorMsg = error.success; });
                        return [2 /*return*/];
                }
            });
        });
    };
    TestingformPage.ctorParameters = function () { return [
        { type: _api_medicalcamp_medicalcamp_service__WEBPACK_IMPORTED_MODULE_3__["MedicalcampService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] }
    ]; };
    TestingformPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-testingform',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./testingform.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/testingform/testingform.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./testingform.page.scss */ "./src/app/testingform/testingform.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_api_medicalcamp_medicalcamp_service__WEBPACK_IMPORTED_MODULE_3__["MedicalcampService"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]])
    ], TestingformPage);
    return TestingformPage;
}());



/***/ })

}]);
//# sourceMappingURL=testingform-testingform-module.js.map