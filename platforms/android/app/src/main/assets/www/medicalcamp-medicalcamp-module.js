(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["medicalcamp-medicalcamp-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/medicalcamp/medicalcamp.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/medicalcamp/medicalcamp.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\n      <p ><b>Medical Camp</b></p>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div>\n    <ion-list padding>\n      <form [formGroup]=\"MemberForm\" >\n      <ion-item lines=\"none\">\n        <ion-select placeholder=\"Select Entity\" formControlName=\"entityid\" class=\"maxwidth\" #cat (ionChange)=\"getSubcat($event)\"\n          color=\"tertiary\">\n          <ion-select-option *ngFor=\"let school of schoollistdata\" value=\"{{school.org_id}},{{school.from}},{{school.consultant_id}},{{school.event_id}}\">{{school.organization_name}}\n          </ion-select-option>\n        </ion-select>\n        <div *ngFor=\"let error of errorMessages.entityid\">\n          <ng-container *ngIf=\"entityid.hasError(error.type) && (entityid.dirty || entityid.touched)\">\n            <small class=\"error-message\">{{error.message}}</small>\n          </ng-container>\n        </div>\n      </ion-item>\n      <div *ngIf=\"studentliststatus\">\n      <ion-item lines=\"none\">\n        <!-- <ion-select placeholder=\"Select Member Id\" class=\"maxwidth\" formControlName=\"memberid\" \n          color=\"tertiary\"   (ionChange)=\"studentcat($event)\">\n          <ion-select-option *ngFor=\"let student of studentlistdata\" value=\"{{student.mid}},{{student.fname}},{{student.lname}},{{student.class}},{{student.roll_no}},{{student.division}}\">{{student.fname}}{{student.lname}},{{student.mid}}\n          </ion-select-option>\n        </ion-select>\n        <div *ngFor=\"let error of errorMessages.memberid\">\n          <ng-container *ngIf=\"memberid.hasError(error.type) && (memberid.dirty || memberid.touched)\">\n            <small class=\"error-message\">{{error.message}}</small>\n          </ng-container>\n        </div> -->\n        <ion-searchbar (ionInput)=\"FilterJsonData($event)\"></ion-searchbar>\n        \n      </ion-item>\n      <ion-item (click)=\"studentcat(student)\" *ngFor=\"let student of studentlistdata | slice:0:5;\" >{{student.mid}}|{{student.fname}} {{student.lname}}|{{student.roll_no}}|{{student.class}}|{{student.division}}</ion-item>\n      <ion-item>\n          You Selected : {{studentfirstname}} {{studentlastname}}\n      </ion-item>\n      <ion-item lines=\"none\" *ngIf=\"specilitycode == 'GP'\">\n        <ion-button [disabled]=\"!submitbutton\" class=\"centerblock\" type=\"submit\" color=\"tertiary\" Expand=\"block\"\n          size=\"default\" [routerLink]=\"['/physician',studentid,consulentid,studentfirstname,studentlastname,event_id,studentclass,studentrollno,studentdiv]\">Submit</ion-button>\n        \n      </ion-item>\n      <ion-item lines=\"none\" *ngIf=\"specilitycode == 'OPTH'\">\n        <ion-button [disabled]=\"!submitbutton\" class=\"centerblock\" type=\"submit\" color=\"tertiary\" Expand=\"block\"\n        size=\"default\" [routerLink]=\"['/eyeform',studentid,consulentid,studentfirstname,studentlastname,event_id,studentclass,studentrollno,studentdiv]\">Submit</ion-button>\n      </ion-item>\n      <ion-item lines=\"none\" *ngIf=\"specilitycode == 'DEN'\">\n        <ion-button [disabled]=\"!submitbutton\" class=\"centerblock\" type=\"submit\" color=\"tertiary\" Expand=\"block\"\n        size=\"default\" [routerLink]=\"['/dentalform',studentid,consulentid,studentfirstname,studentlastname,event_id,studentclass,studentrollno,studentdiv]\">Submit</ion-button>\n      </ion-item>\n      <ion-item lines=\"none\" *ngIf=\"specilitycode == 'DIET'\">\n        <ion-button [disabled]=\"!submitbutton\" class=\"centerblock\" type=\"submit\" color=\"tertiary\" Expand=\"block\"\n        size=\"default\" [routerLink]=\"['/dietform',studentid,consulentid,studentfirstname,studentlastname,event_id,studentclass,studentrollno,studentdiv]\">Submit</ion-button>\n      </ion-item>\n      <ion-item lines=\"none\" *ngIf=\"specilitycode == 'PSYCH' || specilitycode == 'HOSP'\">\n        <ion-button [disabled]=\"!submitbutton\" class=\"centerblock\" type=\"submit\" color=\"tertiary\" Expand=\"block\"\n        size=\"default\" [routerLink]=\"['/mentalhealthform',studentid,consulentid,studentfirstname,studentlastname,event_id,studentclass,studentrollno,studentdiv]\">Submit</ion-button>\n      </ion-item>\n      </div>\n    </form>\n      </ion-list>\n      </div>\n      <div class=\"couponimage\" *ngIf=\"schoolliststatus\">\n        <ion-grid>\n          <ion-row>\n            <ion-col>\n              <p class=\"colorwhite textaligncenter headercopupon\"><br>No School Found</p>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n</ion-content>\n<!-- <ion-footer>\n  <ion-toolbar>\n    <div class=\"textaligncenter\" >\n    <ion-button  color=\"tertiary\" class=\"fontsize11px texttransform\">Add Member</ion-button>\n    </div>\n  </ion-toolbar>\n</ion-footer> -->");

/***/ }),

/***/ "./src/app/api/path/path.service.ts":
/*!******************************************!*\
  !*** ./src/app/api/path/path.service.ts ***!
  \******************************************/
/*! exports provided: PathService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PathService", function() { return PathService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PathService = /** @class */ (function () {
    function PathService() {
        this.url = 'https://cors-anywhere.herokuapp.com/http://www.planmyhealth.in/planhealthapi/api/';
    }
    Object.defineProperty(PathService.prototype, "serverurl", {
        get: function () {
            return this.url;
        },
        enumerable: true,
        configurable: true
    });
    PathService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PathService);
    return PathService;
}());



/***/ }),

/***/ "./src/app/medicalcamp/medicalcamp-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/medicalcamp/medicalcamp-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: MedicalcampPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MedicalcampPageRoutingModule", function() { return MedicalcampPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _medicalcamp_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./medicalcamp.page */ "./src/app/medicalcamp/medicalcamp.page.ts");




var routes = [
    {
        path: '',
        component: _medicalcamp_page__WEBPACK_IMPORTED_MODULE_3__["MedicalcampPage"]
    }
];
var MedicalcampPageRoutingModule = /** @class */ (function () {
    function MedicalcampPageRoutingModule() {
    }
    MedicalcampPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], MedicalcampPageRoutingModule);
    return MedicalcampPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/medicalcamp/medicalcamp.module.ts":
/*!***************************************************!*\
  !*** ./src/app/medicalcamp/medicalcamp.module.ts ***!
  \***************************************************/
/*! exports provided: MedicalcampPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MedicalcampPageModule", function() { return MedicalcampPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _medicalcamp_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./medicalcamp-routing.module */ "./src/app/medicalcamp/medicalcamp-routing.module.ts");
/* harmony import */ var _medicalcamp_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./medicalcamp.page */ "./src/app/medicalcamp/medicalcamp.page.ts");







var MedicalcampPageModule = /** @class */ (function () {
    function MedicalcampPageModule() {
    }
    MedicalcampPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _medicalcamp_routing_module__WEBPACK_IMPORTED_MODULE_5__["MedicalcampPageRoutingModule"]
            ],
            declarations: [_medicalcamp_page__WEBPACK_IMPORTED_MODULE_6__["MedicalcampPage"]]
        })
    ], MedicalcampPageModule);
    return MedicalcampPageModule;
}());



/***/ }),

/***/ "./src/app/medicalcamp/medicalcamp.page.scss":
/*!***************************************************!*\
  !*** ./src/app/medicalcamp/medicalcamp.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".maxwidth {\n  max-width: 100% !important;\n  margin: auto !important;\n}\n\n.invalid-feedback {\n  color: red !important;\n  font-weight: bold;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWVkaWNhbGNhbXAvQzpcXGlvbmljXFxrcnVzaG5hbWFpXFxOZXcgZm9sZGVyL3NyY1xcYXBwXFxtZWRpY2FsY2FtcFxcbWVkaWNhbGNhbXAucGFnZS5zY3NzIiwic3JjL2FwcC9tZWRpY2FsY2FtcC9tZWRpY2FsY2FtcC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSwwQkFBQTtFQUNBLHVCQUFBO0FDQ0o7O0FEQ0E7RUFDSSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNFSiIsImZpbGUiOiJzcmMvYXBwL21lZGljYWxjYW1wL21lZGljYWxjYW1wLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXh3aWR0aHtcclxuICAgIG1heC13aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG4gICAgbWFyZ2luOiBhdXRvICFpbXBvcnRhbnQ7XHJcbn1cclxuLmludmFsaWQtZmVlZGJhY2t7XHJcbiAgICBjb2xvcjpyZWQgIWltcG9ydGFudDsgXHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufSIsIi5tYXh3aWR0aCB7XG4gIG1heC13aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBtYXJnaW46IGF1dG8gIWltcG9ydGFudDtcbn1cblxuLmludmFsaWQtZmVlZGJhY2sge1xuICBjb2xvcjogcmVkICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59Il19 */");

/***/ }),

/***/ "./src/app/medicalcamp/medicalcamp.page.ts":
/*!*************************************************!*\
  !*** ./src/app/medicalcamp/medicalcamp.page.ts ***!
  \*************************************************/
/*! exports provided: MedicalcampPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MedicalcampPage", function() { return MedicalcampPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _api_medicalcamp_medicalcamp_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../api/medicalcamp/medicalcamp.service */ "./src/app/api/medicalcamp/medicalcamp.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");







var MedicalcampPage = /** @class */ (function () {
    function MedicalcampPage(_medicalcamp, formBuilder, toastCtrl, router, storage, menuCtrl, loadingCtrl) {
        this._medicalcamp = _medicalcamp;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.storage = storage;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.MemberForm = this.formBuilder.group({
            entityid: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]],
            memberid: [''
            ]
        });
        this.errorMessages = {
            entityid: [
                { type: 'required', message: 'Entity Id is required' }
            ],
            memberid: [
                { type: 'required', message: 'Member Id is required' }
            ]
        };
        this.inislizejsondata();
    }
    MedicalcampPage.prototype.ngOnInit = function () {
    };
    MedicalcampPage.prototype.FilterJsonData = function (ev) {
        var val = ev.target.value;
        if (val && val.trim() != '') {
            //this.schoollistdatavalue();
            this.studentlistdata = this.studentlistdata.filter(function (item) {
                return (item.fname.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.mid.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    MedicalcampPage.prototype.inislizejsondata = function () {
        this.jsonData = [
            {
                "id": "123",
                "name": "abc",
            },
            {
                "id": "456",
                "name": "test",
            },
            {
                "id": "789",
                "name": "example",
            },
            {
                "id": "021",
                "name": "testing",
            },
        ];
    };
    MedicalcampPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(true);
        this.MemberForm.reset();
        this.schoollist();
        this.specilatycode();
    };
    Object.defineProperty(MedicalcampPage.prototype, "entityid", {
        get: function () {
            return this.MemberForm.get("entityid");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MedicalcampPage.prototype, "memberid", {
        get: function () {
            return this.MemberForm.get("memberid");
        },
        enumerable: true,
        configurable: true
    });
    MedicalcampPage.prototype.specilatycode = function () {
        var _this = this;
        this.storage.get('speciality_code').then(function (val) {
            _this.specilitycode = val;
        });
    };
    MedicalcampPage.prototype.schoollistdatavalue = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading_1, nameArr;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.getschoolcode != '')) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Please wait...',
                                mode: "ios"
                            })];
                    case 1:
                        loading_1 = _a.sent();
                        return [4 /*yield*/, loading_1.present()];
                    case 2:
                        _a.sent();
                        nameArr = this.getschoolcode.split(',');
                        this.consulentid = nameArr[2];
                        this.event_id = nameArr[3];
                        this.storage.get('speciality_code').then(function (val) {
                            _this.specilitycode = val;
                            _this._medicalcamp.studentlist(_this.getschoolcode, _this.specilitycode).subscribe(function (data) {
                                loading_1.dismiss();
                                if (data['success'] == '1') {
                                    _this.studentlistdata = data['data']['schoollist'];
                                    _this.studentliststatus = false;
                                    _this.schoolliststatus = false;
                                }
                                else {
                                    _this.schoolliststatus = true;
                                    _this.studentliststatus = false;
                                }
                            }, function (error) { _this.errorMsg = error.success; });
                        });
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MedicalcampPage.prototype.getSubcat = function ($event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading_2, nameArr;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.getschoolcode = $event.target.value;
                        if (!(this.getschoolcode != '')) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Please wait...',
                                mode: "ios"
                            })];
                    case 1:
                        loading_2 = _a.sent();
                        return [4 /*yield*/, loading_2.present()];
                    case 2:
                        _a.sent();
                        nameArr = this.getschoolcode.split(',');
                        this.consulentid = nameArr[2];
                        this.event_id = nameArr[3];
                        this.storage.get('speciality_code').then(function (val) {
                            _this.specilitycode = val;
                            _this._medicalcamp.studentlist(_this.getschoolcode, _this.specilitycode).subscribe(function (data) {
                                loading_2.dismiss();
                                if (data['success'] == '1') {
                                    _this.studentlistdata = data['data']['schoollist'];
                                    _this.schoolliststatus = false;
                                    _this.studentliststatus = true;
                                }
                                else {
                                    _this.schoolliststatus = true;
                                    _this.studentliststatus = false;
                                }
                            }, function (error) { _this.errorMsg = error.success; });
                        });
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MedicalcampPage.prototype.studentcat = function (student) {
        this.studentid = student.mid;
        this.studentfirstname = student.fname;
        this.studentlastname = student.lname;
        this.studentclass = student.class;
        this.studentrollno = student.roll_no;
        this.studentdiv = student.division;
        this.submitbutton = true;
    };
    MedicalcampPage.prototype.schoollist = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.storage.get('provider_no').then(function (val) {
                            _this._medicalcamp.schoollist(val).subscribe(function (data) {
                                loading.dismiss();
                                if (data['success'] == '1') {
                                    _this.schoollistdata = data['data']['schoollist'];
                                    _this.schoolliststatus = true;
                                }
                                else {
                                    _this.schoolliststatus = true;
                                }
                            }, function (error) { _this.errorMsg = error.success; });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    MedicalcampPage.ctorParameters = function () { return [
        { type: _api_medicalcamp_medicalcamp_service__WEBPACK_IMPORTED_MODULE_4__["MedicalcampService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] }
    ]; };
    MedicalcampPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-medicalcamp',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./medicalcamp.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/medicalcamp/medicalcamp.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./medicalcamp.page.scss */ "./src/app/medicalcamp/medicalcamp.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_api_medicalcamp_medicalcamp_service__WEBPACK_IMPORTED_MODULE_4__["MedicalcampService"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"]])
    ], MedicalcampPage);
    return MedicalcampPage;
}());



/***/ })

}]);
//# sourceMappingURL=medicalcamp-medicalcamp-module.js.map