(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["planslist-planslist-module"],{

/***/ "./New folder/src/app/api/path/path.service.ts":
/*!*****************************************************!*\
  !*** ./New folder/src/app/api/path/path.service.ts ***!
  \*****************************************************/
/*! exports provided: PathService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PathService", function() { return PathService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PathService = /** @class */ (function () {
    function PathService() {
        this.url = 'https://cors-anywhere.herokuapp.com/http://www.planmyhealth.in/planhealthapi/api/';
        this.productimage = 'https://www.planmyhealth.in/planhealthapi/assets/images/';
    }
    Object.defineProperty(PathService.prototype, "serverurl", {
        get: function () {
            return this.url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PathService.prototype, "productimageurl", {
        get: function () {
            return this.productimage;
        },
        enumerable: true,
        configurable: true
    });
    PathService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PathService);
    return PathService;
}());



/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/planslist/planslist.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/planslist/planslist.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/memberdashboard\"></ion-back-button>\n      <p ><b>Plans</b></p>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"margintop10per\">\n   \n  <ion-card class=\"boxshadowcard margintop10per cardheight cardbackground\" *ngFor=\"let p of planlistdata\">\n    <ion-row >\n      <ion-col  size=\"5\">\n        <img src=\"{{p.pa_plan_photo == undefined ? 'assets/imgs/imageplan.jpg' :(p.pa_plan_photo != '' ? producturl+p.pa_plan_photo:'assets/imgs/imageplan.jpg')}}\" class=\"widthimage\">\n      </ion-col>\n      <ion-col  size=\"7\" >\n        <p class=\"marginzero colorbrown colorgrey fontsize12px textaligncenter \">{{p.pa_plan_name}}</p>\n        <p class=\"marginzero colorbrown colorgrey fontsize11px textaligncenter \">Rs{{p.pa_plan_price}}</p>\n        <p class=\"marginzero colorbrown colorgrey fontsize9px textaligncenter \" (click)=\"viewmore(p.pa_id)\">View More</p>\n        <div class=\"centerblock textaligncenter\">\n        <ion-button   color=\"tertiary\" \n                      size=\"small\" (click)=\"submit()\">Buy Now</ion-button>\n                    </div>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n  <ion-card class=\"paddingzero\" *ngIf=\"tpskelton\">\n    <ion-row >\n      <ion-col  size=\"5\">\n        <ion-skeleton-text  animated></ion-skeleton-text>\n        </ion-col>\n        <ion-col  size=\"7\" >\n          <p class=\"marginzero colorbrown colorgrey fontsize12px textaligncenter \"><ion-skeleton-text  animated></ion-skeleton-text></p>\n        <p class=\"marginzero colorbrown colorgrey fontsize11px textaligncenter \"><ion-skeleton-text  animated></ion-skeleton-text></p>\n        <p class=\"marginzero colorbrown colorgrey fontsize9px textaligncenter \" ><ion-skeleton-text  animated></ion-skeleton-text></p>\n        </ion-col>\n        </ion-row>\n  </ion-card>\n\n</div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/api/path/path.service.ts":
/*!******************************************!*\
  !*** ./src/app/api/path/path.service.ts ***!
  \******************************************/
/*! exports provided: PathService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PathService", function() { return PathService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PathService = /** @class */ (function () {
    function PathService() {
        this.url = 'https://cors-anywhere.herokuapp.com/http://www.planmyhealth.in/planhealthapi/api/';
    }
    Object.defineProperty(PathService.prototype, "serverurl", {
        get: function () {
            return this.url;
        },
        enumerable: true,
        configurable: true
    });
    PathService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PathService);
    return PathService;
}());



/***/ }),

/***/ "./src/app/planslist/planslist-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/planslist/planslist-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: PlanslistPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlanslistPageRoutingModule", function() { return PlanslistPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _planslist_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./planslist.page */ "./src/app/planslist/planslist.page.ts");




var routes = [
    {
        path: '',
        component: _planslist_page__WEBPACK_IMPORTED_MODULE_3__["PlanslistPage"]
    }
];
var PlanslistPageRoutingModule = /** @class */ (function () {
    function PlanslistPageRoutingModule() {
    }
    PlanslistPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], PlanslistPageRoutingModule);
    return PlanslistPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/planslist/planslist.module.ts":
/*!***********************************************!*\
  !*** ./src/app/planslist/planslist.module.ts ***!
  \***********************************************/
/*! exports provided: PlanslistPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlanslistPageModule", function() { return PlanslistPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _planslist_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./planslist-routing.module */ "./src/app/planslist/planslist-routing.module.ts");
/* harmony import */ var _planslist_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./planslist.page */ "./src/app/planslist/planslist.page.ts");







var PlanslistPageModule = /** @class */ (function () {
    function PlanslistPageModule() {
    }
    PlanslistPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _planslist_routing_module__WEBPACK_IMPORTED_MODULE_5__["PlanslistPageRoutingModule"]
            ],
            declarations: [_planslist_page__WEBPACK_IMPORTED_MODULE_6__["PlanslistPage"]]
        })
    ], PlanslistPageModule);
    return PlanslistPageModule;
}());



/***/ }),

/***/ "./src/app/planslist/planslist.page.scss":
/*!***********************************************!*\
  !*** ./src/app/planslist/planslist.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".boxshadowcard {\n  box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.2);\n}\n\n.margintop10per {\n  margin-top: 15px;\n}\n\nion-content {\n  --ion-background-color: #faf4f4;\n}\n\n.cardbackground {\n  background-color: #fff;\n  border-radius: 8px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGxhbnNsaXN0L0M6XFxpb25pY1xca3J1c2huYW1haVxcTmV3IGZvbGRlci9zcmNcXGFwcFxccGxhbnNsaXN0XFxwbGFuc2xpc3QucGFnZS5zY3NzIiwic3JjL2FwcC9wbGFuc2xpc3QvcGxhbnNsaXN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDBDQUFBO0FDQ0o7O0FEQ0E7RUFDSSxnQkFBQTtBQ0VKOztBRENBO0VBQ0ksK0JBQUE7QUNFSjs7QURDQTtFQUNJLHNCQUFBO0VBQ0osa0JBQUE7QUNFQSIsImZpbGUiOiJzcmMvYXBwL3BsYW5zbGlzdC9wbGFuc2xpc3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJveHNoYWRvd2NhcmR7XHJcbiAgICBib3gtc2hhZG93OiAwIDJweCA2cHggMCByZ2JhKDAsIDAsIDAsIDAuMik7XHJcbn1cclxuLm1hcmdpbnRvcDEwcGVye1xyXG4gICAgbWFyZ2luLXRvcDogMTVweDtcclxufVxyXG5cclxuaW9uLWNvbnRlbnQge1xyXG4gICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogI2ZhZjRmNDtcclxuICB9XHJcblxyXG4uY2FyZGJhY2tncm91bmR7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG5ib3JkZXItcmFkaXVzOiA4cHg7XHJcbn0iLCIuYm94c2hhZG93Y2FyZCB7XG4gIGJveC1zaGFkb3c6IDAgMnB4IDZweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcbn1cblxuLm1hcmdpbnRvcDEwcGVyIHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjZmFmNGY0O1xufVxuXG4uY2FyZGJhY2tncm91bmQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/planslist/planslist.page.ts":
/*!*********************************************!*\
  !*** ./src/app/planslist/planslist.page.ts ***!
  \*********************************************/
/*! exports provided: PlanslistPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlanslistPage", function() { return PlanslistPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _api_medicalcamp_medicalcamp_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../api/medicalcamp/medicalcamp.service */ "./src/app/api/medicalcamp/medicalcamp.service.ts");
/* harmony import */ var New_folder_src_app_api_path_path_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! New folder/src/app/api/path/path.service */ "./New folder/src/app/api/path/path.service.ts");








var PlanslistPage = /** @class */ (function () {
    function PlanslistPage(alertController, toastCtrl, router, menuCtrl, _Activatedroute, loadingCtrl, storage, _medicalcamp, _url) {
        this.alertController = alertController;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.menuCtrl = menuCtrl;
        this._Activatedroute = _Activatedroute;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this._medicalcamp = _medicalcamp;
        this._url = _url;
        this.tpskelton = [1, 2, 3, 4];
    }
    PlanslistPage.prototype.ngOnInit = function () {
    };
    PlanslistPage.prototype.ionViewWillEnter = function () {
        this.producturl = this._url.productimageurl;
        this.getplans();
    };
    PlanslistPage.prototype.getplans = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this._medicalcamp.planlist()
                    .subscribe(function (data) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                        this.tpskelton = false;
                        if (data['success'] == '1') {
                            this.planlistdata = data['data']['planlist'];
                            this.planliststatus = false;
                        }
                        else {
                        }
                        return [2 /*return*/];
                    });
                }); }, function (error) { _this.errorMsg = error.success; });
                return [2 /*return*/];
            });
        });
    };
    PlanslistPage.prototype.submit = function () {
        window.open('https://rzp.io/l/oAVXVON', '_system');
    };
    PlanslistPage.prototype.viewmore = function (id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this._medicalcamp.plandetails(id)
                    .subscribe(function (data) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                    var alert_1;
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                this.tpskelton = false;
                                if (!(data['success'] == '1')) return [3 /*break*/, 3];
                                return [4 /*yield*/, this.alertController.create({
                                        cssClass: 'my-custom-class',
                                        mode: 'ios',
                                        message: data['data']['plandetails'][0]['pa_plan_details'],
                                        buttons: ['OK']
                                    })];
                            case 1:
                                alert_1 = _a.sent();
                                return [4 /*yield*/, alert_1.present()];
                            case 2:
                                _a.sent();
                                return [3 /*break*/, 3];
                            case 3: return [2 /*return*/];
                        }
                    });
                }); }, function (error) { _this.errorMsg = error.success; });
                return [2 /*return*/];
            });
        });
    };
    PlanslistPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
        { type: _api_medicalcamp_medicalcamp_service__WEBPACK_IMPORTED_MODULE_5__["MedicalcampService"] },
        { type: New_folder_src_app_api_path_path_service__WEBPACK_IMPORTED_MODULE_6__["PathService"] }
    ]; };
    PlanslistPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-planslist',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./planslist.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/planslist/planslist.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./planslist.page.scss */ "./src/app/planslist/planslist.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"], _api_medicalcamp_medicalcamp_service__WEBPACK_IMPORTED_MODULE_5__["MedicalcampService"], New_folder_src_app_api_path_path_service__WEBPACK_IMPORTED_MODULE_6__["PathService"]])
    ], PlanslistPage);
    return PlanslistPage;
}());



/***/ })

}]);
//# sourceMappingURL=planslist-planslist-module.js.map