(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dentalform-dentalform-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/dentalform/dentalform.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dentalform/dentalform.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\n      <p><b>Dental </b></p>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div>\n    <div class=\"borderstudentlist\">\n      <h3 class=\"textaligncenter fontsize15px\"><u>Student Information</u></h3>\n      <ion-row>\n        <ion-col col-6> <p class=\"studentlist\">Name/Id:</p></ion-col>\n        <ion-col col-6><p class=\"studentlist\">{{firstname}} {{lastname}}/{{id}}</p></ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6><p class=\"studentlist\">Class/Roll No/ Div:</p></ion-col>\n        <ion-col col-6><p class=\"studentlist\">{{studentclass}}/{{studentrollno}}/{{studentdiv}}</p></ion-col>\n      </ion-row>\n    </div>\n    <ion-list padding>\n      <form [formGroup]=\"DentalForm\" >\n      <ion-item >\n        <ion-label  position=\"stacked\">Teeths Decayed:-</ion-label>\n        <ion-select value=\"0\" multiple=\"true\" formControlName=\"decayed\">\n          <ion-select-option value=\"24\" selected>24</ion-select-option>\n          <ion-select-option value=\"23\" selected>23</ion-select-option>\n          <ion-select-option *ngFor=\"let item of createparticularRange(11,85); let currentElementIndex=index\">{{currentElementIndex}}</ion-select-option>\n        </ion-select>\n        </ion-item>\n\n      <ion-item >\n        <ion-label  position=\"stacked\">Missing:-<ion-text color=\"danger\">*</ion-text></ion-label>\n        <ion-select value=\"0\" multiple=\"true\" formControlName=\"missing\">\n          <ion-select-option value=\"121\" selected>121</ion-select-option>\n          <ion-select-option value=\"122\" selected>122</ion-select-option>\n          <ion-select-option *ngFor=\"let item of createparticularRange(11,85); let currentElementIndex=index\">{{currentElementIndex}}</ion-select-option>\n        </ion-select>\n        </ion-item>\n\n      <ion-item >\n        <ion-label  position=\"stacked\">Filling/Crown:-<ion-text color=\"danger\">*</ion-text></ion-label>\n        <ion-select value=\"0\" multiple=\"true\" formControlName=\"crown\">\n          <ion-select-option value=\"41\" selected>41</ion-select-option>\n          <ion-select-option value=\"42\" selected>42</ion-select-option>\n          <ion-select-option *ngFor=\"let item of createparticularRange(11,85); let currentElementIndex=index\">{{currentElementIndex}}</ion-select-option>\n        </ion-select>\n        </ion-item>\n\n        <ion-item >\n          <ion-label  position=\"stacked\">Gingivitis:-</ion-label>\n          <ion-select value=\"No\" formControlName=\"givingteath\">\n            <ion-select-option value=\"Yes\" selected>Yes</ion-select-option>\n            <ion-select-option value=\"No\" selected>No</ion-select-option>\n          </ion-select>\n          </ion-item>\n\n        <ion-item >\n          <ion-label  position=\"stacked\">Periodontitis:-</ion-label>\n          <ion-select value=\"No\" formControlName=\"appearance\">\n            <ion-select-option value=\"Yes\" selected>Yes</ion-select-option>\n            <ion-select-option value=\"No\" selected>No</ion-select-option>\n          </ion-select>\n          </ion-item>\n\n          <ion-item >\n            <ion-label  position=\"stacked\">Stain:-</ion-label>\n            <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\" class=\"max-width\">\n                <ion-select class=\"maxwidth\" value=\"Absent\" formControlName=\"Stain\">\n                  <ion-select-option value=\"Present\" selected>Present</ion-select-option>\n                  <ion-select-option value=\"Absent\" selected>Absent</ion-select-option>\n                </ion-select>\n          </ion-col>\n              <ion-col size=\"6\">\n            <ion-select class=\"maxwidth\" value=\"GradeIII\"  formControlName=\"Stainsub\">\n              <ion-select-option value=\"GradeI\" selected>GradeI</ion-select-option>\n              <ion-select-option value=\"GradeII\" selected>GradeII</ion-select-option>\n              <ion-select-option value=\"GradeIII\" selected>GradeIII</ion-select-option>\n            </ion-select>\n          </ion-col>\n          </ion-row>\n        </ion-grid>\n            </ion-item>\n\n          <ion-item >\n            <ion-label  position=\"stacked\">Calculus:-</ion-label>\n            <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\" class=\"max-width\">\n                <ion-select class=\"maxwidth\" value=\"Absent\" formControlName=\"calculusteath\">\n                  <ion-select-option value=\"Present\" selected>Present</ion-select-option>\n                  <ion-select-option value=\"Absent\" selected>Absent</ion-select-option>\n                </ion-select>\n          </ion-col>\n              <ion-col size=\"6\">\n            <ion-select class=\"maxwidth\" value=\"GradeIII\"  formControlName=\"calculusteath2\">\n              <ion-select-option value=\"GradeI\" selected>GradeI</ion-select-option>\n              <ion-select-option value=\"GradeII\" selected>GradeII</ion-select-option>\n              <ion-select-option value=\"GradeIII\" selected>GradeIII</ion-select-option>\n            </ion-select>\n          </ion-col>\n          </ion-row>\n        </ion-grid>\n            </ion-item>\n\n            <ion-item >\n              <ion-label  position=\"stacked\">Flurosis:-</ion-label>\n              <ion-select value=\"No\" formControlName=\"Flurosis\">\n                <ion-select-option value=\"Yes\" selected>Yes</ion-select-option>\n                <ion-select-option value=\"No\" selected>No</ion-select-option>\n              </ion-select>\n              </ion-item>\n\n              <ion-item >\n                <ion-label  position=\"stacked\">Malocclusion:-</ion-label>\n                <ion-select value=\"No\" formControlName=\"Malocclusion\">\n                  <ion-select-option value=\"Yes\" selected>Yes</ion-select-option>\n                  <ion-select-option value=\"No\" selected>No</ion-select-option>\n                </ion-select>\n                </ion-item>\n              <ion-item >\n                <ion-label  position=\"stacked\">Brushing Frequency:-</ion-label>\n                <ion-select value=\"1\" formControlName=\"brushingfrequency\">\n                  <ion-select-option value=\"1\" selected>1</ion-select-option>\n                  <ion-select-option value=\"2\" selected>2</ion-select-option>\n                  <ion-select-option value=\"3\" selected>3</ion-select-option>\n                </ion-select>\n                </ion-item>\n              <ion-item >\n                <ion-label  position=\"stacked\">Use of Mouth Wash:-</ion-label>\n                <ion-select value=\"No\" formControlName=\"mouthwash\">\n                  <ion-select-option value=\"Yes\" selected>Yes</ion-select-option>\n                  <ion-select-option value=\"No\" selected>No</ion-select-option>\n                </ion-select>\n                </ion-item>\n              <ion-item >\n                <ion-label  position=\"stacked\">Tooth Type:-</ion-label>\n                <ion-select value=\"Normal\" formControlName=\"toothtype\">\n                  <ion-select-option value=\"Sensitive\" selected>Sensitive</ion-select-option>\n                  <ion-select-option value=\"Normal\" selected>Normal</ion-select-option>\n                </ion-select>\n                </ion-item>\n                <ion-item >\n                  <p class=\"fontsize11px colordefault textaligncenter\">Upload Photo</p>\n                  <ion-button class=\"centerblock\" (click)=\"selectImage()\" color=\"tertiary\">Upload</ion-button>\n                </ion-item>\n                <img src=\"{{imagepathurl}}\" style=\"width: 102px;\"/>\n                  <ion-item lines=\"none\">\n                    <ion-button class=\"centerblock\" type=\"submit\" color=\"tertiary\" Expand=\"block\"\n                      size=\"default\" (click)=\"submit()\">Submit</ion-button>\n                  </ion-item>\n                  </form>\n        </ion-list>\n        \n        </div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/dentalform/dentalform-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/dentalform/dentalform-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: DentalformPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DentalformPageRoutingModule", function() { return DentalformPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _dentalform_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dentalform.page */ "./src/app/dentalform/dentalform.page.ts");




var routes = [
    {
        path: '',
        component: _dentalform_page__WEBPACK_IMPORTED_MODULE_3__["DentalformPage"]
    }
];
var DentalformPageRoutingModule = /** @class */ (function () {
    function DentalformPageRoutingModule() {
    }
    DentalformPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], DentalformPageRoutingModule);
    return DentalformPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/dentalform/dentalform.module.ts":
/*!*************************************************!*\
  !*** ./src/app/dentalform/dentalform.module.ts ***!
  \*************************************************/
/*! exports provided: DentalformPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DentalformPageModule", function() { return DentalformPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _dentalform_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dentalform-routing.module */ "./src/app/dentalform/dentalform-routing.module.ts");
/* harmony import */ var _dentalform_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dentalform.page */ "./src/app/dentalform/dentalform.page.ts");







var DentalformPageModule = /** @class */ (function () {
    function DentalformPageModule() {
    }
    DentalformPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _dentalform_routing_module__WEBPACK_IMPORTED_MODULE_5__["DentalformPageRoutingModule"]
            ],
            declarations: [_dentalform_page__WEBPACK_IMPORTED_MODULE_6__["DentalformPage"]]
        })
    ], DentalformPageModule);
    return DentalformPageModule;
}());



/***/ }),

/***/ "./src/app/dentalform/dentalform.page.scss":
/*!*************************************************!*\
  !*** ./src/app/dentalform/dentalform.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".maxwidth {\n  max-width: 100% !important;\n  width: 126px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGVudGFsZm9ybS9DOlxcaW9uaWNcXGtydXNobmFtYWlcXE5ldyBmb2xkZXIvc3JjXFxhcHBcXGRlbnRhbGZvcm1cXGRlbnRhbGZvcm0ucGFnZS5zY3NzIiwic3JjL2FwcC9kZW50YWxmb3JtL2RlbnRhbGZvcm0ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksMEJBQUE7RUFDQSx1QkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvZGVudGFsZm9ybS9kZW50YWxmb3JtLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXh3aWR0aHtcclxuICAgIG1heC13aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG4gICAgd2lkdGg6IDEyNnB4ICFpbXBvcnRhbnQ7XHJcbn0iLCIubWF4d2lkdGgge1xuICBtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgd2lkdGg6IDEyNnB4ICFpbXBvcnRhbnQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/dentalform/dentalform.page.ts":
/*!***********************************************!*\
  !*** ./src/app/dentalform/dentalform.page.ts ***!
  \***********************************************/
/*! exports provided: DentalformPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DentalformPage", function() { return DentalformPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../api/form/form.service */ "./src/app/api/form/form.service.ts");
/* harmony import */ var _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/Camera/ngx */ "./node_modules/@ionic-native/Camera/ngx/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");












var DentalformPage = /** @class */ (function () {
    function DentalformPage(formBuilder, toastCtrl, router, storage, _form, menuCtrl, _Activatedroute, loadingCtrl, file, camera, actionSheetController, filePath, plt, webview) {
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.storage = storage;
        this._form = _form;
        this.menuCtrl = menuCtrl;
        this._Activatedroute = _Activatedroute;
        this.loadingCtrl = loadingCtrl;
        this.file = file;
        this.camera = camera;
        this.actionSheetController = actionSheetController;
        this.filePath = filePath;
        this.plt = plt;
        this.webview = webview;
        this.DentalForm = this.formBuilder.group({
            decayed: ['0'],
            missing: ['0'],
            crown: ['0'],
            givingteath: ['No'],
            appearance: ['No'],
            Stain: ['Absent'],
            Stainsub: ['GradeIII'],
            calculusteath: ['Absent'],
            calculusteath2: ['GradeIII'],
            Flurosis: ['No'],
            Malocclusion: ['No'],
            brushingfrequency: ['1'],
            mouthwash: ['No'],
            toothtype: ['Normal']
        });
    }
    DentalformPage.prototype.ngOnInit = function () {
        this.id = this._Activatedroute.snapshot.paramMap.get('id');
        this.firstname = this._Activatedroute.snapshot.paramMap.get('firstname');
        this.lastname = this._Activatedroute.snapshot.paramMap.get('lastname');
        this.consoluentid = this._Activatedroute.snapshot.paramMap.get('consulentid');
        this.event_id = this._Activatedroute.snapshot.paramMap.get('event_id');
        this.studentclass = this._Activatedroute.snapshot.paramMap.get('studentclass');
        this.studentrollno = this._Activatedroute.snapshot.paramMap.get('studentrollno');
        this.studentdiv = this._Activatedroute.snapshot.paramMap.get('studentdiv');
    };
    DentalformPage.prototype.createRange = function (number) {
        var items = [];
        for (var i = 1; i <= number; i++) {
            items.push(i);
        }
        return items;
    };
    DentalformPage.prototype.createparticularRange = function (start, number) {
        var items = [];
        for (var i = start; i <= number; i++) {
            items.push(i);
        }
        return items;
    };
    DentalformPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(false);
    };
    Object.defineProperty(DentalformPage.prototype, "decayed", {
        get: function () {
            return this.DentalForm.get("decayed");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DentalformPage.prototype, "missing", {
        get: function () {
            return this.DentalForm.get("missing");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DentalformPage.prototype, "crown", {
        get: function () {
            return this.DentalForm.get("crown");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DentalformPage.prototype, "givingteath", {
        get: function () {
            return this.DentalForm.get("givingteath");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DentalformPage.prototype, "appearance", {
        get: function () {
            return this.DentalForm.get("appearance");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DentalformPage.prototype, "Stain", {
        get: function () {
            return this.DentalForm.get("Stain");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DentalformPage.prototype, "Stainsub", {
        get: function () {
            return this.DentalForm.get("Stainsub");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DentalformPage.prototype, "calculusteath", {
        get: function () {
            return this.DentalForm.get("calculusteath");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DentalformPage.prototype, "calculusteath2", {
        get: function () {
            return this.DentalForm.get("calculusteath2");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DentalformPage.prototype, "Flurosis", {
        get: function () {
            return this.DentalForm.get("Flurosis");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DentalformPage.prototype, "Malocclusion", {
        get: function () {
            return this.DentalForm.get("Malocclusion");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DentalformPage.prototype, "brushingfrequency", {
        get: function () {
            return this.DentalForm.get("brushingfrequency");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DentalformPage.prototype, "mouthwash", {
        get: function () {
            return this.DentalForm.get("mouthwash");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DentalformPage.prototype, "toothtype", {
        get: function () {
            return this.DentalForm.get("toothtype");
        },
        enumerable: true,
        configurable: true
    });
    DentalformPage.prototype.selectImage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: "Select Image source",
                            buttons: [{
                                    text: 'Load from Library',
                                    handler: function () {
                                        _this.pickImage(_this.camera.PictureSourceType.PHOTOLIBRARY);
                                    }
                                },
                                {
                                    text: 'Use Camera',
                                    handler: function () {
                                        _this.pickImage(_this.camera.PictureSourceType.CAMERA);
                                    }
                                },
                                {
                                    text: 'Cancel',
                                    role: 'cancel'
                                }
                            ]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DentalformPage.prototype.pickImage = function (sourceType) {
        var _this = this;
        var options = {
            quality: 50,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imagePath) {
            _this.imageformdata = imagePath;
            _this.startUpload(imagePath);
            if (_this.plt.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (err) {
            alert("Error in reading file!!!");
        });
    };
    DentalformPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    DentalformPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(function (success) {
            var filePath = _this.file.dataDirectory + newFileName;
            _this.imagepathurl = _this.pathForImage(filePath);
        }, function (error) {
        });
    };
    DentalformPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            var converted = this.webview.convertFileSrc(img);
            return converted;
        }
    };
    DentalformPage.prototype.startUpload = function (imgEntry) {
        var _this = this;
        this.file.resolveLocalFilesystemUrl(imgEntry)
            .then(function (entry) {
            entry.file(function (file) { return _this.readFile(file); });
        })
            .catch(function (err) {
            alert('Error while reading file.');
        });
    };
    DentalformPage.prototype.readFile = function (file) {
        var _this = this;
        var reader = new FileReader();
        reader.onload = function () {
            var formData = new FormData();
            var imgBlob = new Blob([reader.result], {
                type: file.type
            });
            formData.append('img', imgBlob, file.name);
            formData.append('event_id', _this.event_id);
            formData.append('membercode', _this.id);
            _this.setLogoimg(formData);
        };
        reader.readAsArrayBuffer(file);
    };
    DentalformPage.prototype.setLogoimg = function (img) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this._form.updateLogo(img)
                            .subscribe(function (data) {
                            loading.dismiss();
                            if (data['status'] !== undefined && data['status'] === true) {
                            }
                            else {
                            }
                        }, function (error) { _this.errorMsg = error.success; });
                        return [2 /*return*/];
                }
            });
        });
    };
    DentalformPage.prototype.submit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this._form.dental(this.DentalForm.value, this.id, this.consoluentid, this.event_id)
                            .subscribe(function (data) {
                            loading.dismiss();
                            if (data[0]['success'] == '1') {
                                alert("Data Enter Successfully");
                                _this.router.navigate(['/medicalcamp']);
                            }
                            else {
                                alert(data['msg']);
                                _this.DentalForm.reset();
                            }
                        }, function (error) { _this.errorMsg = error.success; });
                        return [2 /*return*/];
                }
            });
        });
    };
    DentalformPage.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
        { type: _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__["FormService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
        { type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_8__["File"] },
        { type: _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_7__["Camera"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"] },
        { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_9__["FilePath"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
        { type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_10__["WebView"] }
    ]; };
    DentalformPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dentalform',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./dentalform.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/dentalform/dentalform.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./dentalform.page.scss */ "./src/app/dentalform/dentalform.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
            _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__["FormService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"], _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_8__["File"], _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_7__["Camera"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"], _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_9__["FilePath"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"], _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_10__["WebView"]])
    ], DentalformPage);
    return DentalformPage;
}());



/***/ })

}]);
//# sourceMappingURL=dentalform-dentalform-module.js.map