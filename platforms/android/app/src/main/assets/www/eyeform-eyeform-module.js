(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["eyeform-eyeform-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/eyeform/eyeform.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/eyeform/eyeform.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\n      <p><b>Eye  </b></p>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div>\n    <div class=\"borderstudentlist\">\n      <h3 class=\"textaligncenter fontsize15px\"><u>Student Information</u></h3>\n      <ion-row>\n        <ion-col col-6> <p class=\"studentlist\">Name/Id:</p></ion-col>\n        <ion-col col-6><p class=\"studentlist\">{{firstname}} {{lastname}}/{{id}}</p></ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6><p class=\"studentlist\">Class/Roll No/ Div:</p></ion-col>\n        <ion-col col-6><p class=\"studentlist\">{{studentclass}}/{{studentrollno}}/{{studentdiv}}</p></ion-col>\n      </ion-row>\n    </div>\n    <ion-list padding>\n      <form [formGroup]=\"EyeForm\" >\n      <ion-item >\n        <ion-label  position=\"stacked\">Wears Glasses:-</ion-label>\n        <ion-select value=\"No\" formControlName=\"Glasses\">\n          <ion-select-option value=\"Yes\" >Yes</ion-select-option>\n          <ion-select-option value=\"No\" >No</ion-select-option>\n        </ion-select>\n      </ion-item>\n\n          <ion-item >\n            <ion-label  position=\"stacked\">Vision Right Eye:-</ion-label>\n            <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\" class=\"max-width inputclass\">\n                <ion-input inputmode=\"decimal\" placeholder=\"Enter Value\"  formControlName=\"righteyeno\" value=\"6\"></ion-input>\n          </ion-col>\n              <ion-col  size=\"6\" class=\"max-width inputclass\">\n                <ion-select class=\"marginauto\" formControlName=\"righteyeno2\" value=\"6\">\n                  <ion-select-option value=\"6\" >6</ion-select-option>\n                  <ion-select-option value=\"9\" >9</ion-select-option>\n                  <ion-select-option value=\"12\" >12</ion-select-option>\n                  <ion-select-option value=\"18\" >18</ion-select-option>\n                  <ion-select-option value=\"24\" >24</ion-select-option>\n                  <ion-select-option value=\"36\" >36</ion-select-option>\n                </ion-select>\n          </ion-col>\n          </ion-row>\n        </ion-grid>\n        <ion-grid>\n          <ion-row>\n            <ion-col size=\"6\" class=\"max-width textaligncenter\">\n              <ion-label  position=\"stacked\">Spherical:-</ion-label>\n              <ion-input inputmode=\"decimal\" placeholder=\"Enter Value\" formControlName=\"spherical\" value=\"6\"></ion-input>\n        </ion-col>\n            <ion-col size=\"6\" class=\"max-width textaligncenter\">\n              <ion-label  position=\"stacked\">Cylindrical:-</ion-label>\n              <ion-input inputmode=\"decimal\" placeholder=\"Enter Value\" value=\"6\" formControlName=\"cylindrical\" ></ion-input>\n        </ion-col>\n        </ion-row>\n      </ion-grid>\n            </ion-item>\n\n            <ion-item >\n              <ion-label  position=\"stacked\">Vision Left Eye:-</ion-label>\n              <ion-grid>\n              <ion-row>\n                <ion-col size=\"6\" class=\"max-width inputclass\">\n                  <ion-input inputmode=\"decimal\" placeholder=\"Enter Value\" value=\"6\" formControlName=\"lefteyeno\" ></ion-input>\n            </ion-col>\n                <ion-col size=\"6\" class=\"max-width inputclass\">\n                  <ion-select class=\"marginauto\" formControlName=\"lefteyeno2\" value=\"6\">\n                    <ion-select-option value=\"6\" >6</ion-select-option>\n                    <ion-select-option value=\"9\" >9</ion-select-option>\n                    <ion-select-option value=\"12\" >12</ion-select-option>\n                    <ion-select-option value=\"18\" >18</ion-select-option>\n                    <ion-select-option value=\"24\" >24</ion-select-option>\n                    <ion-select-option value=\"36\" >36</ion-select-option>\n                  </ion-select>\n            </ion-col>\n            </ion-row>\n          </ion-grid>\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\" class=\"max-width textaligncenter\">\n                <ion-label  position=\"stacked\">Spherical:-</ion-label>\n                <ion-input inputmode=\"decimal\" placeholder=\"Enter Value\" value=\"6\" formControlName=\"leftSpherical\" ></ion-input>\n          </ion-col>\n              <ion-col size=\"6\" class=\"max-width textaligncenter\">\n                <ion-label  position=\"stacked\">Cylindrical:-</ion-label>\n                <ion-input inputmode=\"decimal\" placeholder=\"Enter Value\" value=\"6\" formControlName=\"leftcylindrical\" ></ion-input>\n          </ion-col>\n          </ion-row>\n        </ion-grid>\n              </ion-item>\n              <ion-item >\n                <ion-radio-group value=\"Normal\" formControlName=\"righteye\">\n                  <ion-list-header>\n                    <ion-label>Colour Perception/Right Eye</ion-label>\n                  </ion-list-header>\n                  <ion-item>\n                    <ion-label>Normal</ion-label>\n                    <ion-radio slot=\"start\" color=\"tertiary\" value=\"Normal\"  ></ion-radio>\n                  </ion-item>\n        \n                  <ion-item>\n                    <ion-label>AbNormal</ion-label>\n                    <ion-radio slot=\"start\" color=\"tertiary\" value=\"AbNormal\"  ></ion-radio>\n                  </ion-item>\n                </ion-radio-group>\n              </ion-item>\n              <ion-item >\n                <ion-radio-group value=\"Normal\" formControlName=\"lefteye\" >\n                  <ion-list-header>\n                    <ion-label>Colour Perception/Left Eye</ion-label>\n                  </ion-list-header>\n                  <ion-item>\n                    <ion-label>Normal</ion-label>\n                    <ion-radio slot=\"start\" color=\"tertiary\" value=\"Normal\" ></ion-radio>\n                  </ion-item>\n        \n                  <ion-item>\n                    <ion-label>AbNormal</ion-label>\n                    <ion-radio slot=\"start\" color=\"tertiary\" value=\"AbNormal\"></ion-radio>\n                  </ion-item>\n                </ion-radio-group>\n              </ion-item>\n              <ion-item >\n                <ion-label  position=\"stacked\">Remark/Treatment Advised:-</ion-label>\n                <ion-textarea formControlName=\"remark\"></ion-textarea>\n                </ion-item>\n                \n                  <ion-item lines=\"none\">\n                    <ion-button class=\"centerblock\" type=\"submit\" color=\"tertiary\" Expand=\"block\"\n                      size=\"default\"  (click)=\"submit()\">Submit</ion-button>\n                  </ion-item>\n                </form>\n        </ion-list>\n        </div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/eyeform/eyeform-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/eyeform/eyeform-routing.module.ts ***!
  \***************************************************/
/*! exports provided: EyeformPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EyeformPageRoutingModule", function() { return EyeformPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _eyeform_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./eyeform.page */ "./src/app/eyeform/eyeform.page.ts");




var routes = [
    {
        path: '',
        component: _eyeform_page__WEBPACK_IMPORTED_MODULE_3__["EyeformPage"]
    }
];
var EyeformPageRoutingModule = /** @class */ (function () {
    function EyeformPageRoutingModule() {
    }
    EyeformPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], EyeformPageRoutingModule);
    return EyeformPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/eyeform/eyeform.module.ts":
/*!*******************************************!*\
  !*** ./src/app/eyeform/eyeform.module.ts ***!
  \*******************************************/
/*! exports provided: EyeformPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EyeformPageModule", function() { return EyeformPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _eyeform_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./eyeform-routing.module */ "./src/app/eyeform/eyeform-routing.module.ts");
/* harmony import */ var _eyeform_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./eyeform.page */ "./src/app/eyeform/eyeform.page.ts");







var EyeformPageModule = /** @class */ (function () {
    function EyeformPageModule() {
    }
    EyeformPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _eyeform_routing_module__WEBPACK_IMPORTED_MODULE_5__["EyeformPageRoutingModule"]
            ],
            declarations: [_eyeform_page__WEBPACK_IMPORTED_MODULE_6__["EyeformPage"]]
        })
    ], EyeformPageModule);
    return EyeformPageModule;
}());



/***/ }),

/***/ "./src/app/eyeform/eyeform.page.scss":
/*!*******************************************!*\
  !*** ./src/app/eyeform/eyeform.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".max-width {\n  max-width: 100%;\n}\n\n.inputclass {\n  border: 1px solid;\n  max-width: 100%;\n  text-align: center;\n  border-radius: 10px;\n}\n\n.marginauto {\n  margin: auto !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXllZm9ybS9DOlxcaW9uaWNcXGtydXNobmFtYWlcXE5ldyBmb2xkZXIvc3JjXFxhcHBcXGV5ZWZvcm1cXGV5ZWZvcm0ucGFnZS5zY3NzIiwic3JjL2FwcC9leWVmb3JtL2V5ZWZvcm0ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBQTtBQ0NKOztBRENBO0VBQ0ksaUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ0VKOztBRENBO0VBQ0ksdUJBQUE7QUNFSiIsImZpbGUiOiJzcmMvYXBwL2V5ZWZvcm0vZXllZm9ybS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWF4LXdpZHRoe1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG59XHJcbi5pbnB1dGNsYXNze1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG59XHJcblxyXG4ubWFyZ2luYXV0b3tcclxuICAgIG1hcmdpbjogYXV0byAhaW1wb3J0YW50O1xyXG59XHJcbiIsIi5tYXgtd2lkdGgge1xuICBtYXgtd2lkdGg6IDEwMCU7XG59XG5cbi5pbnB1dGNsYXNzIHtcbiAgYm9yZGVyOiAxcHggc29saWQ7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuXG4ubWFyZ2luYXV0byB7XG4gIG1hcmdpbjogYXV0byAhaW1wb3J0YW50O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/eyeform/eyeform.page.ts":
/*!*****************************************!*\
  !*** ./src/app/eyeform/eyeform.page.ts ***!
  \*****************************************/
/*! exports provided: EyeformPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EyeformPage", function() { return EyeformPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../api/form/form.service */ "./src/app/api/form/form.service.ts");







var EyeformPage = /** @class */ (function () {
    function EyeformPage(formBuilder, toastCtrl, router, storage, _form, menuCtrl, _Activatedroute, loadingCtrl) {
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.storage = storage;
        this._form = _form;
        this.menuCtrl = menuCtrl;
        this._Activatedroute = _Activatedroute;
        this.loadingCtrl = loadingCtrl;
        this.EyeForm = this.formBuilder.group({
            Glasses: ['No'],
            righteyeno: ['6'],
            righteyeno2: ['6'],
            spherical: ['6'],
            cylindrical: ['6'],
            lefteyeno: ['6'],
            lefteyeno2: ['6'],
            leftSpherical: ['6'],
            leftcylindrical: ['6'],
            righteye: ['Normal'],
            lefteye: ['Normal'],
            remark: [''],
        });
    }
    EyeformPage.prototype.ngOnInit = function () {
        this.id = this._Activatedroute.snapshot.paramMap.get('id');
        this.firstname = this._Activatedroute.snapshot.paramMap.get('firstname');
        this.lastname = this._Activatedroute.snapshot.paramMap.get('lastname');
        this.consoluentid = this._Activatedroute.snapshot.paramMap.get('consulentid');
        this.event_id = this._Activatedroute.snapshot.paramMap.get('event_id');
        this.studentclass = this._Activatedroute.snapshot.paramMap.get('studentclass');
        this.studentrollno = this._Activatedroute.snapshot.paramMap.get('studentrollno');
        this.studentdiv = this._Activatedroute.snapshot.paramMap.get('studentdiv');
    };
    EyeformPage.prototype.createRange = function (number) {
        var items = [];
        for (var i = 1; i <= number; i++) {
            items.push(i);
        }
        return items;
    };
    EyeformPage.prototype.createparticularRange = function (start, number) {
        var items = [];
        for (var i = start; i <= number; i++) {
            items.push(i);
        }
        return items;
    };
    EyeformPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(false);
    };
    Object.defineProperty(EyeformPage.prototype, "Glasses", {
        get: function () {
            return this.EyeForm.get("Glasses");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EyeformPage.prototype, "righteyeno", {
        get: function () {
            return this.EyeForm.get("righteyeno");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EyeformPage.prototype, "righteyeno2", {
        get: function () {
            return this.EyeForm.get("righteyeno2");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EyeformPage.prototype, "spherical", {
        get: function () {
            return this.EyeForm.get("spherical");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EyeformPage.prototype, "cylindrical", {
        get: function () {
            return this.EyeForm.get("cylindrical");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EyeformPage.prototype, "lefteyeno", {
        get: function () {
            return this.EyeForm.get("lefteyeno");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EyeformPage.prototype, "lefteyeno2", {
        get: function () {
            return this.EyeForm.get("lefteyeno2");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EyeformPage.prototype, "leftSpherical", {
        get: function () {
            return this.EyeForm.get("leftSpherical");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EyeformPage.prototype, "leftcylindrical", {
        get: function () {
            return this.EyeForm.get("leftcylindrical");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EyeformPage.prototype, "righteye", {
        get: function () {
            return this.EyeForm.get("righteye");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EyeformPage.prototype, "lefteye", {
        get: function () {
            return this.EyeForm.get("lefteye");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EyeformPage.prototype, "remark", {
        get: function () {
            return this.EyeForm.get("remark");
        },
        enumerable: true,
        configurable: true
    });
    EyeformPage.prototype.submit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this._form.eyeform(this.EyeForm.value, this.id, this.consoluentid, this.event_id)
                            .subscribe(function (data) {
                            loading.dismiss();
                            if (data['success'] == '1') {
                                alert("Data Enter Successfully");
                                _this.router.navigate(['/medicalcamp']);
                            }
                            else {
                                alert(data['msg']);
                                _this.EyeForm.reset();
                            }
                        }, function (error) { _this.errorMsg = error.success; });
                        return [2 /*return*/];
                }
            });
        });
    };
    EyeformPage.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
        { type: _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__["FormService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] }
    ]; };
    EyeformPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-eyeform',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./eyeform.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/eyeform/eyeform.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./eyeform.page.scss */ "./src/app/eyeform/eyeform.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
            _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__["FormService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]])
    ], EyeformPage);
    return EyeformPage;
}());



/***/ })

}]);
//# sourceMappingURL=eyeform-eyeform-module.js.map