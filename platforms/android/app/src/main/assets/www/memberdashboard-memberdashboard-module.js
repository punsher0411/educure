(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["memberdashboard-memberdashboard-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/memberdashboard/memberdashboard.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/memberdashboard/memberdashboard.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header  >\n\t<ion-toolbar>\n\t\t<ion-buttons slot=\"start\">\n\t\t</ion-buttons>\n\t\t<img src=\"assets/imgs/logo.jpeg\" class=\"centerblock logoimage\"/>\n\t</ion-toolbar>\n</ion-header>\n\n<ion-content>\n\t<div>\n\t\n\t<p class=\"fontsize11px textaligncenter\">Student Dashboard</p>\n\t<p class=\"fontsize13px textaligncenter colordefault\" [routerLink]=\"['/medicalrecordmember']\"><u>Click To Check Medical Record</u></p>\n\t<p class=\"fontsize13px textaligncenter colordefault\" [routerLink]=\"['/planslist']\"><u>Buy Now Plans</u></p> \n\t<p class=\"fontsize13px textaligncenter colordefault\" [routerLink]=\"['/chatbot']\"><u>ChatBot</u></p>\n    <p class=\"fontsize13px textaligncenter colordefault\" (click)=\"logout()\"><u>Logout</u></p>\n\t</div>\n\n\n\t\n</ion-content>\n");

/***/ }),

/***/ "./src/app/memberdashboard/memberdashboard-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/memberdashboard/memberdashboard-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: MemberdashboardPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberdashboardPageRoutingModule", function() { return MemberdashboardPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _memberdashboard_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./memberdashboard.page */ "./src/app/memberdashboard/memberdashboard.page.ts");




var routes = [
    {
        path: '',
        component: _memberdashboard_page__WEBPACK_IMPORTED_MODULE_3__["MemberdashboardPage"]
    }
];
var MemberdashboardPageRoutingModule = /** @class */ (function () {
    function MemberdashboardPageRoutingModule() {
    }
    MemberdashboardPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], MemberdashboardPageRoutingModule);
    return MemberdashboardPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/memberdashboard/memberdashboard.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/memberdashboard/memberdashboard.module.ts ***!
  \***********************************************************/
/*! exports provided: MemberdashboardPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberdashboardPageModule", function() { return MemberdashboardPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _memberdashboard_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./memberdashboard-routing.module */ "./src/app/memberdashboard/memberdashboard-routing.module.ts");
/* harmony import */ var _memberdashboard_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./memberdashboard.page */ "./src/app/memberdashboard/memberdashboard.page.ts");







var MemberdashboardPageModule = /** @class */ (function () {
    function MemberdashboardPageModule() {
    }
    MemberdashboardPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _memberdashboard_routing_module__WEBPACK_IMPORTED_MODULE_5__["MemberdashboardPageRoutingModule"]
            ],
            declarations: [_memberdashboard_page__WEBPACK_IMPORTED_MODULE_6__["MemberdashboardPage"]]
        })
    ], MemberdashboardPageModule);
    return MemberdashboardPageModule;
}());



/***/ }),

/***/ "./src/app/memberdashboard/memberdashboard.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/memberdashboard/memberdashboard.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21lbWJlcmRhc2hib2FyZC9tZW1iZXJkYXNoYm9hcmQucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/memberdashboard/memberdashboard.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/memberdashboard/memberdashboard.page.ts ***!
  \*********************************************************/
/*! exports provided: MemberdashboardPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberdashboardPage", function() { return MemberdashboardPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var MemberdashboardPage = /** @class */ (function () {
    function MemberdashboardPage(menuCtrl, storage, router) {
        this.menuCtrl = menuCtrl;
        this.storage = storage;
        this.router = router;
    }
    MemberdashboardPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(true);
    };
    MemberdashboardPage.prototype.logout = function () {
        this.storage.clear();
        this.storage.set('roleapp', 'member');
        this.router.navigate(['/login']);
    };
    MemberdashboardPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
    ]; };
    MemberdashboardPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-memberdashboard',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./memberdashboard.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/memberdashboard/memberdashboard.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./memberdashboard.page.scss */ "./src/app/memberdashboard/memberdashboard.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], MemberdashboardPage);
    return MemberdashboardPage;
}());



/***/ })

}]);
//# sourceMappingURL=memberdashboard-memberdashboard-module.js.map