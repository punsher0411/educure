(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["chatbot-chatbot-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/chatbot/chatbot.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/chatbot/chatbot.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/memberdashboard\"></ion-back-button>\n      <p ><b>ChatBot</b></p>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div >\n   \n  <iframe frameborder=\"0\" style=\"position: absolute;border: none;overflow:hidden;height:100%;width:100%\" height=\"100%\" width=\"100%\"  src=\"https://console.dialogflow.com/api-client/demo/embedded/bcc8f721-d354-4fa0-b3f3-27a028739397\"></iframe>\n</div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/chatbot/chatbot-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/chatbot/chatbot-routing.module.ts ***!
  \***************************************************/
/*! exports provided: ChatbotPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatbotPageRoutingModule", function() { return ChatbotPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _chatbot_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chatbot.page */ "./src/app/chatbot/chatbot.page.ts");




var routes = [
    {
        path: '',
        component: _chatbot_page__WEBPACK_IMPORTED_MODULE_3__["ChatbotPage"]
    }
];
var ChatbotPageRoutingModule = /** @class */ (function () {
    function ChatbotPageRoutingModule() {
    }
    ChatbotPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], ChatbotPageRoutingModule);
    return ChatbotPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/chatbot/chatbot.module.ts":
/*!*******************************************!*\
  !*** ./src/app/chatbot/chatbot.module.ts ***!
  \*******************************************/
/*! exports provided: ChatbotPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatbotPageModule", function() { return ChatbotPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _chatbot_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./chatbot-routing.module */ "./src/app/chatbot/chatbot-routing.module.ts");
/* harmony import */ var _chatbot_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./chatbot.page */ "./src/app/chatbot/chatbot.page.ts");







var ChatbotPageModule = /** @class */ (function () {
    function ChatbotPageModule() {
    }
    ChatbotPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _chatbot_routing_module__WEBPACK_IMPORTED_MODULE_5__["ChatbotPageRoutingModule"]
            ],
            declarations: [_chatbot_page__WEBPACK_IMPORTED_MODULE_6__["ChatbotPage"]]
        })
    ], ChatbotPageModule);
    return ChatbotPageModule;
}());



/***/ }),

/***/ "./src/app/chatbot/chatbot.page.scss":
/*!*******************************************!*\
  !*** ./src/app/chatbot/chatbot.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".margintop10per {\n  margin-top: 15px;\n}\n\nion-content {\n  --ion-background-color: #faf4f4;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhdGJvdC9DOlxcaW9uaWNcXGtydXNobmFtYWlcXE5ldyBmb2xkZXIvc3JjXFxhcHBcXGNoYXRib3RcXGNoYXRib3QucGFnZS5zY3NzIiwic3JjL2FwcC9jaGF0Ym90L2NoYXRib3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZ0JBQUE7QUNDSjs7QURFQTtFQUNJLCtCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9jaGF0Ym90L2NoYXRib3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1hcmdpbnRvcDEwcGVye1xyXG4gICAgbWFyZ2luLXRvcDogMTVweDtcclxufVxyXG5cclxuaW9uLWNvbnRlbnQge1xyXG4gICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogI2ZhZjRmNDtcclxuICB9IiwiLm1hcmdpbnRvcDEwcGVyIHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjZmFmNGY0O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/chatbot/chatbot.page.ts":
/*!*****************************************!*\
  !*** ./src/app/chatbot/chatbot.page.ts ***!
  \*****************************************/
/*! exports provided: ChatbotPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatbotPage", function() { return ChatbotPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ChatbotPage = /** @class */ (function () {
    function ChatbotPage() {
    }
    ChatbotPage.prototype.ngOnInit = function () {
    };
    ChatbotPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-chatbot',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./chatbot.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/chatbot/chatbot.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./chatbot.page.scss */ "./src/app/chatbot/chatbot.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ChatbotPage);
    return ChatbotPage;
}());



/***/ })

}]);
//# sourceMappingURL=chatbot-chatbot-module.js.map