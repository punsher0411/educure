(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dietform-dietform-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/dietform/dietform.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dietform/dietform.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/medicalcamp\"></ion-back-button>\n      <p><b>Diet </b></p>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div>\n    <div class=\"borderstudentlist\">\n      <h3 class=\"textaligncenter fontsize15px\"><u>Student Information</u></h3>\n      <ion-row>\n        <ion-col col-6> <p class=\"studentlist\">Name/Id:</p></ion-col>\n        <ion-col col-6><p class=\"studentlist\">{{firstname}} {{lastname}}/{{id}}</p></ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6><p class=\"studentlist\">Class/Roll No/ Div:</p></ion-col>\n        <ion-col col-6><p class=\"studentlist\">{{studentclass}}/{{studentrollno}}/{{studentdiv}}</p></ion-col>\n      </ion-row>\n    </div>\n    <ion-list padding>\n      <form [formGroup]=\"DietForm\" >\n      <ion-item >\n        <ion-label  position=\"stacked\">Height:-(Cms)<ion-text color=\"danger\">*</ion-text></ion-label>\n        <ion-select value=\"121\" >\n          <ion-select-option value=\"121\" selected>121</ion-select-option>\n          <ion-select-option value=\"122\" selected>122</ion-select-option>\n          <ion-select-option *ngFor=\"let item of createRange(200); let currentElementIndex=index\">{{currentElementIndex}}</ion-select-option>\n        </ion-select>\n        </ion-item>\n\n      <ion-item >\n        <ion-label  position=\"stacked\">Weight:-(Kgs)<ion-text color=\"danger\">*</ion-text></ion-label>\n        <ion-select value=\"41\" >\n          <ion-select-option value=\"41\" selected>41</ion-select-option>\n          <ion-select-option value=\"42\" selected>42</ion-select-option>\n          <ion-select-option *ngFor=\"let item of createRange(100); let currentElementIndex=index\">{{currentElementIndex}}</ion-select-option>\n        </ion-select>\n        </ion-item>\n\n      <ion-item >\n        <ion-label  position=\"stacked\">BMI</ion-label>\n        <ion-input clearInput value=\"224\" readonly=\"\" ></ion-input>\n        </ion-item>\n      <ion-item >\n        <ion-label  position=\"stacked\">Morning Breakfast:-</ion-label>\n        <ion-select value=\"Milk\" multiple=\"true\" formControlName=\"Breakfast\">\n          <ion-select-option value=\"Milk\" >Milk</ion-select-option>\n          <ion-select-option value=\"Omlet\" >Omlet</ion-select-option>\n          <ion-select-option value=\"Egg\" >Egg</ion-select-option>\n          <ion-select-option value=\"Poha\" >Poha</ion-select-option>\n          <ion-select-option value=\"Upma\" >Upma</ion-select-option>\n          <ion-select-option value=\"Idli\" >Idli</ion-select-option>\n          <ion-select-option value=\"Dosa\" >Dosa</ion-select-option>\n          <ion-select-option value=\"Biscuit\" >Biscuit</ion-select-option>\n          <ion-select-option value=\"Tea\" >Tea</ion-select-option>\n          <ion-select-option value=\"Coffee\" >Coffee</ion-select-option>\n          <ion-select-option value=\"Drink\" >Flavoured Nutrional Drink</ion-select-option>\n          <ion-select-option value=\"Roti\" >Roti</ion-select-option>\n          </ion-select>\n        </ion-item>\n\n      <ion-item >\n        <ion-label  position=\"stacked\">Lunch:-<ion-text color=\"danger\">*</ion-text></ion-label>\n        <ion-select value=\"Roti\" multiple=\"true\" formControlName=\"lunch\">\n          <ion-select-option value=\"Roti\" >Roti</ion-select-option>\n          <ion-select-option value=\"Rice\" >Rice</ion-select-option>\n          <ion-select-option value=\"Sabji\" >Sabji</ion-select-option>\n          <ion-select-option value=\"Dal\" >Dal</ion-select-option>\n          <ion-select-option value=\"Fish\" >Fish</ion-select-option>\n          <ion-select-option value=\"Chicken\" >Chicken</ion-select-option>\n          <ion-select-option value=\"Meat\" >Meat</ion-select-option>\n          <ion-select-option value=\"Sweets\" >Sweets</ion-select-option>\n          <ion-select-option value=\"Pulses\" >Pulses</ion-select-option>\n        </ion-select>\n        </ion-item>\n\n      <ion-item >\n        <ion-label  position=\"stacked\">Evening:-<ion-text color=\"danger\">*</ion-text></ion-label>\n        <ion-select value=\"Milk\" multiple=\"true\" formControlName=\"Evening\">\n          <ion-select-option value=\"Milk\" >Milk</ion-select-option>\n          <ion-select-option value=\"Omlet\" >Omlet</ion-select-option>\n          <ion-select-option value=\"Egg\" >Egg</ion-select-option>\n          <ion-select-option value=\"Poha\" >Poha</ion-select-option>\n          <ion-select-option value=\"Upma\" >Upma</ion-select-option>\n          <ion-select-option value=\"Idli\" >Idli</ion-select-option>\n          <ion-select-option value=\"Dosa\" >Dosa</ion-select-option>\n          <ion-select-option value=\"Biscuit\" >Biscuit</ion-select-option>\n          <ion-select-option value=\"Tea\" >Tea</ion-select-option>\n          <ion-select-option value=\"Coffee\" >Coffee</ion-select-option>\n          <ion-select-option value=\"Drink\" >Flavoured Nutrional Drink</ion-select-option>\n          <ion-select-option value=\"Roti\" >Roti</ion-select-option>\n          <ion-select-option value=\"Paratha\" >Paratha</ion-select-option>\n          <ion-select-option value=\"Farshan\" >Farshan</ion-select-option>\n        </ion-select>\n        </ion-item>\n\n      <ion-item >\n        <ion-label  position=\"stacked\">Dinner:-<ion-text color=\"danger\">*</ion-text></ion-label>\n        <ion-select value=\"Roti\" multiple=\"true\" formControlName=\"Dinner\">\n          <ion-select-option value=\"Roti\" >Roti</ion-select-option>\n          <ion-select-option value=\"Rice\" >Rice</ion-select-option>\n          <ion-select-option value=\"Sabji\" >Sabji</ion-select-option>\n          <ion-select-option value=\"Dal\" >Dal</ion-select-option>\n          <ion-select-option value=\"Fish\" >Fish</ion-select-option>\n          <ion-select-option value=\"Chicken\" >Chicken</ion-select-option>\n          <ion-select-option value=\"Meat\" >Meat</ion-select-option>\n          <ion-select-option value=\"Sweets\" >Sweets</ion-select-option>\n          <ion-select-option value=\"Pulses\" >Pulses</ion-select-option>\n          <ion-select-option value=\"Khichdi\" >Khichdi</ion-select-option>\n          <ion-select-option value=\"Burger\" >Burger</ion-select-option>\n          <ion-select-option value=\"junkfood\" >Junk Food</ion-select-option>\n          <ion-select-option value=\"Farshan\" >Farshan</ion-select-option>\n        </ion-select>\n        </ion-item>\n         <p class=\"textaligncenter\">Question And Answer</p>\n         <ion-item >\n          <ion-label  position=\"stacked\">Q.1)Food Habbit Type:-</ion-label>\n          <ion-radio-group value=\"Veg\" formControlName=\"ansdetailQ30\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Veg\"></ion-radio><span class=\"fontsize12px\">Veg </span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Non Veg \"></ion-radio><span class=\"fontsize12px\">Non Veg</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Jain\"></ion-radio><span class=\"fontsize12px\">Jain </span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Eggetarian\"></ion-radio><span class=\"fontsize12px\">Eggetarian </span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.2) Carbohydrate Intake:-</ion-label>\n          <ion-radio-group value=\"Adequate\" formControlName=\"ansdetailQ120\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Adequate\"></ion-radio><span class=\"fontsize12px\">Adequate</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Inadequate \"></ion-radio><span class=\"fontsize12px\">Inadequate</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Normal\"></ion-radio><span class=\"fontsize12px\">More intake then required</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.3) Protein Intake:-</ion-label>\n          <ion-radio-group value=\"Adequate\" formControlName=\"ansdetailQ121\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Adequate\"></ion-radio><span class=\"fontsize12px\">Adequate</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Inadequate \"></ion-radio><span class=\"fontsize12px\">Inadequate</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Normal\"></ion-radio><span class=\"fontsize12px\">More intake then required</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.4) Fats Intake:-</ion-label>\n          <ion-radio-group value=\"Adequate\" formControlName=\"ansdetailQ122\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Adequate\"></ion-radio><span class=\"fontsize12px\">Adequate</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Inadequate \"></ion-radio><span class=\"fontsize12px\">Inadequate</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Normal\"></ion-radio><span class=\"fontsize12px\">More intake then required</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.5) Minerals Intake:-</ion-label>\n          <ion-radio-group value=\"Adequate\" formControlName=\"ansdetailQ123\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Adequate\"></ion-radio><span class=\"fontsize12px\">Adequate</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Inadequate \"></ion-radio><span class=\"fontsize12px\">Inadequate</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Normal\"></ion-radio><span class=\"fontsize12px\">More intake then required</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.6) Vitamin Intake:-</ion-label>\n          <ion-radio-group value=\"Adequate\" formControlName=\"ansdetailQ124\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Adequate\"></ion-radio><span class=\"fontsize12px\">Adequate</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Inadequate \"></ion-radio><span class=\"fontsize12px\">Inadequate</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Normal\"></ion-radio><span class=\"fontsize12px\">More intake then required</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.7) Is Student Obese:-</ion-label>\n          <ion-radio-group value=\"Yes\" formControlName=\"ansdetailQ125\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Yes\"></ion-radio><span class=\"fontsize12px\">Yes</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"No \"></ion-radio><span class=\"fontsize12px\">No</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.8) Is Student Undernourished:-</ion-label>\n          <ion-radio-group value=\"Yes\" formControlName=\"ansdetailQ126\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Yes\"></ion-radio><span class=\"fontsize12px\">Yes</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"No \"></ion-radio><span class=\"fontsize12px\">No</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.9) Is Student Overfed but undernourished:-</ion-label>\n          <ion-radio-group value=\"Yes\" formControlName=\"ansdetailQ127\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Yes\"></ion-radio><span class=\"fontsize12px\">Yes</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"No \"></ion-radio><span class=\"fontsize12px\">No</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.10) Is Fiber content in meals adequate:-</ion-label>\n          <ion-radio-group value=\"Yes\" formControlName=\"ansdetailQ128\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Yes\"></ion-radio><span class=\"fontsize12px\">Yes</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"No \"></ion-radio><span class=\"fontsize12px\">No</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\" >Q.11) Any other nutritional disease:-</ion-label>\n          <ion-radio-group value=\"No\" formControlName=\"ansdetailQ129\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Yes\"></ion-radio><span class=\"fontsize12px\">Yes</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"No\"></ion-radio><span class=\"fontsize12px\">No</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\" >Q.12) No. of meals consumed in a day?:-</ion-label>\n          <ion-radio-group value=\"2\" formControlName=\"ansdetailQ31\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"2\"></ion-radio><span class=\"fontsize12px\">2</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"3\"></ion-radio><span class=\"fontsize12px\">3</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"4\"></ion-radio><span class=\"fontsize12px\">4</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"5\"></ion-radio><span class=\"fontsize12px\">5</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\" >Q.13) Daily Water Intake:-</ion-label>\n          <ion-radio-group value=\"4\" formControlName=\"ansdetailQ90\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"4\"></ion-radio><span class=\"fontsize12px\">4</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"8\"></ion-radio><span class=\"fontsize12px\">8</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\" >Q.14) Outdoor activity:-</ion-label>\n          <ion-radio-group value=\"Yes\" formControlName=\"ansdetailQ400\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Yes\"></ion-radio><span class=\"fontsize12px\">Yes</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"No \"></ion-radio><span class=\"fontsize12px\">No</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\" >Q.15) Body type:-</ion-label>\n          <ion-radio-group value=\"Normal\" formControlName=\"ansdetailQ225\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Thin\"></ion-radio><span class=\"fontsize12px\">Thin</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Normal\"></ion-radio><span class=\"fontsize12px\">Normal</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Healthy\"></ion-radio><span class=\"fontsize12px\">Healthy</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Obese\"></ion-radio><span class=\"fontsize12px\">Obese</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\" >Q.16) Junk Food Intake:-</ion-label>\n          <ion-radio-group value=\"Yes\" formControlName=\"ansdetailQ89\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Yes\"></ion-radio><span class=\"fontsize12px\">Yes</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"No \"></ion-radio><span class=\"fontsize12px\">No</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        \n       \n\n                  <ion-item lines=\"none\">\n                    <ion-button class=\"centerblock\" type=\"submit\" color=\"tertiary\" Expand=\"block\"\n                      size=\"default\" (click)=\"submit()\">Submit</ion-button>\n                  </ion-item>\n                </form>\n        </ion-list>\n        </div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/dietform/dietform-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/dietform/dietform-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: DietformPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DietformPageRoutingModule", function() { return DietformPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _dietform_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dietform.page */ "./src/app/dietform/dietform.page.ts");




var routes = [
    {
        path: '',
        component: _dietform_page__WEBPACK_IMPORTED_MODULE_3__["DietformPage"]
    }
];
var DietformPageRoutingModule = /** @class */ (function () {
    function DietformPageRoutingModule() {
    }
    DietformPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], DietformPageRoutingModule);
    return DietformPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/dietform/dietform.module.ts":
/*!*********************************************!*\
  !*** ./src/app/dietform/dietform.module.ts ***!
  \*********************************************/
/*! exports provided: DietformPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DietformPageModule", function() { return DietformPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _dietform_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dietform-routing.module */ "./src/app/dietform/dietform-routing.module.ts");
/* harmony import */ var _dietform_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dietform.page */ "./src/app/dietform/dietform.page.ts");







var DietformPageModule = /** @class */ (function () {
    function DietformPageModule() {
    }
    DietformPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _dietform_routing_module__WEBPACK_IMPORTED_MODULE_5__["DietformPageRoutingModule"]
            ],
            declarations: [_dietform_page__WEBPACK_IMPORTED_MODULE_6__["DietformPage"]]
        })
    ], DietformPageModule);
    return DietformPageModule;
}());



/***/ }),

/***/ "./src/app/dietform/dietform.page.scss":
/*!*********************************************!*\
  !*** ./src/app/dietform/dietform.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RpZXRmb3JtL2RpZXRmb3JtLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/dietform/dietform.page.ts":
/*!*******************************************!*\
  !*** ./src/app/dietform/dietform.page.ts ***!
  \*******************************************/
/*! exports provided: DietformPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DietformPage", function() { return DietformPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../api/form/form.service */ "./src/app/api/form/form.service.ts");







var DietformPage = /** @class */ (function () {
    function DietformPage(formBuilder, toastCtrl, router, storage, _form, menuCtrl, _Activatedroute, loadingCtrl) {
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.storage = storage;
        this._form = _form;
        this.menuCtrl = menuCtrl;
        this._Activatedroute = _Activatedroute;
        this.loadingCtrl = loadingCtrl;
        this.DietForm = this.formBuilder.group({
            Breakfast: ['Milk'],
            lunch: ['Roti'],
            Evening: ['Milk'],
            Dinner: ['Milk'],
            ansdetailQ120: ['Adequate'],
            ansdetailQ121: ['Adequate'],
            ansdetailQ122: ['Adequate'],
            ansdetailQ123: ['Adequate'],
            ansdetailQ124: ['Adequate'],
            ansdetailQ125: ['Yes'],
            ansdetailQ126: ['Yes'],
            ansdetailQ127: ['Yes'],
            ansdetailQ128: ['Yes'],
            ansdetailQ129: ['Yes'],
            ansdetailQ30: ['Veg'],
            ansdetailQ31: ['2'],
            ansdetailQ90: ['4'],
            ansdetailQ400: ['Yes'],
            ansdetailQ225: ['Thin'],
            ansdetailQ89: ['Yes'],
        });
    }
    DietformPage.prototype.ngOnInit = function () {
        this.id = this._Activatedroute.snapshot.paramMap.get('id');
        this.firstname = this._Activatedroute.snapshot.paramMap.get('firstname');
        this.lastname = this._Activatedroute.snapshot.paramMap.get('lastname');
        this.consoluentid = this._Activatedroute.snapshot.paramMap.get('consulentid');
        this.event_id = this._Activatedroute.snapshot.paramMap.get('event_id');
        this.studentclass = this._Activatedroute.snapshot.paramMap.get('studentclass');
        this.studentrollno = this._Activatedroute.snapshot.paramMap.get('studentrollno');
        this.studentdiv = this._Activatedroute.snapshot.paramMap.get('studentdiv');
    };
    DietformPage.prototype.createRange = function (number) {
        var items = [];
        for (var i = 1; i <= number; i++) {
            items.push(i);
        }
        return items;
    };
    DietformPage.prototype.createparticularRange = function (start, number) {
        var items = [];
        for (var i = start; i <= number; i++) {
            items.push(i);
        }
        return items;
    };
    Object.defineProperty(DietformPage.prototype, "Breakfast", {
        get: function () {
            return this.DietForm.get("Breakfast");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "lunch", {
        get: function () {
            return this.DietForm.get("lunch");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "Evening", {
        get: function () {
            return this.DietForm.get("Evening");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "Dinner", {
        get: function () {
            return this.DietForm.get("Dinner");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "ansdetailQ120", {
        get: function () {
            return this.DietForm.get("ansdetailQ120");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "ansdetailQ121", {
        get: function () {
            return this.DietForm.get("ansdetailQ121");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "ansdetailQ122", {
        get: function () {
            return this.DietForm.get("ansdetailQ122");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "ansdetailQ123", {
        get: function () {
            return this.DietForm.get("ansdetailQ123");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "ansdetailQ124", {
        get: function () {
            return this.DietForm.get("ansdetailQ124");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "ansdetailQ125", {
        get: function () {
            return this.DietForm.get("ansdetailQ125");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "ansdetailQ126", {
        get: function () {
            return this.DietForm.get("ansdetailQ126");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "ansdetailQ127", {
        get: function () {
            return this.DietForm.get("ansdetailQ127");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "ansdetailQ128", {
        get: function () {
            return this.DietForm.get("ansdetailQ128");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "ansdetailQ129", {
        get: function () {
            return this.DietForm.get("ansdetailQ129");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "ansdetailQ30", {
        get: function () {
            return this.DietForm.get("ansdetailQ30");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "ansdetailQ31", {
        get: function () {
            return this.DietForm.get("ansdetailQ31");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "ansdetailQ90", {
        get: function () {
            return this.DietForm.get("ansdetailQ90");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "ansdetailQ400", {
        get: function () {
            return this.DietForm.get("ansdetailQ400");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "ansdetailQ225", {
        get: function () {
            return this.DietForm.get("ansdetailQ225");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DietformPage.prototype, "ansdetailQ89", {
        get: function () {
            return this.DietForm.get("ansdetailQ89");
        },
        enumerable: true,
        configurable: true
    });
    DietformPage.prototype.submit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.messages = ['Q120', 'Q121', 'Q122', 'Q123', 'Q124', 'Q125', 'Q126', 'Q127', 'Q128', 'Q129', 'Q30', 'Q31', 'Q90', 'Q400', 'Q225', 'Q89'];
                        this._form.diet(this.DietForm.value, this.id, this.messages, this.consoluentid, this.event_id)
                            .subscribe(function (data) {
                            loading.dismiss();
                            if (data[0]['success'] == '1') {
                                alert("Data Enter Successfully");
                                _this.router.navigate(['/medicalcamp']);
                            }
                            else {
                                alert(data[0]['msg']);
                                _this.DietForm.reset();
                            }
                        }, function (error) { _this.errorMsg = error.success; });
                        return [2 /*return*/];
                }
            });
        });
    };
    DietformPage.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
        { type: _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__["FormService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] }
    ]; };
    DietformPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dietform',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./dietform.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/dietform/dietform.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./dietform.page.scss */ "./src/app/dietform/dietform.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
            _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__["FormService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]])
    ], DietformPage);
    return DietformPage;
}());



/***/ })

}]);
//# sourceMappingURL=dietform-dietform-module.js.map