(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["physician-physician-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/physician/physician.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/physician/physician.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\n      <p><b>Physician</b></p>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div>\n    <div class=\"borderstudentlist\">\n      <h3 class=\"textaligncenter fontsize15px\"><u>Student Information</u></h3>\n      <ion-row>\n        <ion-col col-6> <p class=\"studentlist\">Name/Id:</p></ion-col>\n        <ion-col col-6><p class=\"studentlist\">{{firstname}} {{lastname}}/{{id}}</p></ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6><p class=\"studentlist\">Class/Roll No/ Div:</p></ion-col>\n        <ion-col col-6><p class=\"studentlist\">{{studentclass}}/{{studentrollno}}/{{studentdiv}}</p></ion-col>\n      </ion-row>\n    </div>\n    <ion-list padding>\n      <form [formGroup]=\"PhysicianForm\" >\n      <ion-item >\n        <ion-label  position=\"stacked\">Resp.rate/min:-</ion-label>\n        <ion-select value=\"24\" formControlName=\"resprate\" (ionChange)=\"getweight($event)\">\n          <ion-select-option value=\"24\" selected>24</ion-select-option>\n          <ion-select-option value=\"23\" selected>23</ion-select-option>\n          <ion-select-option *ngFor=\"let item of createparticularRange(10,35); let currentElementIndex=index\">{{currentElementIndex +10}}</ion-select-option>\n        </ion-select>\n        </ion-item>\n\n      <ion-item >\n        <ion-label  position=\"stacked\">Height:-(Cms)<ion-text color=\"danger\">*</ion-text></ion-label>\n        <ion-select value=\"121\" formControlName=\"height\" (ionChange)=\"getheight($event)\">\n          <ion-select-option value=\"121\" selected>121</ion-select-option>\n          <ion-select-option value=\"122\" selected>122</ion-select-option>\n          <ion-select-option *ngFor=\"let item of createparticularRange(30,210); let currentElementIndex=index\">{{currentElementIndex +30}}</ion-select-option>\n        </ion-select>\n        </ion-item>\n\n      <ion-item >\n        <ion-label  position=\"stacked\">Weight:-(Kgs)<ion-text color=\"danger\">*</ion-text></ion-label>\n        <ion-select value=\"41\" formControlName=\"weight\">\n          <ion-select-option value=\"41\" selected>41</ion-select-option>\n          <ion-select-option value=\"42\" selected>42</ion-select-option>\n          <ion-select-option *ngFor=\"let item of createparticularRange(5,120); let currentElementIndex=index\">{{currentElementIndex +5}}</ion-select-option>\n        </ion-select>\n        </ion-item>\n\n      <ion-item >\n        <ion-label  position=\"stacked\">BMI</ion-label>\n        <ion-input clearInput value=\"{{bmidata}}\" readonly=\"\" formControlName=\"bmi\"></ion-input>\n        </ion-item>\n\n        <ion-item >\n          <ion-label  position=\"stacked\">Temprature:-(F)</ion-label>\n          <ion-select value=\"98\" formControlName=\"temp\">\n            <ion-select-option value=\"98\" selected>98</ion-select-option>\n            <ion-select-option value=\"99\" selected>99</ion-select-option>\n            <ion-select-option *ngFor=\"let item of createparticularRange(95,104); let currentElementIndex=index\">{{currentElementIndex +95}}</ion-select-option>\n          </ion-select>\n          </ion-item>\n\n          <ion-item >\n            <ion-label  position=\"stacked\">Blood Pressure:-</ion-label>\n            <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\" >\n            <ion-select value=\"80\" class=\"max-width\" formControlName=\"systolic\">\n              <ion-select-option value=\"80\" selected>80</ion-select-option>\n              <ion-select-option value=\"81\" selected>81</ion-select-option>\n              <ion-select-option *ngFor=\"let item of createparticularRange(60,120); let currentElementIndex=index\">{{currentElementIndex + 60}}</ion-select-option>\n            </ion-select>\n          </ion-col>\n              <ion-col size=\"6\">\n            <ion-select value=\"120\" class=\"max-width\" formControlName=\"diastolic\">\n              <ion-select-option value=\"120\" selected>120</ion-select-option>\n              <ion-select-option value=\"121\" selected>121</ion-select-option>\n              <ion-select-option *ngFor=\"let item of createparticularRange(75,200); let currentElementIndex=index\">{{currentElementIndex + 75}}</ion-select-option>\n            </ion-select>\n          </ion-col>\n          </ion-row>\n        </ion-grid>\n            </ion-item>\n\n\n            <ion-item >\n              <ion-label  position=\"stacked\">Pulse:-(/min)</ion-label>\n              <ion-select value=\"72\" formControlName=\"pulse\">\n                <ion-select-option value=\"72\" selected>72</ion-select-option>\n                <ion-select-option value=\"73\" selected>73</ion-select-option>\n                <ion-select-option *ngFor=\"let item of createparticularRange(50,100); let currentElementIndex=index\">{{currentElementIndex + 50}}</ion-select-option>\n              </ion-select>\n              </ion-item>\n\n\n              <ion-item >\n                <ion-label  position=\"stacked\">Blood Groop:-<ion-text color=\"danger\">*</ion-text></ion-label>\n                <ion-select formControlName=\"bloodgp\" value=\"NA\">\n                  <ion-select-option value=\"A+\" >A+</ion-select-option>\n                  <ion-select-option value=\"98\" >A-</ion-select-option>\n                  <ion-select-option value=\"B+\" >B+</ion-select-option>\n                  <ion-select-option value=\"B-\" >B-</ion-select-option>\n                  <ion-select-option value=\"AB+\" >AB+</ion-select-option>\n                  <ion-select-option value=\"AB-\" >AB-</ion-select-option>\n                  <ion-select-option value=\"O=\" >O+</ion-select-option>\n                  <ion-select-option value=\"O-\" >O-</ion-select-option>\n                </ion-select>\n                </ion-item>\n\n\n                <ion-item >\n                  <ion-label  position=\"stacked\">Blood Sugar:-(mg/dl)</ion-label>\n                  <ion-select value=\"0\" formControlName=\"bloodsugar\">\n                    <ion-select-option value=\"94\" selected>94</ion-select-option>\n              <ion-select-option value=\"95\" selected>95</ion-select-option>\n              <ion-select-option *ngFor=\"let item of createparticularRange(70,300); let currentElementIndex=index\">{{currentElementIndex + 70}}</ion-select-option>\n            </ion-select>\n                 \n                  </ion-item>\n\n                  <ion-item lines=\"none\">\n                    <ion-button class=\"centerblock\" type=\"submit\" color=\"tertiary\" Expand=\"block\"\n                      size=\"default\" (click)=\"submit()\">Submit</ion-button>\n                  </ion-item>\n                  </form>\n        </ion-list>\n        </div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/physician/physician-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/physician/physician-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: PhysicianPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhysicianPageRoutingModule", function() { return PhysicianPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _physician_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./physician.page */ "./src/app/physician/physician.page.ts");




var routes = [
    {
        path: '',
        component: _physician_page__WEBPACK_IMPORTED_MODULE_3__["PhysicianPage"]
    }
];
var PhysicianPageRoutingModule = /** @class */ (function () {
    function PhysicianPageRoutingModule() {
    }
    PhysicianPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], PhysicianPageRoutingModule);
    return PhysicianPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/physician/physician.module.ts":
/*!***********************************************!*\
  !*** ./src/app/physician/physician.module.ts ***!
  \***********************************************/
/*! exports provided: PhysicianPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhysicianPageModule", function() { return PhysicianPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _physician_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./physician-routing.module */ "./src/app/physician/physician-routing.module.ts");
/* harmony import */ var _physician_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./physician.page */ "./src/app/physician/physician.page.ts");







var PhysicianPageModule = /** @class */ (function () {
    function PhysicianPageModule() {
    }
    PhysicianPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _physician_routing_module__WEBPACK_IMPORTED_MODULE_5__["PhysicianPageRoutingModule"]
            ],
            declarations: [_physician_page__WEBPACK_IMPORTED_MODULE_6__["PhysicianPage"]]
        })
    ], PhysicianPageModule);
    return PhysicianPageModule;
}());



/***/ }),

/***/ "./src/app/physician/physician.page.scss":
/*!***********************************************!*\
  !*** ./src/app/physician/physician.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".max-width {\n  max-width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGh5c2ljaWFuL0M6XFxpb25pY1xca3J1c2huYW1haVxcTmV3IGZvbGRlci9zcmNcXGFwcFxccGh5c2ljaWFuXFxwaHlzaWNpYW4ucGFnZS5zY3NzIiwic3JjL2FwcC9waHlzaWNpYW4vcGh5c2ljaWFuLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGVBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3BoeXNpY2lhbi9waHlzaWNpYW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1heC13aWR0aHtcclxuICAgIG1heC13aWR0aDogMTAwJTtcclxufSIsIi5tYXgtd2lkdGgge1xuICBtYXgtd2lkdGg6IDEwMCU7XG59Il19 */");

/***/ }),

/***/ "./src/app/physician/physician.page.ts":
/*!*********************************************!*\
  !*** ./src/app/physician/physician.page.ts ***!
  \*********************************************/
/*! exports provided: PhysicianPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhysicianPage", function() { return PhysicianPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../api/form/form.service */ "./src/app/api/form/form.service.ts");







var PhysicianPage = /** @class */ (function () {
    function PhysicianPage(formBuilder, toastCtrl, router, storage, _form, menuCtrl, _Activatedroute, loadingCtrl) {
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.storage = storage;
        this._form = _form;
        this.menuCtrl = menuCtrl;
        this._Activatedroute = _Activatedroute;
        this.loadingCtrl = loadingCtrl;
        this.weightdata = "121";
        this.heightdata = "41";
        this.bmidata = 224;
        this.PhysicianForm = this.formBuilder.group({
            resprate: ['24'],
            height: ['121'],
            weight: ['41'],
            bmi: ['224'],
            temp: ['98'],
            systolic: ['80'],
            diastolic: ['120'],
            pulse: ['72'],
            bloodsugar: ['0'],
            bloodgp: ['NA']
        });
    }
    PhysicianPage.prototype.ngOnInit = function () {
        this.id = this._Activatedroute.snapshot.paramMap.get('id');
        this.consoluentid = this._Activatedroute.snapshot.paramMap.get('consulentid');
        this.firstname = this._Activatedroute.snapshot.paramMap.get('firstname');
        this.lastname = this._Activatedroute.snapshot.paramMap.get('lastname');
        this.event_id = this._Activatedroute.snapshot.paramMap.get('event_id');
        this.studentclass = this._Activatedroute.snapshot.paramMap.get('studentclass');
        this.studentrollno = this._Activatedroute.snapshot.paramMap.get('studentrollno');
        this.studentdiv = this._Activatedroute.snapshot.paramMap.get('studentdiv');
    };
    PhysicianPage.prototype.createRange = function (number) {
        var items = [];
        for (var i = 1; i <= number; i++) {
            items.push(i);
        }
        return items;
    };
    PhysicianPage.prototype.createparticularRange = function (start, number) {
        var items = [];
        for (var i = start; i <= number; i++) {
            items.push(i);
        }
        return items;
    };
    PhysicianPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(false);
    };
    Object.defineProperty(PhysicianPage.prototype, "resprate", {
        get: function () {
            return this.PhysicianForm.get("resprate");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PhysicianPage.prototype, "height", {
        get: function () {
            return this.PhysicianForm.get("height");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PhysicianPage.prototype, "weight", {
        get: function () {
            return this.PhysicianForm.get("weight");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PhysicianPage.prototype, "bmi", {
        get: function () {
            return this.PhysicianForm.get("bmi");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PhysicianPage.prototype, "temp", {
        get: function () {
            return this.PhysicianForm.get("temp");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PhysicianPage.prototype, "systolic", {
        get: function () {
            return this.PhysicianForm.get("systolic");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PhysicianPage.prototype, "diastolic", {
        get: function () {
            return this.PhysicianForm.get("diastolic");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PhysicianPage.prototype, "pulse", {
        get: function () {
            return this.PhysicianForm.get("pulse");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PhysicianPage.prototype, "bloodsugar", {
        get: function () {
            return this.PhysicianForm.get("bloodsugar");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PhysicianPage.prototype, "bloodgp", {
        get: function () {
            return this.PhysicianForm.get("bloodgp");
        },
        enumerable: true,
        configurable: true
    });
    PhysicianPage.prototype.submit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this._form.vitalphysican(this.PhysicianForm.value, this.id, this.consoluentid, this.event_id)
                            .subscribe(function (data) {
                            loading.dismiss();
                            if (data['success'] == '1') {
                                alert("Data Enter Successfully");
                                _this.router.navigate(['/medicalcamp']);
                            }
                            else {
                                alert(data['msg']);
                                _this.PhysicianForm.reset();
                            }
                        }, function (error) { _this.errorMsg = error.success; });
                        return [2 /*return*/];
                }
            });
        });
    };
    PhysicianPage.prototype.getweight = function ($event) {
        this.weightdata = $event.target.value;
        var h2 = this.heightdata * this.heightdata;
        var h3 = h2 / 100;
        this.bmidata = this.weightdata / h3;
    };
    PhysicianPage.prototype.getheight = function ($event) {
        this.heightdata = $event.target.value;
        var h2 = this.heightdata * this.heightdata;
        var h3 = h2 / 100;
        this.bmidata = this.weightdata / h3;
    };
    PhysicianPage.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
        { type: _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__["FormService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] }
    ]; };
    PhysicianPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-physician',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./physician.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/physician/physician.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./physician.page.scss */ "./src/app/physician/physician.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
            _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__["FormService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]])
    ], PhysicianPage);
    return PhysicianPage;
}());



/***/ })

}]);
//# sourceMappingURL=physician-physician-module.js.map