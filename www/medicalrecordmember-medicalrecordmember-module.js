(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["medicalrecordmember-medicalrecordmember-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/medicalrecordmember/medicalrecordmember.page.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/medicalrecordmember/medicalrecordmember.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/memberdashboard\"></ion-back-button>\n      <p ><b>Medical Record</b></p>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"margintop20\">\n    <ion-card class=\"boxshadowcard\" >\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"10\" >\n              <p \n                class=\"colorblackcategory floatleft paddingzero marginzero fontsize15px  textaligncenter\">Physician</p>\n        </ion-col>\n        <ion-col size=\"2\"   [routerLink]=\"['/medicalrecordsdetails','Physician']\">\n         <img src=\"assets/imgs/arrowup.svg\"  class=\"arrowup\"/>\n    </ion-col>\n        \n      </ion-row>\n      </ion-grid>\n    </ion-card>\n    <ion-card class=\"boxshadowcard\" >\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"10\" >\n              <p \n                class=\"colorblackcategory floatleft paddingzero marginzero fontsize15px  textaligncenter\">Eye</p>\n        </ion-col>\n        <ion-col size=\"2\"   [routerLink]=\"['/medicalrecordsdetails','Eye']\">\n         <img src=\"assets/imgs/arrowup.svg\"  class=\"arrowup\"/>\n    </ion-col>\n        \n      </ion-row>\n      </ion-grid>\n    </ion-card>\n    <ion-card class=\"boxshadowcard\" >\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"10\" >\n              <p \n                class=\"colorblackcategory floatleft paddingzero marginzero fontsize15px  textaligncenter\">Dental</p>\n        </ion-col>\n        <ion-col size=\"2\"  [routerLink]=\"['/medicalrecordsdetails','dental']\">\n         <img src=\"assets/imgs/arrowup.svg\"  class=\"arrowup\"/>\n    </ion-col>\n        \n      </ion-row>\n      </ion-grid>\n    </ion-card>\n    <ion-card class=\"boxshadowcard\" >\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"10\" >\n              <p \n                class=\"colorblackcategory floatleft paddingzero marginzero fontsize15px  textaligncenter\">Diet</p>\n        </ion-col>\n        <ion-col size=\"2\"   [routerLink]=\"['/medicalrecordsdetails','diet']\">\n         <img src=\"assets/imgs/arrowup.svg\"  class=\"arrowup\"/>\n    </ion-col>\n        \n      </ion-row>\n      </ion-grid>\n    </ion-card>\n    <ion-card class=\"boxshadowcard\" >\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"10\" >\n              <p \n                class=\"colorblackcategory floatleft paddingzero marginzero fontsize15px  textaligncenter\">Mental Health</p>\n        </ion-col>\n        <ion-col size=\"2\"   [routerLink]=\"['/medicalrecordsdetails','mentalhealth']\">\n         <img src=\"assets/imgs/arrowup.svg\"  class=\"arrowup\"/>\n    </ion-col>\n        \n      </ion-row>\n      </ion-grid>\n    </ion-card>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/medicalrecordmember/medicalrecordmember-routing.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/medicalrecordmember/medicalrecordmember-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: MedicalrecordmemberPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MedicalrecordmemberPageRoutingModule", function() { return MedicalrecordmemberPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _medicalrecordmember_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./medicalrecordmember.page */ "./src/app/medicalrecordmember/medicalrecordmember.page.ts");




var routes = [
    {
        path: '',
        component: _medicalrecordmember_page__WEBPACK_IMPORTED_MODULE_3__["MedicalrecordmemberPage"]
    }
];
var MedicalrecordmemberPageRoutingModule = /** @class */ (function () {
    function MedicalrecordmemberPageRoutingModule() {
    }
    MedicalrecordmemberPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], MedicalrecordmemberPageRoutingModule);
    return MedicalrecordmemberPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/medicalrecordmember/medicalrecordmember.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/medicalrecordmember/medicalrecordmember.module.ts ***!
  \*******************************************************************/
/*! exports provided: MedicalrecordmemberPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MedicalrecordmemberPageModule", function() { return MedicalrecordmemberPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _medicalrecordmember_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./medicalrecordmember-routing.module */ "./src/app/medicalrecordmember/medicalrecordmember-routing.module.ts");
/* harmony import */ var _medicalrecordmember_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./medicalrecordmember.page */ "./src/app/medicalrecordmember/medicalrecordmember.page.ts");







var MedicalrecordmemberPageModule = /** @class */ (function () {
    function MedicalrecordmemberPageModule() {
    }
    MedicalrecordmemberPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _medicalrecordmember_routing_module__WEBPACK_IMPORTED_MODULE_5__["MedicalrecordmemberPageRoutingModule"]
            ],
            declarations: [_medicalrecordmember_page__WEBPACK_IMPORTED_MODULE_6__["MedicalrecordmemberPage"]]
        })
    ], MedicalrecordmemberPageModule);
    return MedicalrecordmemberPageModule;
}());



/***/ }),

/***/ "./src/app/medicalrecordmember/medicalrecordmember.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/medicalrecordmember/medicalrecordmember.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".colorblackcategory {\n  color: #0F0F0F;\n}\n\n.arrowup {\n  width: 32px;\n  height: 18px;\n}\n\n.margintop20 {\n  margin-top: 5%;\n}\n\n.margin10px {\n  margin: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWVkaWNhbHJlY29yZG1lbWJlci9DOlxcaW9uaWNcXGtydXNobmFtYWlcXE5ldyBmb2xkZXIvc3JjXFxhcHBcXG1lZGljYWxyZWNvcmRtZW1iZXJcXG1lZGljYWxyZWNvcmRtZW1iZXIucGFnZS5zY3NzIiwic3JjL2FwcC9tZWRpY2FscmVjb3JkbWVtYmVyL21lZGljYWxyZWNvcmRtZW1iZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksY0FBQTtBQ0NKOztBRENBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUNFSjs7QURDQTtFQUNJLGNBQUE7QUNFSjs7QURBQTtFQUNJLFlBQUE7QUNHSiIsImZpbGUiOiJzcmMvYXBwL21lZGljYWxyZWNvcmRtZW1iZXIvbWVkaWNhbHJlY29yZG1lbWJlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29sb3JibGFja2NhdGVnb3J5e1xyXG4gICAgY29sb3I6ICMwRjBGMEY7XHJcbn1cclxuLmFycm93dXB7XHJcbiAgICB3aWR0aDogMzJweDtcclxuICAgIGhlaWdodDogMThweDtcclxufVxyXG5cclxuLm1hcmdpbnRvcDIwe1xyXG4gICAgbWFyZ2luLXRvcDogNSU7XHJcbn1cclxuLm1hcmdpbjEwcHh7XHJcbiAgICBtYXJnaW46IDEwcHg7XHJcbn0iLCIuY29sb3JibGFja2NhdGVnb3J5IHtcbiAgY29sb3I6ICMwRjBGMEY7XG59XG5cbi5hcnJvd3VwIHtcbiAgd2lkdGg6IDMycHg7XG4gIGhlaWdodDogMThweDtcbn1cblxuLm1hcmdpbnRvcDIwIHtcbiAgbWFyZ2luLXRvcDogNSU7XG59XG5cbi5tYXJnaW4xMHB4IHtcbiAgbWFyZ2luOiAxMHB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/medicalrecordmember/medicalrecordmember.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/medicalrecordmember/medicalrecordmember.page.ts ***!
  \*****************************************************************/
/*! exports provided: MedicalrecordmemberPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MedicalrecordmemberPage", function() { return MedicalrecordmemberPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MedicalrecordmemberPage = /** @class */ (function () {
    function MedicalrecordmemberPage() {
    }
    MedicalrecordmemberPage.prototype.ngOnInit = function () {
    };
    MedicalrecordmemberPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-medicalrecordmember',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./medicalrecordmember.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/medicalrecordmember/medicalrecordmember.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./medicalrecordmember.page.scss */ "./src/app/medicalrecordmember/medicalrecordmember.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MedicalrecordmemberPage);
    return MedicalrecordmemberPage;
}());



/***/ })

}]);
//# sourceMappingURL=medicalrecordmember-medicalrecordmember-module.js.map