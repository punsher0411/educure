(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["medicalrecordsdetails-medicalrecordsdetails-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/medicalrecordsdetails/medicalrecordsdetails.page.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/medicalrecordsdetails/medicalrecordsdetails.page.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/medicalrecordmember\"></ion-back-button>\n      <p ><b>Medical Record Details</b></p>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <p class=\"textaligncenter fontsize16px\">You Selected: <b>{{this.id}}</b></p>\n  <div *ngIf=\"this.id == 'dental'\">\n    <ion-segment [(ngModel)]=\"medicalrecord\">\n      <ion-segment-button value=\"Information\" class=\" fontsize12px colorblue texttransform\">\n        Information\n      </ion-segment-button>\n      <ion-segment-button value=\"Assessment\"   class=\" fontsize12px colorblue texttransform\">\n        Assessment\n      </ion-segment-button>\n    </ion-segment>\n    <div [ngSwitch]=\"medicalrecord\">\n      <ion-list *ngSwitchCase=\"'Information'\" >\n  <ion-card *ngFor=\"let r of recorddata;\">\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <p class=\"marginzero   fontsize11px  fontfamilymon floatright\">Date:{{r.Date}}</p>\n        </ion-col>\n       \n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Decayed:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.decayed}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Missing:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.missing}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Fillingcrown:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.fillingcrown}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Gingiva/Colour:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.gingiva}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Appearance:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.appearance}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Event Date:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.event_date}}</b></p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-card  *ngIf=\"!recorddata?.length > 0\">\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <p class=\"marginzero   fontsize13px  fontfamilymon  textaligncenter\">There Is No Information</p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  </ion-list>\n  <ion-list *ngSwitchCase=\"'Assessment'\" >\n    <ion-card *ngFor=\"let reord of recorddataqueans;\">\n      <ion-grid *ngFor=\"let r of reord.valuedate;\">\n        <ion-row>\n          <ion-col  size=\"12\">\n            <p class=\"marginzero   fontsize16px fontfamilymon\">Q:{{r.que_detail}}</p>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col  size=\"12\">\n            <p class=\"marginzero   fontsize16px fontfamilymon\">A:{{r.ans_code}}</p>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n    <ion-card  *ngIf=\"!recorddataqueans?.length > 0\">\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <p class=\"marginzero   fontsize13px  fontfamilymon  textaligncenter\">There Is No Assessment</p>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n  </ion-list>\n  </div>\n</div>\n\n\n<div *ngIf=\"this.id == 'Physician'\">\n  <ion-segment [(ngModel)]=\"medicalrecord\">\n    <ion-segment-button value=\"Information\" class=\" fontsize12px colorblue texttransform\">\n      Information\n    </ion-segment-button>\n    <ion-segment-button value=\"Assessment\"   class=\" fontsize12px colorblue texttransform\">\n      Assessment\n    </ion-segment-button>\n  </ion-segment>\n  <div [ngSwitch]=\"medicalrecord\">\n    <ion-list *ngSwitchCase=\"'Information'\" >\n  <ion-card *ngFor=\"let r of recorddata;\">\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <p class=\"marginzero   fontsize11px  fontfamilymon floatright\">Date:{{r.entry_date}}</p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Height(cms):</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.height}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Weight(kgs):</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.weight}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Bmi:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.bmi}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Pulse:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.pulse}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Respiratory:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.resprate}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Blood Group:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.bloodgp}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Event Date:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.event_date}}</b></p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  </ion-list>\n  <ion-list *ngSwitchCase=\"'Assessment'\" >\n    <ion-card *ngFor=\"let reord of recorddataqueans;\">\n      <ion-grid *ngFor=\"let r of reord.valuedate;\">\n        <ion-row>\n          <ion-col  size=\"12\">\n            <p class=\"marginzero   fontsize16px fontfamilymon\">Q:{{r.que_detail}}</p>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col  size=\"12\">\n            <p class=\"marginzero   fontsize16px fontfamilymon\">A:{{r.ans_code}}</p>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n    <ion-card  *ngIf=\"!recorddataqueans?.length > 0\">\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <p class=\"marginzero   fontsize13px  fontfamilymon  textaligncenter\">There Is No Assessment</p>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n  </ion-list>\n  </div>\n</div>\n\n<div *ngIf=\"this.id == 'Eye'\">\n\n  <ion-segment [(ngModel)]=\"medicalrecord\">\n    <ion-segment-button value=\"Information\" class=\" fontsize12px colorblue texttransform\">\n      Information\n    </ion-segment-button>\n    <ion-segment-button value=\"Assessment\"   class=\" fontsize12px colorblue texttransform\">\n      Assessment\n    </ion-segment-button>\n  </ion-segment>\n  <div [ngSwitch]=\"medicalrecord\">\n    <ion-list *ngSwitchCase=\"'Information'\" >\n  <ion-card *ngFor=\"let r of recorddata;\">\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <p class=\"marginzero   fontsize11px  fontfamilymon floatright\">Date:{{r.infodata}}</p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Wear Glasses:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.Glasses}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Vision Right eye:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.righteyeno}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Vision Left Eye:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.lefteyeno}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Color Perception Right Eye:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.righteye}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Color Perception Left Eye:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.lefteye}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Event date:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.event_date}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Remark:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.remark}}</b></p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-card  *ngIf=\"!recorddata?.length > 0\">\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <p class=\"marginzero   fontsize13px  fontfamilymon  textaligncenter\">There Is No Information</p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  </ion-list>\n\n  <ion-list *ngSwitchCase=\"'Assessment'\" >\n    <ion-card *ngFor=\"let reord of recorddataqueans;\">\n      <ion-grid *ngFor=\"let r of reord.valuedate;\">\n        <ion-row>\n          <ion-col  size=\"12\">\n            <p class=\"marginzero   fontsize16px fontfamilymon\">Q:{{r.que_detail}}</p>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col  size=\"12\">\n            <p class=\"marginzero   fontsize16px fontfamilymon\">A:{{r.ans_code}}</p>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n    <ion-card  *ngIf=\"!recorddataqueans?.length > 0\">\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <p class=\"marginzero   fontsize13px  fontfamilymon  textaligncenter\">There Is No Assessment</p>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n  </ion-list>\n  </div>\n</div>\n\n\n<div *ngIf=\"this.id == 'diet'\">\n  <div >\n    \n  <ion-segment [(ngModel)]=\"medicalrecord\">\n    <ion-segment-button value=\"Information\" class=\" fontsize12px colorblue texttransform\">\n      Information\n    </ion-segment-button>\n    <ion-segment-button value=\"Assessment\"   class=\" fontsize12px colorblue texttransform\">\n      Assessment\n    </ion-segment-button>\n  </ion-segment>\n  <div [ngSwitch]=\"medicalrecord\">\n    <ion-list *ngSwitchCase=\"'Information'\" >\n  <ion-card *ngFor=\"let r of recorddata;\">\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <p class=\"marginzero   fontsize11px  fontfamilymon floatright\">Date:{{r.datec}}</p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Breakfast:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.Breakfast}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Lunch:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.lunch}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Evening:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.Evening}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Dinner:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.Dinner}}</b></p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-card  *ngIf=\"!recorddata?.length > 0\">\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <p class=\"marginzero   fontsize13px  fontfamilymon  textaligncenter\">There Is No Information</p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  </ion-list>\n\n  <ion-list *ngSwitchCase=\"'Assessment'\" >\n  <ion-card *ngFor=\"let reord of recorddataqueans;\">\n    <ion-grid *ngFor=\"let r of reord.valuedate;\">\n      <ion-row>\n        <ion-col  size=\"12\">\n          <p class=\"marginzero   fontsize16px fontfamilymon\">Q:{{r.que_detail}}</p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"12\">\n          <p class=\"marginzero   fontsize16px fontfamilymon\">A:{{r.ans_code}}</p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-card  *ngIf=\"!recorddataqueans?.length > 0\">\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <p class=\"marginzero   fontsize13px  fontfamilymon  textaligncenter\">There Is No Assessment</p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  </ion-list>\n  </div>\n</div>\n</div>\n<div *ngIf=\"this.id == 'mentalhealth'\">\n  <div >\n  <!-- <ion-segment [(ngModel)]=\"medicalrecord\">\n    <ion-segment-button value=\"Information\" class=\" fontsize12px colorblue texttransform\">\n      Information\n    </ion-segment-button>\n    <ion-segment-button value=\"Assessment\"   class=\" fontsize12px colorblue texttransform\">\n      Assessment\n    </ion-segment-button>\n  </ion-segment>\n  <div [ngSwitch]=\"medicalrecord\"> -->\n    <!-- <ion-list *ngSwitchCase=\"'Information'\" >\n  <ion-card *ngFor=\"let r of recorddata;\">\n    <ion-grid>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Wear Glasses:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.Glasses}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Vision Right eye:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.righteyeno}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Vision Left Eye:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.lefteyeno}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Color Perception Right Eye:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.righteye}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Color Perception Left Eye:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.lefteye}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Event date:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.event_date}}</b></p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero  floatright fontsize16px fontfamilymon\">Remark:</p>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <p class=\"marginzero   fontsize16px  fontfamilymon\"><b>{{r.remark}}</b></p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  </ion-list> -->\n\n  <!-- <ion-list *ngSwitchCase=\"'Assessment'\" > -->\n  <ion-card *ngFor=\"let reord of recorddataqueans;\">\n    <ion-grid *ngFor=\"let r of reord.valuedate;\">\n      <ion-row>\n        <ion-col  size=\"12\">\n          <p class=\"marginzero   fontsize16px fontfamilymon\">Q:{{r.que_detail}}</p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col  size=\"12\">\n          <p class=\"marginzero   fontsize16px fontfamilymon\">A:{{r.ans_code}}</p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-card  *ngIf=\"!recorddataqueans?.length > 0\">\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <p class=\"marginzero   fontsize13px  fontfamilymon  textaligncenter\">There Is No Assessment</p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <!-- </ion-list> -->\n  <!-- </div> -->\n</div>\n</div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/api/path/path.service.ts":
/*!******************************************!*\
  !*** ./src/app/api/path/path.service.ts ***!
  \******************************************/
/*! exports provided: PathService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PathService", function() { return PathService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PathService = /** @class */ (function () {
    function PathService() {
        this.url = 'https://cors-anywhere.herokuapp.com/http://www.planmyhealth.in/planhealthapi/api/';
    }
    Object.defineProperty(PathService.prototype, "serverurl", {
        get: function () {
            return this.url;
        },
        enumerable: true,
        configurable: true
    });
    PathService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PathService);
    return PathService;
}());



/***/ }),

/***/ "./src/app/medicalrecordsdetails/medicalrecordsdetails-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/medicalrecordsdetails/medicalrecordsdetails-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: MedicalrecordsdetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MedicalrecordsdetailsPageRoutingModule", function() { return MedicalrecordsdetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _medicalrecordsdetails_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./medicalrecordsdetails.page */ "./src/app/medicalrecordsdetails/medicalrecordsdetails.page.ts");




var routes = [
    {
        path: '',
        component: _medicalrecordsdetails_page__WEBPACK_IMPORTED_MODULE_3__["MedicalrecordsdetailsPage"]
    }
];
var MedicalrecordsdetailsPageRoutingModule = /** @class */ (function () {
    function MedicalrecordsdetailsPageRoutingModule() {
    }
    MedicalrecordsdetailsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], MedicalrecordsdetailsPageRoutingModule);
    return MedicalrecordsdetailsPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/medicalrecordsdetails/medicalrecordsdetails.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/medicalrecordsdetails/medicalrecordsdetails.module.ts ***!
  \***********************************************************************/
/*! exports provided: MedicalrecordsdetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MedicalrecordsdetailsPageModule", function() { return MedicalrecordsdetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _medicalrecordsdetails_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./medicalrecordsdetails-routing.module */ "./src/app/medicalrecordsdetails/medicalrecordsdetails-routing.module.ts");
/* harmony import */ var _medicalrecordsdetails_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./medicalrecordsdetails.page */ "./src/app/medicalrecordsdetails/medicalrecordsdetails.page.ts");







var MedicalrecordsdetailsPageModule = /** @class */ (function () {
    function MedicalrecordsdetailsPageModule() {
    }
    MedicalrecordsdetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _medicalrecordsdetails_routing_module__WEBPACK_IMPORTED_MODULE_5__["MedicalrecordsdetailsPageRoutingModule"]
            ],
            declarations: [_medicalrecordsdetails_page__WEBPACK_IMPORTED_MODULE_6__["MedicalrecordsdetailsPage"]]
        })
    ], MedicalrecordsdetailsPageModule);
    return MedicalrecordsdetailsPageModule;
}());



/***/ }),

/***/ "./src/app/medicalrecordsdetails/medicalrecordsdetails.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/medicalrecordsdetails/medicalrecordsdetails.page.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21lZGljYWxyZWNvcmRzZGV0YWlscy9tZWRpY2FscmVjb3Jkc2RldGFpbHMucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/medicalrecordsdetails/medicalrecordsdetails.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/medicalrecordsdetails/medicalrecordsdetails.page.ts ***!
  \*********************************************************************/
/*! exports provided: MedicalrecordsdetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MedicalrecordsdetailsPage", function() { return MedicalrecordsdetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _api_medicalcamp_medicalcamp_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../api/medicalcamp/medicalcamp.service */ "./src/app/api/medicalcamp/medicalcamp.service.ts");






var MedicalrecordsdetailsPage = /** @class */ (function () {
    function MedicalrecordsdetailsPage(toastCtrl, router, menuCtrl, _Activatedroute, loadingCtrl, storage, _medicalcamp) {
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.menuCtrl = menuCtrl;
        this._Activatedroute = _Activatedroute;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this._medicalcamp = _medicalcamp;
        this.medicalrecord = "Information";
    }
    MedicalrecordsdetailsPage.prototype.ngOnInit = function () {
        var _this = this;
        this.id = this._Activatedroute.snapshot.paramMap.get('id');
        this.storage.get('membercode').then(function (val) {
            _this.membercode = val;
        });
        if (this.id == 'Physician') {
            this.Physician();
            this.questionanswer('1');
        }
        else if (this.id == 'Eye') {
            this.eye();
            this.questionanswer('3');
        }
        else if (this.id == 'dental') {
            this.dental();
            this.questionanswer('2');
        }
        else if (this.id == 'diet') {
            this.diet();
            this.questionanswer('5');
        }
        else if (this.id == 'mentalhealth') {
            this.mentalhealth();
            this.questionanswer('4');
        }
    };
    MedicalrecordsdetailsPage.prototype.Physician = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.storage.get('membercode').then(function (val) {
                            _this._medicalcamp.vitalmember(val).subscribe(function (data) {
                                loading.dismiss();
                                if (data['success'] == '1') {
                                    _this.recorddata = data['data'];
                                    _this.recordstatus = true;
                                }
                                else {
                                    _this.recordstatus = false;
                                }
                            }, function (error) { _this.errorMsg = error.success; });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    MedicalrecordsdetailsPage.prototype.eye = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.storage.get('membercode').then(function (val) {
                            _this._medicalcamp.eyemember(val).subscribe(function (data) {
                                loading.dismiss();
                                if (data['success'] == '1') {
                                    _this.recorddata = data['data'];
                                    _this.recordstatus = true;
                                }
                                else {
                                    _this.recordstatus = false;
                                }
                            }, function (error) { _this.errorMsg = error.success; });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    MedicalrecordsdetailsPage.prototype.dental = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.storage.get('membercode').then(function (val) {
                            _this._medicalcamp.dentalmember(val).subscribe(function (data) {
                                loading.dismiss();
                                if (data['success'] == '1') {
                                    _this.recorddata = data['data'];
                                    _this.recordstatus = true;
                                }
                                else {
                                    _this.recordstatus = false;
                                }
                            }, function (error) { _this.errorMsg = error.success; });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    MedicalrecordsdetailsPage.prototype.diet = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.storage.get('membercode').then(function (val) {
                            _this._medicalcamp.diet(val).subscribe(function (data) {
                                loading.dismiss();
                                if (data['success'] == '1') {
                                    _this.recorddata = data['data'];
                                    _this.recordstatus = true;
                                }
                                else {
                                    _this.recordstatus = false;
                                }
                            }, function (error) { _this.errorMsg = error.success; });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    MedicalrecordsdetailsPage.prototype.mentalhealth = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.storage.get('membercode').then(function (val) {
                            _this._medicalcamp.mentalhealth(val).subscribe(function (data) {
                                loading.dismiss();
                                if (data['success'] == '1') {
                                    _this.recorddata = data['data'];
                                    _this.recordstatus = true;
                                }
                                else {
                                    _this.recordstatus = false;
                                }
                            }, function (error) { _this.errorMsg = error.success; });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    MedicalrecordsdetailsPage.prototype.questionanswer = function (id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.storage.get('membercode').then(function (val) {
                            _this._medicalcamp.questionanswermember(val, id).subscribe(function (data) {
                                loading.dismiss();
                                if (data['success'] == '1') {
                                    _this.recorddatafetch = data['data'];
                                    var groups_1 = _this.recorddatafetch.reduce(function (groups, game) {
                                        var date = game.entry_date.split(' ')[0];
                                        if (!groups[date]) {
                                            groups[date] = [];
                                        }
                                        groups[date].push(game);
                                        return groups;
                                    }, {});
                                    // Edit: to add it in the array format instead
                                    var groupArrays = Object.keys(groups_1).map(function (date) {
                                        return {
                                            valuedate: groups_1[date]
                                        };
                                    });
                                    _this.recorddataqueans = groupArrays;
                                    console.log(groupArrays);
                                    _this.recordstatus = false;
                                }
                                else {
                                    _this.recordstatus = false;
                                }
                            }, function (error) { _this.errorMsg = error.success; });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    MedicalrecordsdetailsPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
        { type: _api_medicalcamp_medicalcamp_service__WEBPACK_IMPORTED_MODULE_5__["MedicalcampService"] }
    ]; };
    MedicalrecordsdetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-medicalrecordsdetails',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./medicalrecordsdetails.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/medicalrecordsdetails/medicalrecordsdetails.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./medicalrecordsdetails.page.scss */ "./src/app/medicalrecordsdetails/medicalrecordsdetails.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"], _api_medicalcamp_medicalcamp_service__WEBPACK_IMPORTED_MODULE_5__["MedicalcampService"]])
    ], MedicalrecordsdetailsPage);
    return MedicalrecordsdetailsPage;
}());



/***/ })

}]);
//# sourceMappingURL=medicalrecordsdetails-medicalrecordsdetails-module.js.map