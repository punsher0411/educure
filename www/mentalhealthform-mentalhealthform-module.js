(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["mentalhealthform-mentalhealthform-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/mentalhealthform/mentalhealthform.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/mentalhealthform/mentalhealthform.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/medicalcamp\"></ion-back-button>\n      <p><b>Mental Health </b></p>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div>\n    <div class=\"borderstudentlist\">\n      <h3 class=\"textaligncenter fontsize15px\"><u>Student Information</u></h3>\n      <ion-row>\n        <ion-col col-6> <p class=\"studentlist\">Name/Id:</p></ion-col>\n        <ion-col col-6><p class=\"studentlist\">{{firstname}} {{lastname}}/{{id}}</p></ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6><p class=\"studentlist\">Class/Roll No/ Div:</p></ion-col>\n        <ion-col col-6><p class=\"studentlist\">{{studentclass}}/{{studentrollno}}/{{studentdiv}}</p></ion-col>\n      </ion-row>\n    </div>\n    <ion-list padding>\n      <form [formGroup]=\"MentalForm\" >\n         <p class=\"textaligncenter\">Question And Answer</p>\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.1) General Intelligence:-</ion-label>\n          <ion-radio-group value=\"Average\" formControlName=\"ansdetailQ135\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Average\"></ion-radio><span class=\"fontsize12px\">Average</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Belowavg \"></ion-radio><span class=\"fontsize12px\">Below Avg</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Aboveavg\"></ion-radio><span class=\"fontsize12px\">Above Avg</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.2) Non Verbal Intelligence:-</ion-label>\n          <ion-radio-group value=\"Average\" formControlName=\"ansdetailQ136\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Average\"></ion-radio><span class=\"fontsize12px\">Average</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Belowavg \"></ion-radio><span class=\"fontsize12px\">Below Avg</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Aboveavg\"></ion-radio><span class=\"fontsize12px\">Above Avg</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.3) Attention:-</ion-label>\n          <ion-radio-group value=\"Average\" formControlName=\"ansdetailQ137\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Average\"></ion-radio><span class=\"fontsize12px\">Average</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Belowavg \"></ion-radio><span class=\"fontsize12px\">Below Avg</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Aboveavg\"></ion-radio><span class=\"fontsize12px\">Above Avg</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.4) Memory & Learning:-</ion-label>\n          <ion-radio-group value=\"Average\" formControlName=\"ansdetailQ138\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Average\"></ion-radio><span class=\"fontsize12px\">Average</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Belowavg \"></ion-radio><span class=\"fontsize12px\">Below Avg</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Aboveavg\"></ion-radio><span class=\"fontsize12px\">Above Avg</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.5) Motor Functioning:-</ion-label>\n          <ion-radio-group value=\"Average\" formControlName=\"ansdetailQ139\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Average\"></ion-radio><span class=\"fontsize12px\">Average</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Belowavg \"></ion-radio><span class=\"fontsize12px\">Below Avg</span></ion-col>\n              <ion-col size=\"4\"><ion-radio  color=\"tertiary\" value=\"Aboveavg\"></ion-radio><span class=\"fontsize12px\">Above Avg</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.6) Eye Contact:-</ion-label>\n          <ion-radio-group value=\"Maintains\" formControlName=\"ansdetailQ140\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Maintains\"></ion-radio><span class=\"fontsize12px\">Maintains</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"NotMaintains\"></ion-radio><span class=\"fontsize12px\">Not Maintains</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.7) Anxiety Levels:-</ion-label>\n          <ion-radio-group value=\"High\" formControlName=\"ansdetailQ141\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"High\"></ion-radio><span class=\"fontsize12px\">High</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Low\"></ion-radio><span class=\"fontsize12px\">Low</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.8) Get Frequently Angry:-</ion-label>\n          <ion-radio-group value=\"Yes\" formControlName=\"ansdetailQ142\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Yes\"></ion-radio><span class=\"fontsize12px\">Yes</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"No \"></ion-radio><span class=\"fontsize12px\">No</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.9) Hyper-Active:-</ion-label>\n          <ion-radio-group value=\"Yes\" formControlName=\"ansdetailQ143\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Yes\"></ion-radio><span class=\"fontsize12px\">Yes</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"No \"></ion-radio><span class=\"fontsize12px\">No</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n        <ion-item >\n          <ion-label  position=\"stacked\">Q.10) Communication Type:-</ion-label>\n          <ion-radio-group value=\"Extrovert\" formControlName=\"ansdetailQ144\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Extrovert\"></ion-radio><span class=\"fontsize12px\">Extrovert</span></ion-col>\n              <ion-col size=\"6\"><ion-radio  color=\"tertiary\" value=\"Introvert\"></ion-radio><span class=\"fontsize12px\">Introvert</span></ion-col>\n            </ion-row>\n          </ion-grid>\n          </ion-radio-group>\n          </ion-item>\n       \n\n                  <ion-item lines=\"none\">\n                    <ion-button class=\"centerblock\" type=\"submit\" color=\"tertiary\" Expand=\"block\"\n                      size=\"default\" (click)=\"submit()\">Submit</ion-button>\n                  </ion-item>\n                </form>\n        </ion-list>\n        </div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/mentalhealthform/mentalhealthform-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/mentalhealthform/mentalhealthform-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: MentalhealthformPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MentalhealthformPageRoutingModule", function() { return MentalhealthformPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _mentalhealthform_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./mentalhealthform.page */ "./src/app/mentalhealthform/mentalhealthform.page.ts");




var routes = [
    {
        path: '',
        component: _mentalhealthform_page__WEBPACK_IMPORTED_MODULE_3__["MentalhealthformPage"]
    }
];
var MentalhealthformPageRoutingModule = /** @class */ (function () {
    function MentalhealthformPageRoutingModule() {
    }
    MentalhealthformPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], MentalhealthformPageRoutingModule);
    return MentalhealthformPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/mentalhealthform/mentalhealthform.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/mentalhealthform/mentalhealthform.module.ts ***!
  \*************************************************************/
/*! exports provided: MentalhealthformPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MentalhealthformPageModule", function() { return MentalhealthformPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _mentalhealthform_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./mentalhealthform-routing.module */ "./src/app/mentalhealthform/mentalhealthform-routing.module.ts");
/* harmony import */ var _mentalhealthform_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./mentalhealthform.page */ "./src/app/mentalhealthform/mentalhealthform.page.ts");







var MentalhealthformPageModule = /** @class */ (function () {
    function MentalhealthformPageModule() {
    }
    MentalhealthformPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _mentalhealthform_routing_module__WEBPACK_IMPORTED_MODULE_5__["MentalhealthformPageRoutingModule"]
            ],
            declarations: [_mentalhealthform_page__WEBPACK_IMPORTED_MODULE_6__["MentalhealthformPage"]]
        })
    ], MentalhealthformPageModule);
    return MentalhealthformPageModule;
}());



/***/ }),

/***/ "./src/app/mentalhealthform/mentalhealthform.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/mentalhealthform/mentalhealthform.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21lbnRhbGhlYWx0aGZvcm0vbWVudGFsaGVhbHRoZm9ybS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/mentalhealthform/mentalhealthform.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/mentalhealthform/mentalhealthform.page.ts ***!
  \***********************************************************/
/*! exports provided: MentalhealthformPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MentalhealthformPage", function() { return MentalhealthformPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../api/form/form.service */ "./src/app/api/form/form.service.ts");







var MentalhealthformPage = /** @class */ (function () {
    function MentalhealthformPage(formBuilder, toastCtrl, router, storage, _form, menuCtrl, _Activatedroute, loadingCtrl) {
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.storage = storage;
        this._form = _form;
        this.menuCtrl = menuCtrl;
        this._Activatedroute = _Activatedroute;
        this.loadingCtrl = loadingCtrl;
        this.MentalForm = this.formBuilder.group({
            ansdetailQ135: ['Average'],
            ansdetailQ136: ['Average'],
            ansdetailQ137: ['Average'],
            ansdetailQ138: ['Average'],
            ansdetailQ139: ['Average'],
            ansdetailQ140: ['Maintains'],
            ansdetailQ141: ['High'],
            ansdetailQ142: ['Yes'],
            ansdetailQ143: ['Yes'],
            ansdetailQ144: ['Extrovert']
        });
    }
    MentalhealthformPage.prototype.ngOnInit = function () {
        this.id = this._Activatedroute.snapshot.paramMap.get('id');
        this.firstname = this._Activatedroute.snapshot.paramMap.get('firstname');
        this.lastname = this._Activatedroute.snapshot.paramMap.get('lastname');
        this.consoluentid = this._Activatedroute.snapshot.paramMap.get('consulentid');
        this.event_id = this._Activatedroute.snapshot.paramMap.get('event_id');
        this.studentclass = this._Activatedroute.snapshot.paramMap.get('studentclass');
        this.studentrollno = this._Activatedroute.snapshot.paramMap.get('studentrollno');
        this.studentdiv = this._Activatedroute.snapshot.paramMap.get('studentdiv');
    };
    Object.defineProperty(MentalhealthformPage.prototype, "ansdetailQ135", {
        get: function () {
            return this.MentalForm.get("ansdetailQ135");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MentalhealthformPage.prototype, "ansdetailQ136", {
        get: function () {
            return this.MentalForm.get("ansdetailQ136");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MentalhealthformPage.prototype, "ansdetailQ137", {
        get: function () {
            return this.MentalForm.get("ansdetailQ137");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MentalhealthformPage.prototype, "ansdetailQ138", {
        get: function () {
            return this.MentalForm.get("ansdetailQ138");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MentalhealthformPage.prototype, "ansdetailQ139", {
        get: function () {
            return this.MentalForm.get("ansdetailQ139");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MentalhealthformPage.prototype, "ansdetailQ140", {
        get: function () {
            return this.MentalForm.get("ansdetailQ140");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MentalhealthformPage.prototype, "ansdetailQ141", {
        get: function () {
            return this.MentalForm.get("ansdetailQ141");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MentalhealthformPage.prototype, "ansdetailQ142", {
        get: function () {
            return this.MentalForm.get("ansdetailQ142");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MentalhealthformPage.prototype, "ansdetailQ143", {
        get: function () {
            return this.MentalForm.get("ansdetailQ143");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MentalhealthformPage.prototype, "ansdetailQ129", {
        get: function () {
            return this.MentalForm.get("ansdetailQ129");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MentalhealthformPage.prototype, "ansdetailQ144", {
        get: function () {
            return this.MentalForm.get("ansdetailQ144");
        },
        enumerable: true,
        configurable: true
    });
    MentalhealthformPage.prototype.submit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.messages = ['Q135', 'Q136', 'Q137', 'Q138', 'Q139', 'Q140', 'Q141', 'Q142', 'Q143', 'Q144'];
                        this._form.mentalhealth(this.MentalForm.value, this.id, this.messages, this.consoluentid, this.event_id)
                            .subscribe(function (data) {
                            loading.dismiss();
                            if (data['success'] == '1') {
                                alert("Data Enter Successfully");
                                _this.router.navigate(['/medicalcamp']);
                            }
                            else {
                                alert(data['msg']);
                                _this.MentalForm.reset();
                            }
                        }, function (error) { _this.errorMsg = error.success; });
                        return [2 /*return*/];
                }
            });
        });
    };
    MentalhealthformPage.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
        { type: _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__["FormService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] }
    ]; };
    MentalhealthformPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-mentalhealthform',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./mentalhealthform.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/mentalhealthform/mentalhealthform.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./mentalhealthform.page.scss */ "./src/app/mentalhealthform/mentalhealthform.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
            _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__["FormService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]])
    ], MentalhealthformPage);
    return MentalhealthformPage;
}());



/***/ })

}]);
//# sourceMappingURL=mentalhealthform-mentalhealthform-module.js.map