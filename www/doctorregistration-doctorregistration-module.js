(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["doctorregistration-doctorregistration-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/doctorregistration/doctorregistration.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/doctorregistration/doctorregistration.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/login\"></ion-back-button>\n      <p><b>Doctor Registration</b></p>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card>\n  <div>\n    \n    <ion-list padding>\n      <form [formGroup]=\"DoctorregisterForm\" >\n      <ion-item>\n        <ion-label  position=\"stacked\">Name:-</ion-label>\n        <ion-input autocapitalize inputmode=\"text\" formControlName=\"name\"></ion-input>\n        </ion-item>\n        <div *ngFor=\"let error of errorMessages.name\">\n          <ng-container *ngIf=\"name.hasError(error.type) && (name.dirty || name.touched)\">\n            <small class=\"error-message\">{{error.message}}</small>\n          </ng-container>\n        </div>\n      <ion-item >\n        <ion-label  position=\"stacked\">Age<ion-text color=\"danger\">*</ion-text></ion-label>\n        <ion-input autocapitalize inputmode=\"text\" formControlName=\"age\"></ion-input>\n        </ion-item>\n        <div *ngFor=\"let error of errorMessages.age\">\n          <ng-container *ngIf=\"age.hasError(error.type) && (age.dirty || age.touched)\">\n            <small class=\"error-message\">{{error.message}}</small>\n          </ng-container>\n        </div>\n      <ion-list >\n        <ion-label  position=\"stacked\">Gender<ion-text color=\"danger\">*</ion-text></ion-label>\n        <ion-item  class=\"backgroundcolortransparent\">  \n        <ion-radio-group formControlName=\"gender\">\n          <ion-row >\n            <ion-item lines=\"none\">\n              <ion-label>Male</ion-label>\n              <ion-radio value=\"male\"></ion-radio>\n            </ion-item>\n            <ion-item lines=\"none\">\n              <ion-label>Female</ion-label>\n              <ion-radio value=\"female\"></ion-radio>\n            </ion-item>\n          </ion-row>\n        </ion-radio-group>\n        </ion-item>\n        </ion-list>\n        <div *ngFor=\"let error of errorMessages.gender\">\n          <ng-container *ngIf=\"gender.hasError(error.type) && (gender.dirty || gender.touched)\">\n            <small class=\"error-message\">{{error.message}}</small>\n          </ng-container>\n        </div>\n      \n        <ion-item >\n          <ion-label  position=\"stacked\">City</ion-label>\n          <ion-input autocapitalize inputmode=\"text\" formControlName=\"city\" (ionInput)=\"FilterJsonData($event)\" value=\"{{getcityname}}\"></ion-input>\n          </ion-item>\n          <div *ngIf=\"cityliststatus\">\n          <ion-item (click)=\"getpincode(city.city_name)\" *ngFor=\"let city of citylistdata | slice:0:2;\" >{{city.city_name}}</ion-item>\n          </div>\n\n          <div *ngFor=\"let error of errorMessages.city\">\n            <ng-container *ngIf=\"city.hasError(error.type) && (city.dirty || city.touched)\">\n              <small class=\"error-message\">{{error.message}}</small>\n            </ng-container>\n          </div>\n          \n          <ion-item >\n            <ion-label  position=\"stacked\">Pincode:-</ion-label>\n            <div *ngIf=\"pincodeliststatus\">\n            <ion-select  formControlName=\"pincode\" class=\"maxwidth\">\n              <ion-select-option *ngFor=\"let pincode of pincodelistdata\" value=\"{{pincode.pincode}}\">{{pincode.pincode}}\n              </ion-select-option>\n            </ion-select>\n          </div>\n          <div *ngIf=\"!pincodeliststatus\">\n              <p>Please Select City First</p> \n          </div>\n            </ion-item>\n            <div *ngFor=\"let error of errorMessages.pincode\">\n              <ng-container *ngIf=\"pincode.hasError(error.type) && (pincode.dirty || pincode.touched)\">\n                <small class=\"error-message\">{{error.message}}</small>\n              </ng-container>\n            </div>\n            \n\n            <ion-item >\n              <ion-label  position=\"stacked\">Mobile 1</ion-label>\n              <ion-input autocapitalize type=\"number\" formControlName=\"mobile1\"></ion-input>\n              </ion-item>\n              <div *ngFor=\"let error of errorMessages.mobile1\">\n                <ng-container *ngIf=\"mobile1.hasError(error.type) && (mobile1.dirty || mobile1.touched)\">\n                  <small class=\"error-message\">{{error.message}}</small>\n                </ng-container>\n              </div>\n            <ion-item >\n              <ion-label  position=\"stacked\">Mobile 2</ion-label>\n              <ion-input autocapitalize type=\"number\" formControlName=\"mobile2\"></ion-input>\n              </ion-item>\n              <div *ngFor=\"let error of errorMessages.mobile2\">\n                <ng-container *ngIf=\"mobile2.hasError(error.type) && (mobile2.dirty || mobile2.touched)\">\n                  <small class=\"error-message\">{{error.message}}</small>\n                </ng-container>\n              </div>\n\n              <ion-item >\n                <ion-label  position=\"stacked\">Email</ion-label>\n                <ion-input autocapitalize inputmode=\"email\" formControlName=\"Email\"></ion-input>\n                </ion-item>\n                <div *ngFor=\"let error of errorMessages.Email\">\n                  <ng-container *ngIf=\"Email.hasError(error.type) && (Email.dirty || Email.touched)\">\n                    <small class=\"error-message\">{{error.message}}</small>\n                  </ng-container>\n                </div>\n\n                <ion-item >\n                  <ion-label  position=\"stacked\">Experience</ion-label>\n                  <ion-select formControlName=\"experience\">\n                    <ion-select-option value=\"0\" selected>0</ion-select-option>\n                    <ion-select-option value=\"6\" selected>06</ion-select-option>\n                    <ion-select-option value=\"1\" selected>1</ion-select-option>\n                    <ion-select-option value=\"2\" >2</ion-select-option>\n                    <ion-select-option value=\"3\" >3</ion-select-option>\n                    <ion-select-option value=\"4\" >4</ion-select-option>\n                    <ion-select-option value=\"5\" >5</ion-select-option>\n                    <ion-select-option value=\"6\" >6</ion-select-option>\n                    <ion-select-option value=\"7\" >7</ion-select-option>\n                    <ion-select-option value=\"8\" >8</ion-select-option>\n                    <ion-select-option value=\"9\" >9</ion-select-option>\n                    <ion-select-option value=\"10\" >10</ion-select-option>\n                    <ion-select-option value=\"11\" >11</ion-select-option>\n                    <ion-select-option value=\"12\" >12</ion-select-option>\n                    <ion-select-option value=\"13\" >13</ion-select-option>\n                    <ion-select-option value=\"14\" >14</ion-select-option>\n                    <ion-select-option value=\"15\" >15</ion-select-option>\n                    <ion-select-option value=\"16\" >16</ion-select-option>\n                    <ion-select-option value=\"17\" >17</ion-select-option>\n                    <ion-select-option value=\"18\" >18</ion-select-option>\n                    <ion-select-option value=\"19\" >19</ion-select-option>\n                    <ion-select-option value=\"20\" >20</ion-select-option>\n                    <ion-select-option value=\"21\" >21</ion-select-option>\n                    <ion-select-option value=\"22\" >22</ion-select-option>\n                    <ion-select-option value=\"23\" >23</ion-select-option>\n                    <ion-select-option value=\"24\" >24</ion-select-option>\n                    <ion-select-option value=\"25\" >25</ion-select-option>\n                    <ion-select-option value=\"26\" >26</ion-select-option>\n                    <ion-select-option value=\"27\" >27</ion-select-option>\n                    <ion-select-option value=\"28\" >28</ion-select-option>\n                    <ion-select-option value=\"29\" >29</ion-select-option>\n                    <ion-select-option value=\"30\" >30</ion-select-option>\n                    \n                  </ion-select>\n                  </ion-item>\n                  <div *ngFor=\"let error of errorMessages.experience\">\n                    <ng-container *ngIf=\"experience.hasError(error.type) && (experience.dirty || experience.touched)\">\n                      <small class=\"error-message\">{{error.message}}</small>\n                    </ng-container>\n                  </div>\n                  <ion-item >\n                    <ion-label  position=\"stacked\">Speciality </ion-label>\n                    <ion-select  formControlName=\"speciality\" (ionChange)=\"getqualification($event)\">\n                      <ion-select-option *ngFor=\"let specility of specilitylistdata\" value=\"{{specility.prov_type_code}}\">{{specility.prov_type_name}}\n                      </ion-select-option>\n                    </ion-select>\n                    </ion-item>\n                    <div *ngFor=\"let error of errorMessages.speciality\">\n                      <ng-container *ngIf=\"speciality.hasError(error.type) && (speciality.dirty || speciality.touched)\">\n                        <small class=\"error-message\">{{error.message}}</small>\n                      </ng-container>\n                    </div>\n                <ion-item >\n                  <ion-label  position=\"stacked\">Qualification</ion-label>\n                  <div *ngIf=\"qualificationliststatus\">\n                  <ion-select  class=\"maxwidth\" formControlName=\"qualification\" placeholder=\"Please select Qualification\" >\n                    <ion-select-option *ngFor=\"let qualification of qualificationlistdata\" value=\"{{qualification.qualific_code}}\">{{qualification.qualific_name}} -{{qualification.qualific_desc}}\n                    </ion-select-option>\n                  </ion-select>\n                </div>\n                <div *ngIf=\"!qualificationliststatus\">\n                  <p>Please select Specility First</p>\n                </div>\n                  </ion-item>\n                  <div *ngFor=\"let error of errorMessages.qualification\">\n                    <ng-container *ngIf=\"qualification.hasError(error.type) && (qualification.dirty || qualification.touched)\">\n                      <small class=\"error-message\">{{error.message}}</small>\n                    </ng-container>\n                  </div>\n                \n              \n                  <ion-item >\n                    <ion-label  position=\"stacked\">Clinic Name</ion-label>\n                    <ion-input autocapitalize inputmode=\"text\" formControlName=\"clinicname\"></ion-input>\n                    </ion-item>\n                    <div *ngFor=\"let error of errorMessages.clinicname\">\n                      <ng-container *ngIf=\"clinicname.hasError(error.type) && (clinicname.dirty || clinicname.touched)\">\n                        <small class=\"error-message\">{{error.message}}</small>\n                      </ng-container>\n                    </div>\n\n                    <ion-item >\n                      <ion-label  position=\"stacked\">Available for multi:-<ion-text color=\"danger\">*</ion-text></ion-label>\n                      <ion-select  multiple=\"true\" formControlName=\"availablemulti\" >\n                        <ion-select-option *ngFor=\"let service of servicelistdata\" value=\"{{service.id}}\">{{service.mode}}\n                        </ion-select-option>\n                      </ion-select>\n                      </ion-item>\n                      <div *ngFor=\"let error of errorMessages.availablemulti\">\n                        <ng-container *ngIf=\"availablemulti.hasError(error.type) && (availablemulti.dirty || availablemulti.touched)\">\n                          <small class=\"error-message\">{{error.message}}</small>\n                        </ng-container>\n                      </div>\n  \n                      <ion-item >\n                        <ion-label  position=\"stacked\">Bank Account Holder Name</ion-label>\n                        <ion-input autocapitalize inputmode=\"text\" formControlName=\"bankholdername\"></ion-input>\n                        </ion-item>\n                        <div *ngFor=\"let error of errorMessages.bankholdername\">\n                          <ng-container *ngIf=\"bankholdername.hasError(error.type) && (bankholdername.dirty || bankholdername.touched)\">\n                            <small class=\"error-message\">{{error.message}}</small>\n                          </ng-container>\n                        </div>\n\n                      <ion-item >\n                        <ion-label  position=\"stacked\">Bank Account Number</ion-label>\n                        <ion-input autocapitalize inputmode=\"text\" formControlName=\"accountnumber\"></ion-input>\n                        </ion-item>\n                        <div *ngFor=\"let error of errorMessages.accountnumber\">\n                          <ng-container *ngIf=\"accountnumber.hasError(error.type) && (accountnumber.dirty || accountnumber.touched)\">\n                            <small class=\"error-message\">{{error.message}}</small>\n                          </ng-container>\n                        </div>\n\n                        <ion-item >\n                          <ion-label  position=\"stacked\">Bank IFSC Code</ion-label>\n                          <ion-input autocapitalize inputmode=\"text\" formControlName=\"ifsccode\"></ion-input>\n                          </ion-item>\n                          <div *ngFor=\"let error of errorMessages.ifsccode\">\n                            <ng-container *ngIf=\"ifsccode.hasError(error.type) && (ifsccode.dirty || ifsccode.touched)\">\n                              <small class=\"error-message\">{{error.message}}</small>\n                            </ng-container>\n                          </div>\n\n                        <ion-item >\n                          <ion-label  position=\"stacked\">Upi Pin</ion-label>\n                          <ion-input autocapitalize inputmode=\"text\" formControlName=\"upipin\"></ion-input>\n                          </ion-item>\n                          <div *ngFor=\"let error of errorMessages.upipin\">\n                            <ng-container *ngIf=\"upipin.hasError(error.type) && (upipin.dirty || upipin.touched)\">\n                              <small class=\"error-message\">{{error.message}}</small>\n                            </ng-container>\n                          </div>\n\n                        <ion-item >\n                          <ion-label  position=\"stacked\">Remark</ion-label>\n                          <ion-textarea formControlName=\"remark\" ></ion-textarea>\n                          </ion-item>\n                         \n                          <ion-item >\n                            <p class=\"fontsize11px colordefault textaligncenter\">Upload Photo</p>\n                            <ion-button class=\"centerblock\" (click)=\"selectImage()\" color=\"tertiary\">Upload</ion-button>\n                          </ion-item>\n                          <img src=\"{{imagepathurl}}\" style=\"width: 102px;display: block;margin: auto;\"/>\n                  <ion-item lines=\"none\">\n                    <ion-button class=\"centerblock\" type=\"submit\" color=\"tertiary\" Expand=\"block\"\n                      size=\"default\" (click)=\"submit()\">Submit</ion-button>\n                  </ion-item>\n                  </form>\n        </ion-list>\n        </div>\n      </ion-card>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/api/doctorregister/doctorregister.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/api/doctorregister/doctorregister.service.ts ***!
  \**************************************************************/
/*! exports provided: DoctorregisterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoctorregisterService", function() { return DoctorregisterService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _path_path_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../path/path.service */ "./src/app/api/path/path.service.ts");






var DoctorregisterService = /** @class */ (function () {
    function DoctorregisterService(_http, _url) {
        this._http = _http;
        this._url = _url;
        this._specilitylist = this._url.serverurl + 'specility';
        this._qualificationlist = this._url.serverurl + 'qualificationlist';
        this._citylist = this._url.serverurl + 'city';
        this._servicelist = this._url.serverurl + 'service';
        this._pincode = this._url.serverurl + 'pincode';
        this._doctorform = this._url.serverurl + 'doctorform';
    }
    DoctorregisterService.prototype.getHearder = function () {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json'
        });
        this.httpOptions = { headers: headers };
    };
    DoctorregisterService.prototype.specilitylist = function () {
        var formData = new FormData();
        return this._http.post(this._specilitylist, formData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
    };
    DoctorregisterService.prototype.qualificationlist = function (treatment_mode) {
        var formData = new FormData();
        formData.append('treatment_mode', treatment_mode);
        return this._http.post(this._qualificationlist, formData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
    };
    DoctorregisterService.prototype.citylist = function () {
        var formData = new FormData();
        return this._http.post(this._citylist, formData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
    };
    DoctorregisterService.prototype.servicelist = function () {
        var formData = new FormData();
        return this._http.post(this._servicelist, formData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
    };
    DoctorregisterService.prototype.pincodelist = function (cityname) {
        var formData = new FormData();
        formData.append('city_name', cityname);
        return this._http.post(this._pincode, formData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
    };
    DoctorregisterService.prototype.doctorregister = function (doctorData, DPF, DPFblob, DPFname, latt, longg) {
        var formData = new FormData();
        formData.append('name', doctorData.name);
        formData.append('age', doctorData.age);
        formData.append('gender', doctorData.gender);
        formData.append('speciality', doctorData.speciality);
        formData.append('city', doctorData.city);
        formData.append('pincode', doctorData.pincode);
        formData.append('mobile1', doctorData.mobile1);
        formData.append('mobile2', doctorData.mobile2);
        formData.append('Email', doctorData.Email);
        formData.append('experience', doctorData.experience);
        formData.append('qualification', doctorData.qualification);
        formData.append('clinicname', doctorData.clinicname);
        formData.append('availablemulti', doctorData.availablemulti);
        formData.append('bankholdername', doctorData.bankholdername);
        formData.append('accountnumber', doctorData.accountnumber);
        formData.append('ifsccode', doctorData.ifsccode);
        formData.append('upipin', doctorData.upipin);
        formData.append('remark', doctorData.remark);
        formData.append('image', DPFblob, DPFname);
        formData.append('image', DPFblob, DPFname);
        formData.append('image', DPFblob, DPFname);
        formData.append('latt', latt);
        formData.append('longg', longg);
        return this._http.post(this._doctorform, formData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
    };
    DoctorregisterService.prototype.doctorregisterwithoutimage = function (doctorData, latt, longg) {
        var formData = new FormData();
        formData.append('name', doctorData.name);
        formData.append('age', doctorData.age);
        formData.append('gender', doctorData.gender);
        formData.append('speciality', doctorData.speciality);
        formData.append('city', doctorData.city);
        formData.append('pincode', doctorData.pincode);
        formData.append('mobile1', doctorData.mobile1);
        formData.append('mobile2', doctorData.mobile2);
        formData.append('Email', doctorData.Email);
        formData.append('experience', doctorData.experience);
        formData.append('qualification', doctorData.qualification);
        formData.append('clinicname', doctorData.clinicname);
        formData.append('availablemulti', doctorData.availablemulti);
        formData.append('bankholdername', doctorData.bankholdername);
        formData.append('accountnumber', doctorData.accountnumber);
        formData.append('ifsccode', doctorData.ifsccode);
        formData.append('upipin', doctorData.upipin);
        formData.append('remark', doctorData.remark);
        formData.append('latt', latt);
        formData.append('longg', longg);
        return this._http.post(this._doctorform, formData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
    };
    DoctorregisterService.prototype.errorHandler = function (error) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(error);
    };
    DoctorregisterService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _path_path_service__WEBPACK_IMPORTED_MODULE_5__["PathService"] }
    ]; };
    DoctorregisterService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _path_path_service__WEBPACK_IMPORTED_MODULE_5__["PathService"]])
    ], DoctorregisterService);
    return DoctorregisterService;
}());



/***/ }),

/***/ "./src/app/doctorregistration/doctorregistration-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/doctorregistration/doctorregistration-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: DoctorregistrationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoctorregistrationPageRoutingModule", function() { return DoctorregistrationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _doctorregistration_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./doctorregistration.page */ "./src/app/doctorregistration/doctorregistration.page.ts");




var routes = [
    {
        path: '',
        component: _doctorregistration_page__WEBPACK_IMPORTED_MODULE_3__["DoctorregistrationPage"]
    }
];
var DoctorregistrationPageRoutingModule = /** @class */ (function () {
    function DoctorregistrationPageRoutingModule() {
    }
    DoctorregistrationPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], DoctorregistrationPageRoutingModule);
    return DoctorregistrationPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/doctorregistration/doctorregistration.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/doctorregistration/doctorregistration.module.ts ***!
  \*****************************************************************/
/*! exports provided: DoctorregistrationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoctorregistrationPageModule", function() { return DoctorregistrationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _doctorregistration_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./doctorregistration-routing.module */ "./src/app/doctorregistration/doctorregistration-routing.module.ts");
/* harmony import */ var _doctorregistration_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./doctorregistration.page */ "./src/app/doctorregistration/doctorregistration.page.ts");







var DoctorregistrationPageModule = /** @class */ (function () {
    function DoctorregistrationPageModule() {
    }
    DoctorregistrationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _doctorregistration_routing_module__WEBPACK_IMPORTED_MODULE_5__["DoctorregistrationPageRoutingModule"]
            ],
            declarations: [_doctorregistration_page__WEBPACK_IMPORTED_MODULE_6__["DoctorregistrationPage"]]
        })
    ], DoctorregistrationPageModule);
    return DoctorregistrationPageModule;
}());



/***/ }),

/***/ "./src/app/doctorregistration/doctorregistration.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/doctorregistration/doctorregistration.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".maxwidth {\n  max-width: 100% !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZG9jdG9ycmVnaXN0cmF0aW9uL0M6XFxpb25pY1xca3J1c2huYW1haVxcTmV3IGZvbGRlci9zcmNcXGFwcFxcZG9jdG9ycmVnaXN0cmF0aW9uXFxkb2N0b3JyZWdpc3RyYXRpb24ucGFnZS5zY3NzIiwic3JjL2FwcC9kb2N0b3JyZWdpc3RyYXRpb24vZG9jdG9ycmVnaXN0cmF0aW9uLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDBCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9kb2N0b3JyZWdpc3RyYXRpb24vZG9jdG9ycmVnaXN0cmF0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXh3aWR0aHtcclxuICAgIG1heC13aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG59IiwiLm1heHdpZHRoIHtcbiAgbWF4LXdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/doctorregistration/doctorregistration.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/doctorregistration/doctorregistration.page.ts ***!
  \***************************************************************/
/*! exports provided: DoctorregistrationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoctorregistrationPage", function() { return DoctorregistrationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../api/form/form.service */ "./src/app/api/form/form.service.ts");
/* harmony import */ var _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/Camera/ngx */ "./node_modules/@ionic-native/Camera/ngx/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _api_doctorregister_doctorregister_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../api/doctorregister/doctorregister.service */ "./src/app/api/doctorregister/doctorregister.service.ts");
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "./node_modules/@ionic-native/android-permissions/ngx/index.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/location-accuracy/ngx */ "./node_modules/@ionic-native/location-accuracy/ngx/index.js");
















var DoctorregistrationPage = /** @class */ (function () {
    function DoctorregistrationPage(_doctorregister, formBuilder, toastCtrl, router, storage, _form, menuCtrl, _Activatedroute, loadingCtrl, file, camera, actionSheetController, filePath, plt, webview, androidPermissions, geolocation, locationAccuracy) {
        this._doctorregister = _doctorregister;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.storage = storage;
        this._form = _form;
        this.menuCtrl = menuCtrl;
        this._Activatedroute = _Activatedroute;
        this.loadingCtrl = loadingCtrl;
        this.file = file;
        this.camera = camera;
        this.actionSheetController = actionSheetController;
        this.filePath = filePath;
        this.plt = plt;
        this.webview = webview;
        this.androidPermissions = androidPermissions;
        this.geolocation = geolocation;
        this.locationAccuracy = locationAccuracy;
        this.DoctorregisterForm = this.formBuilder.group({
            name: [''],
            age: [''],
            gender: [''],
            speciality: [''],
            city: [''],
            pincode: [''],
            mobile1: [''],
            mobile2: [''],
            Email: [''],
            experience: [''],
            qualification: [''],
            clinicname: [''],
            availablemulti: [''],
            bankholdername: [''],
            accountnumber: [''],
            ifsccode: [''],
            upipin: [''],
            remark: ['']
        });
        this.errorMessages = {
            name: [
                { type: 'required', message: 'Name is required' },
                { type: 'maxlength', message: 'Name cant be longer than 10 characters' }
            ],
            age: [
                { type: 'required', message: 'Age is required' }
            ],
            gender: [
                { type: 'required', message: 'Gender is required' }
            ],
            speciality: [
                { type: 'required', message: 'Speciality is required' }
            ],
            city: [
                { type: 'required', message: 'City is required' }
            ],
            pincode: [
                { type: 'required', message: 'Pincode is required' }
            ],
            mobile1: [
                { type: 'required', message: 'Mobile1 is required' }
            ],
            mobile2: [
                { type: 'required', message: 'Mobile2 is required' }
            ],
            Email: [
                { type: 'required', message: 'Email is required' }
            ],
            experience: [
                { type: 'required', message: 'Experience is required' }
            ],
            qualification: [
                { type: 'required', message: 'Qualification is required' }
            ],
            clinicname: [
                { type: 'required', message: 'Clinic Name is required' }
            ],
            availablemulti: [
                { type: 'required', message: 'Available is required' }
            ],
            bankholdername: [
                { type: 'required', message: 'BankName is required' }
            ],
            accountnumber: [
                { type: 'required', message: 'AccountNumber is required' }
            ],
            ifsccode: [
                { type: 'required', message: 'Ifsc Code is required' }
            ],
            upipin: [
                { type: 'required', message: 'UpiPin is required' }
            ]
        };
        this.locationCoords = {
            latitude: "",
            longitude: "",
            accuracy: ""
        };
    }
    DoctorregistrationPage.prototype.ngOnInit = function () {
    };
    DoctorregistrationPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(true);
        this.specilitylist();
        this.citylist();
        this.servicelist();
        this.checkGPSPermission();
    };
    DoctorregistrationPage.prototype.specilitylist = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this._doctorregister.specilitylist().subscribe(function (data) {
                            loading.dismiss();
                            if (data['success'] == '1') {
                                _this.specilitylistdata = data['data']['specilitylist'];
                                _this.specilityliststatus = true;
                            }
                            else {
                                _this.specilityliststatus = true;
                            }
                        }, function (error) { _this.errorMsg = error.success; });
                        return [2 /*return*/];
                }
            });
        });
    };
    DoctorregistrationPage.prototype.citylist = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this._doctorregister.citylist().subscribe(function (data) {
                            loading.dismiss();
                            if (data['success'] == '1') {
                                _this.citylistdata = data['data']['citylist'];
                                _this.citylistliststatus = true;
                            }
                            else {
                                _this.citylistliststatus = true;
                            }
                        }, function (error) { _this.errorMsg = error.success; });
                        return [2 /*return*/];
                }
            });
        });
    };
    DoctorregistrationPage.prototype.servicelist = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this._doctorregister.servicelist().subscribe(function (data) {
                            loading.dismiss();
                            if (data['success'] == '1') {
                                _this.servicelistdata = data['data']['servicelist'];
                            }
                            else {
                            }
                        }, function (error) { _this.errorMsg = error.success; });
                        return [2 /*return*/];
                }
            });
        });
    };
    DoctorregistrationPage.prototype.getqualification = function ($event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading_1;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.gettreatmentcode = $event.target.value;
                        if (!(this.gettreatmentcode != '')) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Please wait...',
                                mode: "ios"
                            })];
                    case 1:
                        loading_1 = _a.sent();
                        return [4 /*yield*/, loading_1.present()];
                    case 2:
                        _a.sent();
                        this._doctorregister.qualificationlist(this.gettreatmentcode).subscribe(function (data) {
                            loading_1.dismiss();
                            if (data['success'] == '1') {
                                _this.qualificationlistdata = data['data']['qualificationlist'];
                                _this.specilityliststatus = false;
                                _this.qualificationliststatus = true;
                            }
                            else {
                                alert("Doesn't have qualification please select other specility");
                                _this.specilityliststatus = true;
                                _this.qualificationliststatus = false;
                            }
                        }, function (error) { _this.errorMsg = error.success; });
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DoctorregistrationPage.prototype.getpincode = function (cityname) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading_2;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.getcityname = cityname;
                        if (!(this.getcityname != '')) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: 'Please wait...',
                                mode: "ios"
                            })];
                    case 1:
                        loading_2 = _a.sent();
                        return [4 /*yield*/, loading_2.present()];
                    case 2:
                        _a.sent();
                        this._doctorregister.pincodelist(this.getcityname).subscribe(function (data) {
                            _this.cityliststatus = false;
                            loading_2.dismiss();
                            if (data['success'] == '1') {
                                _this.pincodelistdata = data['data']['pincodelist'];
                                _this.citylistliststatus = false;
                                _this.pincodeliststatus = true;
                            }
                            else {
                                _this.citylistliststatus = true;
                                _this.pincodeliststatus = false;
                            }
                        }, function (error) { _this.errorMsg = error.success; });
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    Object.defineProperty(DoctorregistrationPage.prototype, "name", {
        get: function () {
            return this.DoctorregisterForm.get("name");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "age", {
        get: function () {
            return this.DoctorregisterForm.get("age");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "gender", {
        get: function () {
            return this.DoctorregisterForm.get("gender");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "speciality", {
        get: function () {
            return this.DoctorregisterForm.get("speciality");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "city", {
        get: function () {
            return this.DoctorregisterForm.get("city");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "pincode", {
        get: function () {
            return this.DoctorregisterForm.get("pincode");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "mobile1", {
        get: function () {
            return this.DoctorregisterForm.get("mobile1");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "mobile2", {
        get: function () {
            return this.DoctorregisterForm.get("mobile2");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "Email", {
        get: function () {
            return this.DoctorregisterForm.get("Email");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "experience", {
        get: function () {
            return this.DoctorregisterForm.get("experience");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "qualification", {
        get: function () {
            return this.DoctorregisterForm.get("qualification");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "clinicname", {
        get: function () {
            return this.DoctorregisterForm.get("clinicname");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "availablemulti", {
        get: function () {
            return this.DoctorregisterForm.get("availablemulti");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "bankholdername", {
        get: function () {
            return this.DoctorregisterForm.get("bankholdername");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "accountnumber", {
        get: function () {
            return this.DoctorregisterForm.get("accountnumber");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "ifsccode", {
        get: function () {
            return this.DoctorregisterForm.get("ifsccode");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "upipin", {
        get: function () {
            return this.DoctorregisterForm.get("upipin");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DoctorregistrationPage.prototype, "remark", {
        get: function () {
            return this.DoctorregisterForm.get("remark");
        },
        enumerable: true,
        configurable: true
    });
    DoctorregistrationPage.prototype.FilterJsonData = function (ev) {
        this.cityliststatus = true;
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.citylistdata = this.citylistdata.filter(function (item) {
                return (item.city_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    DoctorregistrationPage.prototype.selectImage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: "Select Image source",
                            buttons: [{
                                    text: 'Load from Library',
                                    handler: function () {
                                        _this.pickImage(_this.camera.PictureSourceType.PHOTOLIBRARY);
                                    }
                                },
                                {
                                    text: 'Use Camera',
                                    handler: function () {
                                        _this.pickImage(_this.camera.PictureSourceType.CAMERA);
                                    }
                                },
                                {
                                    text: 'Cancel',
                                    role: 'cancel'
                                }
                            ]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DoctorregistrationPage.prototype.pickImage = function (sourceType) {
        var _this = this;
        var options = {
            quality: 50,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imagePath) {
            _this.imageformdata = imagePath;
            //this.imagebase6 = 'data:image/png;base64,' + imagePath;
            _this.startUpload(imagePath);
            if (_this.plt.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (err) {
            alert("Error in reading file!!!");
        });
    };
    DoctorregistrationPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    DoctorregistrationPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(function (success) {
            var filePath = _this.file.dataDirectory + newFileName;
            _this.imagepathurl = _this.pathForImage(filePath);
        }, function (error) {
        });
    };
    DoctorregistrationPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            var converted = this.webview.convertFileSrc(img);
            return converted;
        }
    };
    DoctorregistrationPage.prototype.startUpload = function (imgEntry) {
        var _this = this;
        this.file.resolveLocalFilesystemUrl(imgEntry)
            .then(function (entry) {
            entry.file(function (file) { return _this.readFile(file); });
        })
            .catch(function (err) {
            alert('Error while reading file.');
        });
    };
    DoctorregistrationPage.prototype.readFile = function (file) {
        var _this = this;
        var reader = new FileReader();
        reader.onload = function () {
            var imgBlob = new Blob([reader.result], {
                type: file.type
            });
            _this.DPF = file;
            _this.DPFblob = imgBlob;
            _this.DPFname = file.name;
        };
        reader.readAsArrayBuffer(file);
    };
    // async setLogoimg(img: FormData) {
    //   const loading =  await this.loadingCtrl.create({
    //     message: 'Please wait...',
    //     mode:"ios"
    //     });
    //     await loading.present();
    //   this._form.updateLogo(img)
    //   .subscribe(
    //     data => {
    //       loading.dismiss();
    //       if (data['status'] !== undefined && data['status'] === true) {
    //       } else {
    //       }
    //     },
    //     error => { this.errorMsg = error.success }
    //   )
    // }
    DoctorregistrationPage.prototype.submit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: 'Please wait...',
                            mode: "ios"
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        if (this.DPF != undefined) {
                            this._doctorregister.doctorregister(this.DoctorregisterForm.value, this.DPF, this.DPFblob, this.DPFname, this.locationCoords.latitude, this.locationCoords.longitude)
                                .subscribe(function (data) {
                                loading.dismiss();
                                if (data['success'] == '1') {
                                    alert("Data Enter Successfully");
                                }
                                else {
                                    alert(data['msg']);
                                }
                                _this.DoctorregisterForm.reset();
                            }, function (error) { _this.errorMsg = error.success; });
                        }
                        else {
                            this._doctorregister.doctorregisterwithoutimage(this.DoctorregisterForm.value, this.locationCoords.latitude, this.locationCoords.longitude)
                                .subscribe(function (data) {
                                loading.dismiss();
                                if (data['success'] == '1') {
                                    alert("Data Enter Successfully");
                                }
                                else {
                                    alert(data['msg']);
                                }
                                _this.DoctorregisterForm.reset();
                            }, function (error) { _this.errorMsg = error.success; });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    //========================================= Geo Location ============================================
    //Check if application having GPS access permission  
    DoctorregistrationPage.prototype.checkGPSPermission = function () {
        var _this = this;
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(function (result) {
            if (result.hasPermission) { //If having permission show 'Turn On GPS' dialogue
                _this.askToTurnOnGPS();
            }
            else { //If not having permission ask for permission
                _this.requestGPSPermission();
            }
        }, function (err) { alert(err); });
    };
    DoctorregistrationPage.prototype.requestGPSPermission = function () {
        var _this = this;
        this.locationAccuracy.canRequest().then(function (canRequest) {
            if (canRequest) {
                console.log("4");
            }
            else { //Show 'GPS Permission Request' dialogue
                _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
                    .then(function () {
                    _this.askToTurnOnGPS();
                }, function (error) {
                    alert('requestPermission Error requesting location permissions ' + error);
                });
            }
        });
    };
    DoctorregistrationPage.prototype.askToTurnOnGPS = function () {
        var _this = this;
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(function () {
            _this.getLocationCoordinates();
        }, function (error) { return alert('Error requesting location permissions ' + JSON.stringify(error)); });
    };
    // Methos to get device accurate coordinates using device GPS
    DoctorregistrationPage.prototype.getLocationCoordinates = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.locationCoords.latitude = resp.coords.latitude;
            _this.locationCoords.longitude = resp.coords.longitude;
            _this.locationCoords.accuracy = resp.coords.accuracy;
            console.log(_this.locationCoords);
            //this.storage.set('location', JSON.stringify(this.locationCoords));
            _this.locationvalue = JSON.stringify(_this.locationCoords);
            console.log(_this.locationCoords.latitude);
        }).catch(function (error) {
            alert('Error getting location' + error);
        });
    };
    DoctorregistrationPage.ctorParameters = function () { return [
        { type: _api_doctorregister_doctorregister_service__WEBPACK_IMPORTED_MODULE_11__["DoctorregisterService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
        { type: _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__["FormService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
        { type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_8__["File"] },
        { type: _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_7__["Camera"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"] },
        { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_9__["FilePath"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
        { type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_10__["WebView"] },
        { type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_12__["AndroidPermissions"] },
        { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_13__["Geolocation"] },
        { type: _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_14__["LocationAccuracy"] }
    ]; };
    DoctorregistrationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-doctorregistration',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./doctorregistration.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/doctorregistration/doctorregistration.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./doctorregistration.page.scss */ "./src/app/doctorregistration/doctorregistration.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_api_doctorregister_doctorregister_service__WEBPACK_IMPORTED_MODULE_11__["DoctorregisterService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
            _api_form_form_service__WEBPACK_IMPORTED_MODULE_6__["FormService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"], _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_8__["File"], _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_7__["Camera"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"], _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_9__["FilePath"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"], _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_10__["WebView"], _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_12__["AndroidPermissions"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_13__["Geolocation"], _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_14__["LocationAccuracy"]])
    ], DoctorregistrationPage);
    return DoctorregistrationPage;
}());



/***/ })

}]);
//# sourceMappingURL=doctorregistration-doctorregistration-module.js.map