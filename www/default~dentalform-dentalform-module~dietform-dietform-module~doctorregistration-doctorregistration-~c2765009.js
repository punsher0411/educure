(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~dentalform-dentalform-module~dietform-dietform-module~doctorregistration-doctorregistration-~c2765009"],{

/***/ "./src/app/api/form/form.service.ts":
/*!******************************************!*\
  !*** ./src/app/api/form/form.service.ts ***!
  \******************************************/
/*! exports provided: FormService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormService", function() { return FormService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _path_path_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../path/path.service */ "./src/app/api/path/path.service.ts");
/* harmony import */ var _login_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../login/auth.service */ "./src/app/api/login/auth.service.ts");







var FormService = /** @class */ (function () {
    function FormService(_http, _url, _auth) {
        this._http = _http;
        this._url = _url;
        this._auth = _auth;
        this._pyshician = this._url.serverurl + 'vitalphysicanform';
        this._dentalform = this._url.serverurl + 'dentalform';
        this._dentalformimg = this._url.serverurl + 'uploadimage';
        this._eyeform = this._url.serverurl + 'eyeform';
        this._dietform = this._url.serverurl + 'dietform';
        this._mentalform = this._url.serverurl + 'mentalform';
        this._dietmealform = this._url.serverurl + 'dietmealform';
    }
    FormService.prototype.getHearder = function () {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json'
        });
        this.httpOptions = { headers: headers };
    };
    FormService.prototype.vitalphysican = function (userData, id, consullent_id, event_id) {
        var formData = new FormData();
        formData.append('height', userData.height);
        formData.append('weight', userData.weight);
        formData.append('temp', userData.temp);
        formData.append('bmi', userData.bmi);
        formData.append('pulse', userData.pulse);
        formData.append('bloodsugar', userData.bloodsugar);
        formData.append('bloodgp', userData.bloodgp);
        formData.append('resprate', userData.resprate);
        formData.append('systolic', userData.systolic);
        formData.append('diastolic', userData.diastolic);
        formData.append('membercode', id);
        formData.append('consultant_id', consullent_id);
        formData.append('event_id', event_id);
        return this._http.post(this._pyshician, formData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
    };
    FormService.prototype.updateLogo = function (data) {
        return this._http.post(this._dentalformimg, data)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
    };
    FormService.prototype.dental = function (userData, id, consullent_id, event_id) {
        var formData = new FormData();
        formData.append('decayed', userData.decayed);
        formData.append('missing', userData.missing);
        formData.append('crown', userData.crown);
        formData.append('givingteath', userData.givingteath);
        formData.append('appearance', userData.appearance);
        formData.append('Stain', userData.Stain);
        formData.append('Stainsub', userData.Stainsub);
        formData.append('calculusteath', userData.calculusteath);
        formData.append('calculusteath2', userData.calculusteath2);
        formData.append('Flurosis', userData.Flurosis);
        formData.append('Malocclusion', userData.Malocclusion);
        formData.append('brushingfrequency', userData.brushingfrequency);
        formData.append('mouthwash', userData.mouthwash);
        formData.append('toothtype', userData.toothtype);
        formData.append('member_code', id);
        formData.append('consultant_id', consullent_id);
        formData.append('event_id', event_id);
        return this._http.post(this._dentalform, formData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
    };
    FormService.prototype.eyeform = function (userData, id, consullent_id, event_id) {
        console.log(consullent_id);
        console.log(id);
        console.log(event_id);
        var formData = new FormData();
        formData.append('Glasses', userData.Glasses);
        formData.append('righteyeno', userData.righteyeno);
        formData.append('righteyeno2', userData.righteyeno2);
        formData.append('spherical', userData.spherical);
        formData.append('cylindrical', userData.cylindrical);
        formData.append('lefteyeno', userData.lefteyeno);
        formData.append('lefteyeno2', userData.lefteyeno2);
        formData.append('leftSpherical', userData.leftSpherical);
        formData.append('leftcylindrical', userData.leftcylindrical);
        formData.append('righteye', userData.righteye);
        formData.append('lefteye', userData.lefteye);
        formData.append('member_code', id);
        formData.append('consultant_id', consullent_id);
        formData.append('event_id', event_id);
        formData.append('remark', userData.remark);
        return this._http.post(this._eyeform, formData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
    };
    FormService.prototype.diet = function (userData, id, quecode, consullent_id, event_id) {
        var formData = new FormData();
        formData.append('member_code', id);
        formData.append('Breakfast', userData.Breakfast);
        formData.append('lunch', userData.lunch);
        formData.append('Evening', userData.Evening);
        formData.append('Dinner', userData.Dinner);
        var formData1 = new FormData();
        formData1.append('member_code', id);
        formData1.append('ansdetailQ120', userData.ansdetailQ120);
        formData1.append('ansdetailQ121', userData.ansdetailQ121);
        formData1.append('ansdetailQ122', userData.ansdetailQ122);
        formData1.append('ansdetailQ123', userData.ansdetailQ123);
        formData1.append('ansdetailQ124', userData.ansdetailQ124);
        formData1.append('ansdetailQ125', userData.ansdetailQ125);
        formData1.append('ansdetailQ126', userData.ansdetailQ126);
        formData1.append('ansdetailQ127', userData.ansdetailQ127);
        formData1.append('ansdetailQ128', userData.ansdetailQ128);
        formData1.append('ansdetailQ129', userData.ansdetailQ129);
        formData1.append('ansdetailQ30', userData.ansdetailQ30);
        formData1.append('ansdetailQ31', userData.ansdetailQ31);
        formData1.append('ansdetailQ225', userData.ansdetailQ225);
        formData1.append('ansdetailQ90', userData.ansdetailQ90);
        formData1.append('ansdetailQ89', userData.ansdetailQ89);
        formData1.append('ansdetailQ400', userData.ansdetailQ400);
        formData1.append('que_code', quecode);
        formData1.append('consultant_id', consullent_id);
        formData1.append('event_id', event_id);
        var response1 = this._http.post(this._dietmealform, formData);
        var response2 = this._http.post(this._dietform, formData1);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["forkJoin"])([response1, response2]);
    };
    FormService.prototype.mentalhealth = function (userData, id, quecode, consullent_id, event_id) {
        var formData1 = new FormData();
        formData1.append('member_code', id);
        formData1.append('ansdetailQ135', userData.ansdetailQ135);
        formData1.append('ansdetailQ136', userData.ansdetailQ136);
        formData1.append('ansdetailQ137', userData.ansdetailQ137);
        formData1.append('ansdetailQ138', userData.ansdetailQ138);
        formData1.append('ansdetailQ139', userData.ansdetailQ139);
        formData1.append('ansdetailQ140', userData.ansdetailQ140);
        formData1.append('ansdetailQ141', userData.ansdetailQ141);
        formData1.append('ansdetailQ142', userData.ansdetailQ142);
        formData1.append('ansdetailQ143', userData.ansdetailQ143);
        formData1.append('ansdetailQ144', userData.ansdetailQ144);
        formData1.append('que_code', quecode);
        formData1.append('consultant_id', consullent_id);
        formData1.append('event_id', event_id);
        return this._http.post(this._mentalform, formData1)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
    };
    FormService.prototype.errorHandler = function (error) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(error);
    };
    FormService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _path_path_service__WEBPACK_IMPORTED_MODULE_5__["PathService"] },
        { type: _login_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"] }
    ]; };
    FormService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _path_path_service__WEBPACK_IMPORTED_MODULE_5__["PathService"], _login_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]])
    ], FormService);
    return FormService;
}());



/***/ }),

/***/ "./src/app/api/login/auth.service.ts":
/*!*******************************************!*\
  !*** ./src/app/api/login/auth.service.ts ***!
  \*******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");



// import { OneSignal } from '@ionic-native/onesignal/ngx';
var AuthService = /** @class */ (function () {
    function AuthService(storage) {
        this.storage = storage;
        this.userid = 'null';
        this.name = 'null';
        this.phone = 'null';
        this.email = 'null';
        this.deviceid = 'null';
        this.ext1 = 'null';
        this.ext2 = 'null';
        this.ext3 = 'null';
        this.set = 'null';
    } //,private oneSignal: OneSignal
    AuthService.prototype.DataSet = function () {
        var _this = this;
        Promise.all([this.storage.get("userid"), this.storage.get("name"),
            this.storage.get("phone"), this.storage.get("email")]).then(function (values) {
            _this.userid = values[0] == undefined ? 'null' : (values[0] != '' ? values[0] : 'null');
            _this.name = values[1] == undefined ? 'null' : (values[1] != '' ? values[1] : 'null');
            _this.phone = values[2] == undefined ? 'null' : (values[2] != '' ? values[2] : 'null');
            _this.email = values[3] == undefined ? 'null' : (values[3] != '' ? values[3] : 'null');
            console.log("All Data SET userid " + _this.userid + " name " + _this.name);
        });
    };
    AuthService.prototype.DeviceSet = function () {
        this.deviceid = '123456789';
        console.log("Set Device ID device3");
        // this.oneSignal.getIds().then(identity => {
        //this.deviceid = identity.userId;
        //  this.deviceid = "device32";
        //});
    };
    Object.defineProperty(AuthService.prototype, "PrepairData", {
        set: function (status) {
            console.log("Set PrepairData ", status);
            this.DataSet();
            this.set = status;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "ReSetData", {
        set: function (status) {
            this.userid = 'null';
            this.name = 'null';
            this.phone = 'null';
            this.email = 'null';
            this.set = status;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "isDeviceid", {
        get: function () {
            if (this.deviceid !== 'null') {
                return this.deviceid;
            }
            else {
                this.DeviceSet();
                return 'No Data';
            }
        },
        set: function (status) {
            this.DeviceSet();
            console.log("Set Device ID ", status);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "isUserid", {
        get: function () {
            if (this.userid !== 'null') {
                return this.userid;
            }
            else {
                this.DataSet();
                return '5e00d0a0000ff000a00000f0';
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "isName", {
        get: function () {
            if (this.name !== 'null') {
                return this.name;
            }
            else {
                this.DataSet();
                return 'No Data';
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "isEmail", {
        get: function () {
            if (this.email !== 'null') {
                return this.email;
            }
            else {
                this.DataSet();
                return 'No Data';
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "isPhone", {
        get: function () {
            if (this.phone !== 'null') {
                return this.phone;
            }
            else {
                this.DataSet();
                return 'No Data';
            }
        },
        enumerable: true,
        configurable: true
    });
    AuthService.ctorParameters = function () { return [
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"] }
    ]; };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/api/path/path.service.ts":
/*!******************************************!*\
  !*** ./src/app/api/path/path.service.ts ***!
  \******************************************/
/*! exports provided: PathService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PathService", function() { return PathService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PathService = /** @class */ (function () {
    function PathService() {
        this.url = 'https://cors-anywhere.herokuapp.com/http://www.planmyhealth.in/planhealthapi/api/';
    }
    Object.defineProperty(PathService.prototype, "serverurl", {
        get: function () {
            return this.url;
        },
        enumerable: true,
        configurable: true
    });
    PathService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PathService);
    return PathService;
}());



/***/ })

}]);
//# sourceMappingURL=default~dentalform-dentalform-module~dietform-dietform-module~doctorregistration-doctorregistration-~c2765009.js.map