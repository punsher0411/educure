import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', canActivate: [AuthGuard], loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'medicalcamp',
    loadChildren: () => import('./medicalcamp/medicalcamp.module').then( m => m.MedicalcampPageModule)
  },
  {
    path: 'physician/:id/:consulentid/:firstname/:lastname/:event_id/:studentclass/:studentrollno/:studentdiv',
    loadChildren: () => import('./physician/physician.module').then( m => m.PhysicianPageModule)
  },
  {
    path: 'eyeform/:id/:consulentid/:firstname/:lastname/:event_id/:studentclass/:studentrollno/:studentdiv',
    loadChildren: () => import('./eyeform/eyeform.module').then( m => m.EyeformPageModule)
  },
  {
    path: 'dentalform/:id/:consulentid/:firstname/:lastname/:event_id/:studentclass/:studentrollno/:studentdiv',
    loadChildren: () => import('./dentalform/dentalform.module').then( m => m.DentalformPageModule)
  },
  {
    path: 'dietform/:id/:consulentid/:firstname/:lastname/:event_id/:studentclass/:studentrollno/:studentdiv',
    loadChildren: () => import('./dietform/dietform.module').then( m => m.DietformPageModule)
  },
  {
    path: 'mentalhealthform/:id/:consulentid/:firstname/:lastname/:event_id/:studentclass/:studentrollno/:studentdiv',
    loadChildren: () => import('./mentalhealthform/mentalhealthform.module').then( m => m.MentalhealthformPageModule)
  },
  {
    path: 'doctorregistration',
    loadChildren: () => import('./doctorregistration/doctorregistration.module').then( m => m.DoctorregistrationPageModule)
  },
  {
    path: 'testingmedical',
    loadChildren: () => import('./testingmedical/testingmedical.module').then( m => m.TestingmedicalPageModule)
  },
  {
    path: 'testingform/:id/:specilitycode',
    loadChildren: () => import('./testingform/testingform.module').then( m => m.TestingformPageModule)
  },
  {
    path: 'role',
    loadChildren: () => import('./role/role.module').then( m => m.RolePageModule)
  },
  {
    path: 'memberdashboard',
    loadChildren: () => import('./memberdashboard/memberdashboard.module').then( m => m.MemberdashboardPageModule)
  },
  {
    path: 'medicalrecordmember',
    loadChildren: () => import('./medicalrecordmember/medicalrecordmember.module').then( m => m.MedicalrecordmemberPageModule)
  },
  {
    path: 'medicalrecordsdetails/:id',
    loadChildren: () => import('./medicalrecordsdetails/medicalrecordsdetails.module').then( m => m.MedicalrecordsdetailsPageModule)
  },
  {
    path: 'planslist',
    loadChildren: () => import('./planslist/planslist.module').then( m => m.PlanslistPageModule)
  },
  {
    path: 'chatbot',
    loadChildren: () => import('./chatbot/chatbot.module').then( m => m.ChatbotPageModule)
  },
  {
    path: 'studentregistration',
    loadChildren: () => import('./studentregistration/studentregistration.module').then( m => m.StudentregistrationPageModule)
  },
  {
    path: 'memberraisequery',
    loadChildren: () => import('./memberraisequery/memberraisequery.module').then( m => m.MemberraisequeryPageModule)
  },
  {
    path: 'memberraisequerylist',
    loadChildren: () => import('./memberraisequerylist/memberraisequerylist.module').then( m => m.MemberraisequerylistPageModule)
  },
  {
    path: 'doctorraisequerylist',
    loadChildren: () => import('./doctorraisequerylist/doctorraisequerylist.module').then( m => m.DoctorraisequerylistPageModule)
  },
  {
    path: 'doctorraisequery',
    loadChildren: () => import('./doctorraisequery/doctorraisequery.module').then( m => m.DoctorraisequeryPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
