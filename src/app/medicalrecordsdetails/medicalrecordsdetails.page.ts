import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { ToastController, MenuController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { MedicalcampService } from '../api/medicalcamp/medicalcamp.service';
@Component({
  selector: 'app-medicalrecordsdetails',
  templateUrl: './medicalrecordsdetails.page.html',
  styleUrls: ['./medicalrecordsdetails.page.scss'],
})
export class MedicalrecordsdetailsPage implements OnInit {
  id: string;
  membercode: any;
  recordstatus: boolean;
  recorddata: any;
  errorMsg: any;
  recorddatafetch: any;
  medicalrecord: string = "Information";
  recorddataqueans: { valuedate: any; }[];

  constructor( public toastCtrl: ToastController,
		private router: Router, public menuCtrl: MenuController,private _Activatedroute: ActivatedRoute,public loadingCtrl: LoadingController, private storage: Storage,private _medicalcamp: MedicalcampService) { }

  ngOnInit() {
    this.id = this._Activatedroute.snapshot.paramMap.get('id');
    this.storage.get('membercode').then((val) => {
      this.membercode = val;
    });
    if (this.id == 'Physician') {
      this.Physician();
      this.questionanswer('1');
    }
    else if (this.id == 'Eye') {
      this.eye();
      this.questionanswer('3');
    }
    else if (this.id == 'dental') {
      this.dental();
      this.questionanswer('2');
    }
    else if (this.id == 'diet') {
      this.diet();
      this.questionanswer('5');
    }
    else if (this.id == 'mentalhealth') {
      this.mentalhealth();
      this.questionanswer('4');
    }
  }

  async Physician() {
    const loading =  await this.loadingCtrl.create({
      message: 'Please wait...',
      mode:"ios"
    });
    await loading.present();
    this.storage.get('membercode').then((val) => {    
    this._medicalcamp.vitalmember(val).subscribe(
      data => {
        loading.dismiss();
         if (data['success'] == '1') {
          this.recorddata = data['data'];
          this.recordstatus = true;
         }
         else{
          this.recordstatus = false;
         }
      },
      error => { this.errorMsg = error.success }
    )
  });
  }

  async eye() {
    const loading =  await this.loadingCtrl.create({
      message: 'Please wait...',
      mode:"ios"
    });
    await loading.present();
    this.storage.get('membercode').then((val) => {    
    this._medicalcamp.eyemember(val).subscribe(
      data => {
        loading.dismiss();
         if (data['success'] == '1') {
          this.recorddata = data['data'];
          this.recordstatus = true;
         }
         else{
          this.recordstatus = false;
         }
      },
      error => { this.errorMsg = error.success }
    )
  });
  }
  async dental() {
    const loading =  await this.loadingCtrl.create({
      message: 'Please wait...',
      mode:"ios"
    });
    await loading.present();
    this.storage.get('membercode').then((val) => {    
    this._medicalcamp.dentalmember(val).subscribe(
      data => {
        loading.dismiss();
         if (data['success'] == '1') {
          this.recorddata = data['data'];
          this.recordstatus = true;
         }
         else{
          this.recordstatus = false;
         }
      },
      error => { this.errorMsg = error.success }
    )
  });
  }

  async diet() {
    const loading =  await this.loadingCtrl.create({
      message: 'Please wait...',
      mode:"ios"
    });
    await loading.present();
    this.storage.get('membercode').then((val) => {    
    this._medicalcamp.diet(val).subscribe(
      data => {
        loading.dismiss();
         if (data['success'] == '1') {
          this.recorddata = data['data'];
          this.recordstatus = true;
         }
         else{
          this.recordstatus = false;
         }
      },
      error => { this.errorMsg = error.success }
    )
  });
  }

  async mentalhealth() {
    const loading =  await this.loadingCtrl.create({
      message: 'Please wait...',
      mode:"ios"
    });
    await loading.present();
    this.storage.get('membercode').then((val) => {    
    this._medicalcamp.mentalhealth(val).subscribe(
      data => {
        loading.dismiss();
         if (data['success'] == '1') {
          this.recorddata = data['data'];
          this.recordstatus = true;
         }
         else{
          this.recordstatus = false;
         }
      },
      error => { this.errorMsg = error.success }
    )
  });
  }



  async questionanswer(id) {
    const loading =  await this.loadingCtrl.create({
      message: 'Please wait...',
      mode:"ios"
    });
    await loading.present();
    this.storage.get('membercode').then((val) => {    
    this._medicalcamp.questionanswermember(val,id).subscribe(
      data => {
        loading.dismiss();
        if (data['success'] == '1') {
           
          this.recorddatafetch = data['data'];
          const groups = this.recorddatafetch.reduce((groups, game) => {
            const date = game.entry_date.split(' ')[0];
            if (!groups[date]) {
              groups[date] = [];
            }
            groups[date].push(game);
            return groups;
          }, {});
          
          // Edit: to add it in the array format instead
          const groupArrays = Object.keys(groups).map((date) => {
            return {
              valuedate: groups[date]
            };
          });
          this.recorddataqueans = groupArrays;
          console.log(groupArrays);
          this.recordstatus = false;
          
         }
         else{
          this.recordstatus = false;
         }
      },
      error => { this.errorMsg = error.success }
    )
  });
  }

}
