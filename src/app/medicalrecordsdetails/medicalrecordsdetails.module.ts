import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MedicalrecordsdetailsPageRoutingModule } from './medicalrecordsdetails-routing.module';

import { MedicalrecordsdetailsPage } from './medicalrecordsdetails.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MedicalrecordsdetailsPageRoutingModule
  ],
  declarations: [MedicalrecordsdetailsPage]
})
export class MedicalrecordsdetailsPageModule {}
