import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MedicalrecordsdetailsPage } from './medicalrecordsdetails.page';

const routes: Routes = [
  {
    path: '',
    component: MedicalrecordsdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MedicalrecordsdetailsPageRoutingModule {}
