import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Storage } from "@ionic/storage";
import { HomePage } from './home/home.page';
import { LoginPage } from './login/login.page';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private storage: Storage) { }
 canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<any> | Promise<any> {
    return new Promise((resolve) => {
      
      this.storage.get('mobile_no').then((val) => {
        if(val != null){
          resolve(true);
                 }
        else {	
          this.storage.get('membercode').then((val) => {
            if (val != null) {
              resolve(false);
              this.router.navigate(['/memberdashboard']);
            } else {
              this.storage.get('roleapp').then((value) => {
                if (value != null) {
                  resolve(false);
                  this.router.navigate(['/login']);
                } else {
            
                  resolve(false);
                  this.router.navigate(['/role']);
                }
              });
            }
        });


        }
      });
    });
  }
  
}
