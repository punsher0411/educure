import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
@Component({
  selector: 'app-memberdashboard',
  templateUrl: './memberdashboard.page.html',
  styleUrls: ['./memberdashboard.page.scss'],
})
export class MemberdashboardPage  {

  constructor( public menuCtrl: MenuController, private storage: Storage,private router: Router) {}
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }


  logout() {
    this.storage.clear();
    this.storage.set('roleapp', 'member');
    this.router.navigate(['/login']);
  }

}
