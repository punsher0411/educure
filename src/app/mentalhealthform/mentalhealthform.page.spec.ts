import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MentalhealthformPage } from './mentalhealthform.page';

describe('MentalhealthformPage', () => {
  let component: MentalhealthformPage;
  let fixture: ComponentFixture<MentalhealthformPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentalhealthformPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MentalhealthformPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
