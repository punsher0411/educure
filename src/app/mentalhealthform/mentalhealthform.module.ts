import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MentalhealthformPageRoutingModule } from './mentalhealthform-routing.module';

import { MentalhealthformPage } from './mentalhealthform.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    MentalhealthformPageRoutingModule
  ],
  declarations: [MentalhealthformPage]
})
export class MentalhealthformPageModule {}
