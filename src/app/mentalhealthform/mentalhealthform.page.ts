import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { ToastController, MenuController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FormService } from '../api/form/form.service';
@Component({
  selector: 'app-mentalhealthform',
  templateUrl: './mentalhealthform.page.html',
  styleUrls: ['./mentalhealthform.page.scss'],
})
export class MentalhealthformPage implements OnInit {
  errorMsg: any;
  id: string;
  messages: string[];
  firstname: string;
  lastname: string;
  consoluentid: string;
  event_id: string;
  studentclass: string;
  studentrollno: string;
  studentdiv: string;
  constructor(private formBuilder: FormBuilder, public toastCtrl: ToastController,
		private router: Router, private storage: Storage,
		private _form: FormService, public menuCtrl: MenuController,private _Activatedroute: ActivatedRoute,public loadingCtrl: LoadingController) { }

    ngOnInit() {
      this.id = this._Activatedroute.snapshot.paramMap.get('id');
      this.firstname = this._Activatedroute.snapshot.paramMap.get('firstname');
      this.lastname = this._Activatedroute.snapshot.paramMap.get('lastname');
      this.consoluentid = this._Activatedroute.snapshot.paramMap.get('consulentid');
      this.event_id = this._Activatedroute.snapshot.paramMap.get('event_id');
      this.studentclass = this._Activatedroute.snapshot.paramMap.get('studentclass');
      this.studentrollno = this._Activatedroute.snapshot.paramMap.get('studentrollno');
      this.studentdiv = this._Activatedroute.snapshot.paramMap.get('studentdiv');
    }

    MentalForm = this.formBuilder.group({
      ansdetailQ135: ['Average'],
      ansdetailQ136: ['Average'],
      ansdetailQ137: ['Average'],
      ansdetailQ138: ['Average'],
      ansdetailQ139: ['Average'],
      ansdetailQ140: ['Maintains'],
      ansdetailQ141: ['High'],
      ansdetailQ142: ['Yes'],
      ansdetailQ143: ['Yes'],
      ansdetailQ144: ['Extrovert']
    });
  
    get ansdetailQ135() {
      return this.MentalForm.get("ansdetailQ135");
    }
    get ansdetailQ136() {
      return this.MentalForm.get("ansdetailQ136");
    }
    get ansdetailQ137() {
      return this.MentalForm.get("ansdetailQ137");
    }
    get ansdetailQ138() {
      return this.MentalForm.get("ansdetailQ138");
    }
    get ansdetailQ139() {
      return this.MentalForm.get("ansdetailQ139");
    }
    get ansdetailQ140() {
      return this.MentalForm.get("ansdetailQ140");
    }
    get ansdetailQ141() {
      return this.MentalForm.get("ansdetailQ141");
    }
    get ansdetailQ142() {
      return this.MentalForm.get("ansdetailQ142");
    }
    get ansdetailQ143() {
      return this.MentalForm.get("ansdetailQ143");
    }
    get ansdetailQ129() {
      return this.MentalForm.get("ansdetailQ129");
    }
    get ansdetailQ144() {
      return this.MentalForm.get("ansdetailQ144");
    }
  
    public async submit() {
      const loading =  await this.loadingCtrl.create({
        message: 'Please wait...',
        mode:"ios"
        });
        await loading.present();
      this.messages = ['Q135', 'Q136', 'Q137', 'Q138','Q139', 'Q140','Q141', 'Q142','Q143', 'Q144'];
      this._form.mentalhealth(this.MentalForm.value,this.id,this.messages,this.consoluentid,this.event_id)
        .subscribe(
          data => {
            loading.dismiss();
            if (data['success'] == '1') {
              alert("Data Enter Successfully");
              this.router.navigate(['/medicalcamp']);
            } else {
              alert(data['msg']);
              this.MentalForm.reset();
            }
          },
          error => { this.errorMsg = error.success }
        )
  
    }
  

}
