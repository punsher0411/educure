import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MentalhealthformPage } from './mentalhealthform.page';

const routes: Routes = [
  {
    path: '',
    component: MentalhealthformPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MentalhealthformPageRoutingModule {}
