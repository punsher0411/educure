import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  providername: any;
  email: any;
  roleapp: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    private router: Router,
  ) {
    this.storage.get('roleapp').then((value) => {
      this.roleapp = value;
    });
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  LogOut() {
    this.storage.clear();
    this.storage.set('roleapp', 'doctor');
    this.router.navigate(['/login']);
  }
  ngOnInit() {
  Promise.all([this.storage.get("provider_name"), this.storage.get("e_mail_id")]).then(values => {
    this.providername = values[0];
    this.email = values[1];
  });
}
}
