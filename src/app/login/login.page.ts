import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController, MenuController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoginService } from '../api/login/login.service';
import { AuthService } from '../api/login/auth.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
	errorMsg: any;
  loading: Promise<HTMLIonLoadingElement>;
  locationCoords: any;
  locationvalue: any;
  roleapp: any;
  constructor(private formBuilder: FormBuilder, public toastCtrl: ToastController,
		private router: Router, private storage: Storage,
		private _login: LoginService, private _auth: AuthService, public menuCtrl: MenuController,public loadingCtrl: LoadingController, private androidPermissions: AndroidPermissions, private geolocation: Geolocation, private locationAccuracy: LocationAccuracy) { 
      this.locationCoords = {
      latitude: "",
      longitude: "",
      accuracy: ""
    }
  }
    

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(false);
    this.checkGPSPermission();
    this.storage.get('roleapp').then((value) => {
      this.roleapp = value;
    });
  }

  LoginForm = this.formBuilder.group({
	    mobile: ['', [Validators.required, Validators.maxLength(10)]],
		password: ['', [Validators.required]
		]
	});

	get mobile() {
		return this.LoginForm.get("mobile");
	}
	get password() {
		return this.LoginForm.get("password");
	}

	public errorMessages = {
		mobile: [
			{ type: 'required', message: 'Mobile Number is required' },
			{ type: 'maxlength', message: 'Mobile Number cant be longer than 10 characters' }
		],
		password: [
			{ type: 'required', message: 'Password is required' }
		]
	};

	public async submit() {
    const loading =  await this.loadingCtrl.create({
      message: 'Please wait...',
      mode:"ios"
    });
    await loading.present();
    console.log("hi");
    this._login.login(this.LoginForm.value,this.roleapp)
      .subscribe(
         data => {
            loading.dismiss();
          if (data['success'] == '1') {
            if (this.roleapp == 'member') {
               this.storage.set('membercode', this.LoginForm.value.mobile);
              this.router.navigate(['/memberdashboard']); 
            } else {
              this.storage.set('provider_no', data['data'][0]['provider_no']);
            this.storage.set('provider_code', data['data'][0]['provider_code']);
            this.storage.set('provider_name', data['data'][0]['provider_name']);
            this.storage.set('speciality_type', data['data'][0]['speciality_type']);
            this.storage.set('speciality_code', data['data'][0]['speciality_code']);
            this.storage.set('category_code', data['data'][0]['category_code']);
            this.storage.set('mobile_no', data['data'][0]['mobile_no']);
            this.storage.set('provider_name', data['data'][0]['provider_name']);
            this.storage.set('e_mail_id', data['data'][0]['e_mail_id']);
              this.router.navigate(['/home']); 
            }
                 
          } else {
            alert(data['msg']);
            this.LoginForm.reset();
          }
        },
        error => { this.errorMsg = error.success }
      )

  }

  //========================================= Geo Location ============================================

  //Check if application having GPS access permission  
  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {//If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
        } else { //If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => { alert(err); }
    );
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else { //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(() => { // call method to turn on GPS
            this.askToTurnOnGPS();
          },
            error => { //Show alert if user click on 'No Thanks'
              alert('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => { // When GPS Turned ON call method to get Accurate location coordinates
        this.getLocationCoordinates()
      },
      error => alert('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

  // Methos to get device accurate coordinates using device GPS
  getLocationCoordinates() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.locationCoords.latitude = resp.coords.latitude;
      this.locationCoords.longitude = resp.coords.longitude;
      this.locationCoords.accuracy = resp.coords.accuracy;
      console.log(this.locationCoords);
      //this.storage.set('location', JSON.stringify(this.locationCoords));
      this.locationvalue = JSON.stringify(this.locationCoords);
      console.log(this.locationCoords.latitude);
     
    }).catch((error) => {
      alert('Error getting location' + error);
    });
  }

  
  //======================================= Geo Location End ==========================================
   


}
