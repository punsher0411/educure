import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DietformPageRoutingModule } from './dietform-routing.module';

import { DietformPage } from './dietform.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    DietformPageRoutingModule
  ],
  declarations: [DietformPage]
})
export class DietformPageModule {}
