import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { ToastController, MenuController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FormService } from '../api/form/form.service';
@Component({
  selector: 'app-dietform',
  templateUrl: './dietform.page.html',
  styleUrls: ['./dietform.page.scss'],
})
export class DietformPage implements OnInit {
  errorMsg: any;
  id: string;
  messages: (string | number)[];
	firstname: string;
	lastname: string;
	consoluentid: string;
	event_id: string;
	studentclass: string;
	studentrollno: string;
	studentdiv: string;
  constructor(private formBuilder: FormBuilder, public toastCtrl: ToastController,
		private router: Router, private storage: Storage,
		private _form: FormService, public menuCtrl: MenuController,private _Activatedroute: ActivatedRoute,public loadingCtrl: LoadingController) { }

    ngOnInit() {
      this.id = this._Activatedroute.snapshot.paramMap.get('id');
	  this.firstname = this._Activatedroute.snapshot.paramMap.get('firstname');
	  this.lastname = this._Activatedroute.snapshot.paramMap.get('lastname');
	  this.consoluentid = this._Activatedroute.snapshot.paramMap.get('consulentid');
	  this.event_id = this._Activatedroute.snapshot.paramMap.get('event_id');
	  this.studentclass = this._Activatedroute.snapshot.paramMap.get('studentclass');
      this.studentrollno = this._Activatedroute.snapshot.paramMap.get('studentrollno');
	  this.studentdiv = this._Activatedroute.snapshot.paramMap.get('studentdiv');
    }
  createRange(number){
    var items: number[] = [];
    for(var i = 1; i <= number; i++){
       items.push(i);
    }
    return items;
  }
  createparticularRange(start,number){
    var items: number[] = [];
    for(var i = start; i <= number; i++){
       items.push(i);
    }
    return items;
  }

  DietForm = this.formBuilder.group({
    Breakfast: ['Milk'],
		lunch: ['Roti'],
    Evening: ['Milk'],
		Dinner: ['Milk'],
    ansdetailQ120: ['Adequate'],
		ansdetailQ121: ['Adequate'],
    ansdetailQ122: ['Adequate'],
		ansdetailQ123: ['Adequate'],
    ansdetailQ124: ['Adequate'],
		ansdetailQ125: ['Yes'],
    ansdetailQ126: ['Yes'],
		ansdetailQ127: ['Yes'],
		ansdetailQ128: ['Yes'],
		ansdetailQ129: ['Yes'],
		ansdetailQ30: ['Veg'],
		ansdetailQ31: ['2'],
		ansdetailQ90: ['4'],
		ansdetailQ400: ['Yes'],
		ansdetailQ225: ['Thin'],
		ansdetailQ89: ['Yes'],
	});

	get Breakfast() {
		return this.DietForm.get("Breakfast");
	}
	get lunch() {
		return this.DietForm.get("lunch");
	}
	get Evening() {
		return this.DietForm.get("Evening");
	}
	get Dinner() {
		return this.DietForm.get("Dinner");
	}
	get ansdetailQ120() {
		return this.DietForm.get("ansdetailQ120");
	}
	get ansdetailQ121() {
		return this.DietForm.get("ansdetailQ121");
	}
	get ansdetailQ122() {
		return this.DietForm.get("ansdetailQ122");
	}
	get ansdetailQ123() {
		return this.DietForm.get("ansdetailQ123");
	}
	get ansdetailQ124() {
		return this.DietForm.get("ansdetailQ124");
	}
	get ansdetailQ125() {
		return this.DietForm.get("ansdetailQ125");
	}
	get ansdetailQ126() {
		return this.DietForm.get("ansdetailQ126");
	}
	get ansdetailQ127() {
		return this.DietForm.get("ansdetailQ127");
	}
	get ansdetailQ128() {
		return this.DietForm.get("ansdetailQ128");
	}
	get ansdetailQ129() {
		return this.DietForm.get("ansdetailQ129");
	}
	get ansdetailQ30() {
		return this.DietForm.get("ansdetailQ30");
	}
	get ansdetailQ31() {
		return this.DietForm.get("ansdetailQ31");
	}
	get ansdetailQ90() {
		return this.DietForm.get("ansdetailQ90");
	}
	get ansdetailQ400() {
		return this.DietForm.get("ansdetailQ400");
	}
	get ansdetailQ225() {
		return this.DietForm.get("ansdetailQ225");
	}
	get ansdetailQ89() {
		return this.DietForm.get("ansdetailQ89");
	}
	

	public async submit() {
		const loading =  await this.loadingCtrl.create({
			message: 'Please wait...',
			mode:"ios"
		  });
		  await loading.present();
    this.messages = ['Q120', 'Q121', 'Q122', 'Q123','Q124', 'Q125','Q126', 'Q127','Q128', 'Q129','Q30','Q31','Q90','Q400','Q225','Q89'];
    this._form.diet(this.DietForm.value,this.id,this.messages,this.consoluentid,this.event_id)
      .subscribe(
        data => {
			loading.dismiss();
          if (data[0]['success'] == '1') {
            alert("Data Enter Successfully");
            this.router.navigate(['/medicalcamp']);
          } else {
            alert(data[0]['msg']);
            this.DietForm.reset();
          }
        },
        error => { this.errorMsg = error.success }
      )

  }


}
