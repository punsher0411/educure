import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DietformPage } from './dietform.page';

const routes: Routes = [
  {
    path: '',
    component: DietformPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DietformPageRoutingModule {}
