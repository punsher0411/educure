import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DietformPage } from './dietform.page';

describe('DietformPage', () => {
  let component: DietformPage;
  let fixture: ComponentFixture<DietformPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietformPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DietformPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
