import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DoctorraisequeryPage } from './doctorraisequery.page';

const routes: Routes = [
  {
    path: '',
    component: DoctorraisequeryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DoctorraisequeryPageRoutingModule {}
