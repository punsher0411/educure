import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DoctorraisequeryPageRoutingModule } from './doctorraisequery-routing.module';

import { DoctorraisequeryPage } from './doctorraisequery.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DoctorraisequeryPageRoutingModule
  ],
  declarations: [DoctorraisequeryPage]
})
export class DoctorraisequeryPageModule {}
