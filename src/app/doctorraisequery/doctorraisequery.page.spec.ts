import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DoctorraisequeryPage } from './doctorraisequery.page';

describe('DoctorraisequeryPage', () => {
  let component: DoctorraisequeryPage;
  let fixture: ComponentFixture<DoctorraisequeryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorraisequeryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DoctorraisequeryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
