import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EyeformPage } from './eyeform.page';

describe('EyeformPage', () => {
  let component: EyeformPage;
  let fixture: ComponentFixture<EyeformPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EyeformPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EyeformPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
