import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EyeformPageRoutingModule } from './eyeform-routing.module';

import { EyeformPage } from './eyeform.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    EyeformPageRoutingModule
  ],
  declarations: [EyeformPage]
})
export class EyeformPageModule {}
