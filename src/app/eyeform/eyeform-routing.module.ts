import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EyeformPage } from './eyeform.page';

const routes: Routes = [
  {
    path: '',
    component: EyeformPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EyeformPageRoutingModule {}
