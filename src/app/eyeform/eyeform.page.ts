import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { ToastController, MenuController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FormService } from '../api/form/form.service';
@Component({
  selector: 'app-eyeform',
  templateUrl: './eyeform.page.html',
  styleUrls: ['./eyeform.page.scss'],
})
export class EyeformPage implements OnInit {
  errorMsg: any;
  id: string;
  firstname: string;
  lastname: string;
  consoluentid: string;
  event_id: string;
  studentclass: string;
  studentrollno: string;
  studentdiv: string;
  constructor(private formBuilder: FormBuilder, public toastCtrl: ToastController,
		private router: Router, private storage: Storage,
		private _form: FormService, public menuCtrl: MenuController,private _Activatedroute: ActivatedRoute,public loadingCtrl: LoadingController) { }

  ngOnInit() {
    this.id = this._Activatedroute.snapshot.paramMap.get('id');
    this.firstname = this._Activatedroute.snapshot.paramMap.get('firstname');
    this.lastname = this._Activatedroute.snapshot.paramMap.get('lastname');
    this.consoluentid = this._Activatedroute.snapshot.paramMap.get('consulentid');
    this.event_id = this._Activatedroute.snapshot.paramMap.get('event_id');
    this.studentclass = this._Activatedroute.snapshot.paramMap.get('studentclass');
      this.studentrollno = this._Activatedroute.snapshot.paramMap.get('studentrollno');
      this.studentdiv = this._Activatedroute.snapshot.paramMap.get('studentdiv');
  }
  createRange(number){
    var items: number[] = [];
    for(var i = 1; i <= number; i++){
       items.push(i);
    }
    return items;
  }
  createparticularRange(start,number){
    var items: number[] = [];
    for(var i = start; i <= number; i++){
       items.push(i);
    }
    return items;
  }


  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  EyeForm = this.formBuilder.group({
    Glasses: ['No'],
		righteyeno: ['6'],
    righteyeno2: ['6'],
		spherical: ['6'],
    cylindrical: ['6'],
		lefteyeno: ['6'],
    lefteyeno2: ['6'],
		leftSpherical: ['6'],
    leftcylindrical: ['6'],
		righteye: ['Normal'],
    lefteye: ['Normal'],
		remark: [''],
	});

	get Glasses() {
		return this.EyeForm.get("Glasses");
	}
	get righteyeno() {
		return this.EyeForm.get("righteyeno");
	}
	get righteyeno2() {
		return this.EyeForm.get("righteyeno2");
	}
	get spherical() {
		return this.EyeForm.get("spherical");
	}
	get cylindrical() {
		return this.EyeForm.get("cylindrical");
	}
	get lefteyeno() {
		return this.EyeForm.get("lefteyeno");
	}
	get lefteyeno2() {
		return this.EyeForm.get("lefteyeno2");
	}
	get leftSpherical() {
		return this.EyeForm.get("leftSpherical");
	}
	get leftcylindrical() {
		return this.EyeForm.get("leftcylindrical");
	}
	get righteye() {
		return this.EyeForm.get("righteye");
	}
	get lefteye() {
		return this.EyeForm.get("lefteye");
	}
	get remark() {
		return this.EyeForm.get("remark");
	}

	public async submit() {
    const loading =  await this.loadingCtrl.create({
			message: 'Please wait...',
			mode:"ios"
		  });
		  await loading.present();
    this._form.eyeform(this.EyeForm.value,this.id,this.consoluentid,this.event_id)
      .subscribe(
        data => {
          loading.dismiss();
          if (data['success'] == '1') {
            alert("Data Enter Successfully");
            this.router.navigate(['/medicalcamp']);
          } else {
            alert(data['msg']);
            this.EyeForm.reset();
          }
        },
        error => { this.errorMsg = error.success }
      )

  }

}
