import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { ToastController, MenuController, LoadingController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FormService } from '../api/form/form.service';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { ActionSheetController } from '@ionic/angular';
import { FilePath } from '@ionic-native/file-path/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { DoctorregisterService } from '../api/doctorregister/doctorregister.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
@Component({
  selector: 'app-studentregistration',
  templateUrl: './studentregistration.page.html',
  styleUrls: ['./studentregistration.page.scss'],
})
export class StudentregistrationPage implements OnInit {
  errorMsg: any;
  imagepathurl: string;
  imageformdata: any;
  specilitylistdata: any;
  specilityliststatus: boolean;
  citylistdata: any;
  citylistliststatus: boolean;
  gettreatmentcode: any;
  qualificationlistdata: any;
  qualificationliststatus: boolean;
  getcityname: any;
  pincodelistdata: any;
  pincodeliststatus: boolean;
  cityliststatus: boolean;
  servicelistdata: any;
  imagebase6: string;
  DPF: any ;
  DPFblob: any ;
  DPFname: any;
  locationCoords: any;
  locationvalue: any;
  constructor(private _doctorregister: DoctorregisterService,private formBuilder: FormBuilder, public toastCtrl: ToastController,
		private router: Router, private storage: Storage,
		private _form: FormService, public menuCtrl: MenuController,private _Activatedroute: ActivatedRoute,public loadingCtrl: LoadingController,private file: File, private camera: Camera,
    public actionSheetController: ActionSheetController, private filePath: FilePath, private plt: Platform, private webview: WebView, private androidPermissions: AndroidPermissions, private geolocation: Geolocation, private locationAccuracy: LocationAccuracy) { 
      this.locationCoords = {
        latitude: "",
        longitude: "",
        accuracy: ""
      }
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.citylist();
  }

  FilterJsonData(ev:any){
    this.cityliststatus = true;
    const val = ev.target.value;
    if(val && val.trim() != ''){
    this.citylistdata = this.citylistdata.filter((item)=>{
      return (item.city_name.toLowerCase().indexOf(val.toLowerCase())>-1);
    })
    }
  }
  
  async getpincode(cityname){
    this.getcityname = cityname;
    this.cityliststatus = false;
  }

  ngOnInit() {
  }
  async citylist() {
    const loading =  await this.loadingCtrl.create({
      message: 'Please wait...',
      mode:"ios"
    });
    await loading.present();   
    this._doctorregister.citylist().subscribe(
      data => {
        loading.dismiss();
         if (data['success'] == '1') {
          this.citylistdata = data['data']['citylist'];
          this.citylistliststatus = true;
         }
         else{
          this.citylistliststatus = true;
         }
      },
      error => { this.errorMsg = error.success }
    )
  }

  DoctorregisterForm = this.formBuilder.group({
    name: [''],
    age: [''],
    gender: [''],
    city: [''],
    class: [''],
    parentname: [''],
    schoolname: ['']
  
  });
  
  get name() {
    return this.DoctorregisterForm.get("name");
  }
  get age() {
    return this.DoctorregisterForm.get("age");
  }
  get gender() {
    return this.DoctorregisterForm.get("gender");
  }
  get city() {
    return this.DoctorregisterForm.get("city");
  }
  get class() {
    return this.DoctorregisterForm.get("class");
  }
  get parentname() {
    return this.DoctorregisterForm.get("parentname");
  }
  get schoolname() {
    return this.DoctorregisterForm.get("schoolname");
  }
  
  public errorMessages = {
    name: [
      { type: 'required', message: 'Name is required' },
      { type: 'maxlength', message: 'Name cant be longer than 10 characters' }
    ],
    age: [
      { type: 'required', message: 'Age is required' }
    ]
    ,
    gender: [
      { type: 'required', message: 'Gender is required' }
    ]
    ,
    city: [
      { type: 'required', message: 'City is required' }
    ]
    ,
    class: [
      { type: 'required', message: 'class is required' }
    ]
    ,
    parentname: [
      { type: 'required', message: 'parentname  is required' }
    ]
    ,
    schoolname: [
      { type: 'required', message: 'schoolname is required' }
    ]
  };


  public async submit() {
    const loading =  await this.loadingCtrl.create({
      message: 'Please wait...',
      mode:"ios"
      });
      await loading.present();
      this._doctorregister.memberegister(this.DoctorregisterForm.value)
      .subscribe(
        data => {
         loading.dismiss();
          if (data['success'] == '1') {
            alert("Data Enter Successfully");
          } else {
            alert(data['msg']);
            
          }
          this.DoctorregisterForm.reset();
        },
        error => { this.errorMsg = error.success }
      )
    }

}
