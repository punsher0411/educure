import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlanslistPage } from './planslist.page';

const routes: Routes = [
  {
    path: '',
    component: PlanslistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlanslistPageRoutingModule {}
