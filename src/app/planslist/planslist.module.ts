import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PlanslistPageRoutingModule } from './planslist-routing.module';

import { PlanslistPage } from './planslist.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PlanslistPageRoutingModule
  ],
  declarations: [PlanslistPage]
})
export class PlanslistPageModule {}
