import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ActivatedRoute,Router } from '@angular/router';
import { ToastController, MenuController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { MedicalcampService } from '../api/medicalcamp/medicalcamp.service';
import { PathService } from 'New folder/src/app/api/path/path.service';
@Component({
  selector: 'app-planslist',
  templateUrl: './planslist.page.html',
  styleUrls: ['./planslist.page.scss'],
})
export class PlanslistPage implements OnInit {
  planlistdata: any;
  planliststatus: boolean;
  errorMsg: any;
  tpskelton: any = [1, 2, 3, 4];
  producturl: string;
  constructor(public alertController: AlertController,public toastCtrl: ToastController,
		private router: Router, public menuCtrl: MenuController,private _Activatedroute: ActivatedRoute,public loadingCtrl: LoadingController, private storage: Storage,private _medicalcamp: MedicalcampService, private _url: PathService) { }

  ngOnInit() {
  }
  ionViewWillEnter() {
    
    this.producturl = this._url.productimageurl; 
    this.getplans();
  }

  async getplans() {
    
    this._medicalcamp.planlist()
      .subscribe(
        async data => { 
          this.tpskelton = false;
          if (data['success'] == '1') {
            this.planlistdata = data['data']['planlist'];
            this.planliststatus = false;    
          } else {
          }
        },
        error => { this.errorMsg = error.success }
    )
  
  }

  submit(){

  	window.open('https://rzp.io/l/oAVXVON', '_system');
  }

  async viewmore(id) {
    this._medicalcamp.plandetails(id)
      .subscribe(
        async data => { 
          this.tpskelton = false;
          if (data['success'] == '1') {
            const alert = await this.alertController.create({
              cssClass: 'my-custom-class',
              mode:'ios',
              message: data['data']['plandetails'][0]['pa_plan_details'],
              buttons: ['OK']
            });
        
            await alert.present();  
          } else {
          }
        },
        error => { this.errorMsg = error.success }
    )
    
  }

}
