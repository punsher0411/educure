import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PlanslistPage } from './planslist.page';

describe('PlanslistPage', () => {
  let component: PlanslistPage;
  let fixture: ComponentFixture<PlanslistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanslistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PlanslistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
