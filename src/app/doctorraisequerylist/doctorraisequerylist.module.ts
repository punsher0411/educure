import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DoctorraisequerylistPageRoutingModule } from './doctorraisequerylist-routing.module';

import { DoctorraisequerylistPage } from './doctorraisequerylist.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DoctorraisequerylistPageRoutingModule
  ],
  declarations: [DoctorraisequerylistPage]
})
export class DoctorraisequerylistPageModule {}
