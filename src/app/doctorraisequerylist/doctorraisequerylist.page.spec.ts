import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DoctorraisequerylistPage } from './doctorraisequerylist.page';

describe('DoctorraisequerylistPage', () => {
  let component: DoctorraisequerylistPage;
  let fixture: ComponentFixture<DoctorraisequerylistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorraisequerylistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DoctorraisequerylistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
