import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DoctorraisequerylistPage } from './doctorraisequerylist.page';

const routes: Routes = [
  {
    path: '',
    component: DoctorraisequerylistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DoctorraisequerylistPageRoutingModule {}
