import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { ToastController, MenuController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FormService } from '../api/form/form.service';
@Component({
  selector: 'app-physician',
  templateUrl: './physician.page.html',
  styleUrls: ['./physician.page.scss'],
})
export class PhysicianPage implements OnInit {
  errorMsg: any;
  id: string;
  consoluentid: string;
  firstname: string;
  lastname: string;
  event_id: string;
  studentclass: string;
  studentrollno: string;
  studentdiv: string;
  weightdata: any = "121";
  heightdata: any ="41";
  bmidata: number = 224;
  constructor(private formBuilder: FormBuilder, public toastCtrl: ToastController,
		private router: Router, private storage: Storage,
		private _form: FormService, public menuCtrl: MenuController,private _Activatedroute: ActivatedRoute,public loadingCtrl: LoadingController) { }

    ngOnInit() {
      this.id = this._Activatedroute.snapshot.paramMap.get('id');
      this.consoluentid = this._Activatedroute.snapshot.paramMap.get('consulentid');
      this.firstname = this._Activatedroute.snapshot.paramMap.get('firstname');
      this.lastname = this._Activatedroute.snapshot.paramMap.get('lastname');
      this.event_id = this._Activatedroute.snapshot.paramMap.get('event_id');
      this.studentclass = this._Activatedroute.snapshot.paramMap.get('studentclass');
      this.studentrollno = this._Activatedroute.snapshot.paramMap.get('studentrollno');
      this.studentdiv = this._Activatedroute.snapshot.paramMap.get('studentdiv');
    }
  createRange(number){
    var items: number[] = [];
    for(var i = 1; i <= number; i++){
       items.push(i);
    }
    return items;
  }
  createparticularRange(start,number){
    var items: number[] = [];
    for(var i = start; i <= number; i++){
       items.push(i);
    }
    return items;
  }

  
  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  PhysicianForm = this.formBuilder.group({
    resprate: ['24'],
		height: ['121'],
    weight: ['41'],
		bmi: ['224'],
    temp: ['98'],
		systolic: ['80'],
		diastolic: ['120'],
    pulse: ['72'],
		bloodsugar: ['0'],
    bloodgp: ['NA']
	});

	get resprate() {
		return this.PhysicianForm.get("resprate");
	}
	get height() {
		return this.PhysicianForm.get("height");
	}
	get weight() {
		return this.PhysicianForm.get("weight");
	}
	get bmi() {
		return this.PhysicianForm.get("bmi");
	}
	get temp() {
		return this.PhysicianForm.get("temp");
	}
	get systolic() {
		return this.PhysicianForm.get("systolic");
	}
	get diastolic() {
		return this.PhysicianForm.get("diastolic");
	}
	get pulse() {
		return this.PhysicianForm.get("pulse");
	}
	get bloodsugar() {
		return this.PhysicianForm.get("bloodsugar");
	}
	get bloodgp() {
		return this.PhysicianForm.get("bloodgp");
	}

	public async submit() {
    const loading =  await this.loadingCtrl.create({
			message: 'Please wait...',
			mode:"ios"
		  });
		  await loading.present();
    this._form.vitalphysican(this.PhysicianForm.value,this.id,this.consoluentid,this.event_id)
      .subscribe(
        data => {
          loading.dismiss();
          if (data['success'] == '1') {
            alert("Data Enter Successfully");
            this.router.navigate(['/medicalcamp']);
          } else {
            alert(data['msg']);
            this.PhysicianForm.reset();
          }
        },
        error => { this.errorMsg = error.success }
      )

  }

  getweight($event){
    this.weightdata = $event.target.value;
    let h2 = this.heightdata * this.heightdata;
    let h3 = h2/100;
    this.bmidata = this.weightdata/h3 ;

  }

  getheight($event){
    this.heightdata = $event.target.value;
    let h2 = this.heightdata * this.heightdata;
    let h3 = h2/100;
    this.bmidata = this.weightdata/h3 ;
  }

}
