import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MedicalrecordmemberPage } from './medicalrecordmember.page';

const routes: Routes = [
  {
    path: '',
    component: MedicalrecordmemberPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MedicalrecordmemberPageRoutingModule {}
