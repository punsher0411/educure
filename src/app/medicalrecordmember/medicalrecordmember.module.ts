import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MedicalrecordmemberPageRoutingModule } from './medicalrecordmember-routing.module';

import { MedicalrecordmemberPage } from './medicalrecordmember.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MedicalrecordmemberPageRoutingModule
  ],
  declarations: [MedicalrecordmemberPage]
})
export class MedicalrecordmemberPageModule {}
