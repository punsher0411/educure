import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MedicalrecordmemberPage } from './medicalrecordmember.page';

describe('MedicalrecordmemberPage', () => {
  let component: MedicalrecordmemberPage;
  let fixture: ComponentFixture<MedicalrecordmemberPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalrecordmemberPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MedicalrecordmemberPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
