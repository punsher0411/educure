import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TestingformPage } from './testingform.page';

describe('TestingformPage', () => {
  let component: TestingformPage;
  let fixture: ComponentFixture<TestingformPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestingformPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TestingformPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
