import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { MedicalcampService } from '../api/medicalcamp/medicalcamp.service';
import { ToastController , MenuController, LoadingController} from '@ionic/angular';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
  selector: 'app-testingform',
  templateUrl: './testingform.page.html',
  styleUrls: ['./testingform.page.scss'],
})
export class TestingformPage implements OnInit {
  student_id: string;
  specilitycode: string;
  errorMsg: any;
  questionlistdata: any;
  answerlistdata: any;
  EyeForm: any = FormGroup;


  constructor(
		private _medicalcamp: MedicalcampService,private formBuilder: FormBuilder, public toastCtrl: ToastController,
    private router: Router, private storage: Storage, public menuCtrl: MenuController, public loadingCtrl: LoadingController, private _Activatedroute: ActivatedRoute) { 
    this.student_id = this._Activatedroute.snapshot.paramMap.get('id');
    console.log(this.student_id);
    this.specilitycode = this._Activatedroute.snapshot.paramMap.get('specilitycode');
    this.questionanswerlist();
    console.log(this.questionlistdata);
      
        
    }

  ngOnInit() {
    
    
     
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    //this.MemberForm.reset();
    
    
    
  }
  

  async questionanswerlist() {
    const loading =  await this.loadingCtrl.create({
      message: 'Please wait...',
      mode:"ios"
    });
    await loading.present();
    
    this._medicalcamp.questionanswerlist(this.student_id).subscribe(
      data => {
        loading.dismiss();
        console.log(data);
         if (data['success'] == '1') {
          this.questionlistdata = data['data']['questions'];
          this.answerlistdata = data['data']['answers'];
          let group = {};
      this.questionlistdata.forEach(question => {
        group[question.que_code] = new FormControl('');
      });
      this.EyeForm = new FormGroup(group);
         }
         else{
          
         }
      },
      error => { this.errorMsg = error.success }
    )
  
  }

}
