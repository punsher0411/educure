import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestingformPageRoutingModule } from './testingform-routing.module';

import { TestingformPage } from './testingform.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TestingformPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [TestingformPage]
})
export class TestingformPageModule {}
