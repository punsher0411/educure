import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestingformPage } from './testingform.page';

const routes: Routes = [
  {
    path: '',
    component: TestingformPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestingformPageRoutingModule {}
