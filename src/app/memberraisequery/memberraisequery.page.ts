import { Component, OnInit } from '@angular/core';
import { DoctorregisterService } from '../api/doctorregister/doctorregister.service';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { ToastController, MenuController, LoadingController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FormService } from '../api/form/form.service';

@Component({
  selector: 'app-memberraisequery',
  templateUrl: './memberraisequery.page.html',
  styleUrls: ['./memberraisequery.page.scss'],
})
export class MemberraisequeryPage implements OnInit {
  errorMsg: any;
 
  constructor(private _doctorregister: DoctorregisterService,private formBuilder: FormBuilder, public toastCtrl: ToastController,
		private router: Router, private storage: Storage,
		private _form: FormService, public menuCtrl: MenuController,private _Activatedroute: ActivatedRoute,public loadingCtrl: LoadingController) { }

  ngOnInit() {
  }

  QueryregisterForm = this.formBuilder.group({
    name: ['', [Validators.required]],
    email: ['', [Validators.required]],
    mobilenumber: ['', [Validators.required]],
    Description: ['', [Validators.required]]
  
  });
  
  get name() {
    return this.QueryregisterForm.get("name");
  }
  get email() {
    return this.QueryregisterForm.get("email");
  }
  get mobilenumber() {
    return this.QueryregisterForm.get("mobilenumber");
  }
  get Description() {
    return this.QueryregisterForm.get("Description");
  }

  public errorMessages = {
    name: [
      { type: 'required', message: 'Name is required' },
      { type: 'maxlength', message: 'Name cant be longer than 10 characters' }
    ],
    email: [
      { type: 'required', message: 'email is required' }
    ]
    ,
    mobilenumber: [
      { type: 'required', message: 'mobilenumber is required' }
    ]
    ,
    Description: [
      { type: 'required', message: 'Description is required' }
    ]
  };


  public async submit() {
    const loading =  await this.loadingCtrl.create({
      message: 'Please wait...',
      mode:"ios"
      });
      await loading.present();
      this._doctorregister.memberegister(this.QueryregisterForm.value)
      .subscribe(
        data => {
         loading.dismiss();
          if (data['success'] == '1') {
            alert("Data Enter Successfully");
          } else {
            alert(data['msg']);
            
          }
          this.QueryregisterForm.reset();
        },
        error => { this.errorMsg = error.success }
      )
    }


}
