import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MemberraisequeryPage } from './memberraisequery.page';

describe('MemberraisequeryPage', () => {
  let component: MemberraisequeryPage;
  let fixture: ComponentFixture<MemberraisequeryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberraisequeryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MemberraisequeryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
