import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MemberraisequeryPageRoutingModule } from './memberraisequery-routing.module';

import { MemberraisequeryPage } from './memberraisequery.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MemberraisequeryPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [MemberraisequeryPage]
})
export class MemberraisequeryPageModule {}
