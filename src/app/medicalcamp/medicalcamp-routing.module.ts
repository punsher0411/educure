import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MedicalcampPage } from './medicalcamp.page';

const routes: Routes = [
  {
    path: '',
    component: MedicalcampPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MedicalcampPageRoutingModule {}
