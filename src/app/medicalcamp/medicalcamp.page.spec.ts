import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MedicalcampPage } from './medicalcamp.page';

describe('MedicalcampPage', () => {
  let component: MedicalcampPage;
  let fixture: ComponentFixture<MedicalcampPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalcampPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MedicalcampPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
