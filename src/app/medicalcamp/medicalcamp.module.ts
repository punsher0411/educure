import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MedicalcampPageRoutingModule } from './medicalcamp-routing.module';

import { MedicalcampPage } from './medicalcamp.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    MedicalcampPageRoutingModule
  ],
  declarations: [MedicalcampPage]
})
export class MedicalcampPageModule {}
