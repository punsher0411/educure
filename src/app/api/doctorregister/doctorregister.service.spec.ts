import { TestBed } from '@angular/core/testing';

import { DoctorregisterService } from './doctorregister.service';

describe('DoctorregisterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DoctorregisterService = TestBed.get(DoctorregisterService);
    expect(service).toBeTruthy();
  });
});
