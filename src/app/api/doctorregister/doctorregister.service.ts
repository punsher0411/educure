import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { PathService } from '../path/path.service';

@Injectable({
  providedIn: 'root'
})
export class DoctorregisterService {
  _specilitylist = this._url.serverurl + 'specility';
  _qualificationlist = this._url.serverurl + 'qualificationlist';
  _citylist = this._url.serverurl + 'city';
  _servicelist = this._url.serverurl + 'service';
  _pincode = this._url.serverurl + 'pincode';
  _doctorform = this._url.serverurl + 'doctorform';
  _memberform = this._url.serverurl + 'memberform';
  httpOptions: any;
  constructor(private _http: HttpClient, private _url: PathService) { }
  getHearder() {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    this.httpOptions = { headers: headers };
  }
  specilitylist() {
    let formData: FormData = new FormData(); 
    return this._http.post<any>(this._specilitylist, formData)
      .pipe(catchError(this.errorHandler))
  }
  qualificationlist(treatment_mode) {
    let formData: FormData = new FormData(); 
    formData.append('treatment_mode', treatment_mode); 
    return this._http.post<any>(this._qualificationlist, formData)
      .pipe(catchError(this.errorHandler))
  }
  citylist() {
    let formData: FormData = new FormData(); 
    return this._http.post<any>(this._citylist, formData)
      .pipe(catchError(this.errorHandler))
  }
  servicelist() {
    let formData: FormData = new FormData(); 
    return this._http.post<any>(this._servicelist, formData)
      .pipe(catchError(this.errorHandler))
  }
  pincodelist(cityname) {
    let formData: FormData = new FormData(); 
    formData.append('city_name', cityname); 
    return this._http.post<any>(this._pincode, formData)
      .pipe(catchError(this.errorHandler))
  }

  doctorregister(doctorData,DPF,DPFblob,DPFname,latt,longg) {
    let formData: FormData = new FormData(); 
   formData.append('name', doctorData.name); 
   formData.append('age', doctorData.age); 
   formData.append('gender', doctorData.gender); 
   formData.append('speciality', doctorData.speciality); 
   formData.append('city', doctorData.city); 
   formData.append('pincode', doctorData.pincode); 
   formData.append('mobile1', doctorData.mobile1); 
   formData.append('mobile2', doctorData.mobile2); 
   formData.append('Email', doctorData.Email); 
   formData.append('experience', doctorData.experience); 
   formData.append('qualification', doctorData.qualification); 
   formData.append('clinicname', doctorData.clinicname); 
   formData.append('availablemulti', doctorData.availablemulti); 
   formData.append('bankholdername', doctorData.bankholdername); 
   formData.append('accountnumber', doctorData.accountnumber); 
   formData.append('ifsccode', doctorData.ifsccode); 
   formData.append('upipin', doctorData.upipin); 
   formData.append('remark', doctorData.remark); 
   formData.append('image',DPFblob, DPFname); 
   formData.append('image',DPFblob, DPFname); 
    formData.append('image', DPFblob, DPFname); 
    formData.append('latt',latt); 
    formData.append('longg',longg);
   return this._http.post<any>(this._doctorform, formData)
      .pipe(catchError(this.errorHandler))
  }

  doctorregisterwithoutimage(doctorData,latt,longg) {
    let formData: FormData = new FormData(); 
   formData.append('name', doctorData.name); 
   formData.append('age', doctorData.age); 
   formData.append('gender', doctorData.gender); 
   formData.append('speciality', doctorData.speciality); 
   formData.append('city', doctorData.city); 
   formData.append('pincode', doctorData.pincode); 
   formData.append('mobile1', doctorData.mobile1); 
   formData.append('mobile2', doctorData.mobile2); 
   formData.append('Email', doctorData.Email); 
   formData.append('experience', doctorData.experience); 
   formData.append('qualification', doctorData.qualification); 
   formData.append('clinicname', doctorData.clinicname); 
   formData.append('availablemulti', doctorData.availablemulti); 
   formData.append('bankholdername', doctorData.bankholdername); 
   formData.append('accountnumber', doctorData.accountnumber); 
   formData.append('ifsccode', doctorData.ifsccode); 
   formData.append('upipin', doctorData.upipin); 
    formData.append('remark', doctorData.remark); 
    formData.append('latt',latt); 
   formData.append('longg',longg); 
   return this._http.post<any>(this._doctorform, formData)
      .pipe(catchError(this.errorHandler))
  }


  memberegister(doctorData) {
    let formData: FormData = new FormData(); 
   formData.append('name', doctorData.name); 
   formData.append('age', doctorData.age); 
   formData.append('gender', doctorData.gender); 
   formData.append('class', doctorData.class); 
   formData.append('city', doctorData.city); 
   formData.append('parentname', doctorData.parentname); 
   formData.append('schoolname', doctorData.schoolname); 
   return this._http.post<any>(this._memberform, formData)
      .pipe(catchError(this.errorHandler))
  }

 

  errorHandler(error: HttpErrorResponse) {
    return throwError(error);
  }
}
