import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { PathService } from '../path/path.service';
@Injectable({
  providedIn: 'root'
})
export class MedicalcampService {
  _schoollist = this._url.serverurl + 'schoollist';
  _quesanswer = this._url.serverurl + 'quesanswer';
  _studentlist = this._url.serverurl + 'studentlist';
  _dentalmember = this._url.serverurl + 'dentalmember';
  _vitalmember = this._url.serverurl + 'vitalmember';
  _eyemember = this._url.serverurl + 'eyemember';
  _quesmember = this._url.serverurl + 'diemember';
  _dietmember = this._url.serverurl + 'dietmember';
  _mentalhealthmember = this._url.serverurl + 'mentalhealthmember';
  _planlist = this._url.serverurl + 'planlist';
  _plandetails = this._url.serverurl + 'plandetails';
  httpOptions: any;
  constructor(private _http: HttpClient, private _url: PathService) { }
  getHearder() {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    this.httpOptions = { headers: headers };
  }
  schoollist(userData) {
    let formData: FormData = new FormData(); 
   formData.append('provider_no', userData); 
    return this._http.post<any>(this._schoollist, formData)
      .pipe(catchError(this.errorHandler))
  }
  questionanswerlist(userData) {
    let formData: FormData = new FormData(); 
   formData.append('member_code', userData); 
    return this._http.post<any>(this._quesanswer, formData)
      .pipe(catchError(this.errorHandler))
  }
  studentlist(userData,code) {
    var nameArr = userData.split(',');
    let formData: FormData = new FormData(); 
   formData.append('org_id', nameArr[0]); 
   formData.append('fromdate', nameArr[1]); 
   formData.append('todate', nameArr[2]); 
   formData.append('code', code); 
    return this._http.post<any>(this._studentlist, formData)
      .pipe(catchError(this.errorHandler))
  }


  dentalmember(userData) {
    let userid = userData.toString();
    let formData: FormData = new FormData(); 
   formData.append('member_code', userid); 
    return this._http.post<any>(this._dentalmember, formData)
      .pipe(catchError(this.errorHandler))
  }
  vitalmember(userData) {
    let userid = userData.toString();
    let formData: FormData = new FormData(); 
   formData.append('member_code', userid); 
    return this._http.post<any>(this._vitalmember, formData)
      .pipe(catchError(this.errorHandler))
  }
  eyemember(userData) {
    let userid = userData.toString();
    let formData: FormData = new FormData(); 
   formData.append('member_code', userid); 
    return this._http.post<any>(this._eyemember, formData)
      .pipe(catchError(this.errorHandler))
  }
  mentalhealth(userData) {
    let userid = userData.toString();
    let formData: FormData = new FormData(); 
   formData.append('member_code',userid); 
    return this._http.post<any>(this._mentalhealthmember, formData)
      .pipe(catchError(this.errorHandler))
  }
  diet(userData) {
    let userid = userData.toString();
    let formData: FormData = new FormData(); 
   formData.append('member_code', userid); 
    return this._http.post<any>(this._dietmember, formData)
      .pipe(catchError(this.errorHandler))
  }
  questionanswermember(userData, category_id) {
    let userid = userData.toString();
    let formData: FormData = new FormData(); 
   formData.append('member_code', userid); 
   formData.append('type', category_id); 
    return this._http.post<any>(this._quesmember, formData)
      .pipe(catchError(this.errorHandler))
  }

  planlist() {
    
    return this._http.get<any>(this._planlist, this.httpOptions)
      .pipe(catchError(this.errorHandler))
  }
  plandetails(userData) {
    let userid = userData.toString();
    let formData: FormData = new FormData(); 
   formData.append('plan_id', userid); 
    return this._http.post<any>(this._plandetails, formData)
      .pipe(catchError(this.errorHandler))
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error);
  }
}
