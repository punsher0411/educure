import { TestBed } from '@angular/core/testing';

import { MedicalcampService } from './medicalcamp.service';

describe('MedicalcampService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MedicalcampService = TestBed.get(MedicalcampService);
    expect(service).toBeTruthy();
  });
});
