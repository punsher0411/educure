import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
// import { OneSignal } from '@ionic-native/onesignal/ngx';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private storage: Storage) { } //,private oneSignal: OneSignal

  private loginStatus: boolean;
  private userid = 'null';
  private name = 'null';
  private phone = 'null';
  private email = 'null';
  private deviceid = 'null';

  private ext1 = 'null';
  private ext2 = 'null';
  private ext3 = 'null';

  private set = 'null';




  private DataSet() {
    Promise.all([this.storage.get("userid"), this.storage.get("name"),
    this.storage.get("phone"), this.storage.get("email")]).then(values => {
      this.userid = values[0] == undefined ? 'null' : (values[0] != '' ? values[0] : 'null');
      this.name = values[1] == undefined ? 'null' : (values[1] != '' ? values[1] : 'null');
      this.phone = values[2] == undefined ? 'null' : (values[2] != '' ? values[2] : 'null');
      this.email = values[3] == undefined ? 'null' : (values[3] != '' ? values[3] : 'null');
      console.log("All Data SET userid " + this.userid + " name " + this.name)
    });
  }

  private DeviceSet() {
    this.deviceid = '123456789';
    console.log("Set Device ID device3")
    // this.oneSignal.getIds().then(identity => {
    //this.deviceid = identity.userId;
    //  this.deviceid = "device32";
    //});
  }

  set PrepairData(status: string) {
    console.log("Set PrepairData ", status)
    this.DataSet();
    this.set = status;
  }

  set ReSetData(status: string) {
    this.userid = 'null';
    this.name = 'null';
    this.phone = 'null';
    this.email = 'null';
    this.set = status;
  }

  set isDeviceid(status: string) {
    this.DeviceSet();
    console.log("Set Device ID ", status)
  }

  get isUserid(): string {
    if (this.userid !== 'null') { return this.userid; } else { this.DataSet(); return '5e00d0a0000ff000a00000f0'; }
  }
  get isName(): string {
    if (this.name !== 'null') { return this.name; } else { this.DataSet(); return 'No Data'; }
  }
  get isEmail(): string {
    if (this.email !== 'null') { return this.email; } else { this.DataSet(); return 'No Data'; }
  }
  get isPhone(): string {
    if (this.phone !== 'null') { return this.phone; } else { this.DataSet(); return 'No Data'; }
  }
  get isDeviceid(): string {
    if (this.deviceid !== 'null') { return this.deviceid; } else { this.DeviceSet(); return 'No Data'; }
  }

  // set isToken(status:string) {
  //     this.token = status;

  //     localStorage.setItem('token', status);
  //     }

  //==============================================================================================
}
