import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { PathService } from '../path/path.service';
import { AuthService } from '../login/auth.service';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  _login = this._url.serverurl + 'signin';
  _loginmember = this._url.serverurl + 'signinmember';
  httpOptions: any;
  constructor(private _http: HttpClient, private _url: PathService, private _auth: AuthService) { }
  getHearder() {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    this.httpOptions = { headers: headers };
  }
  login(userData, role) {
    if (role == 'member') {
      let formData: FormData = new FormData(); 
   formData.append('mobile', userData.mobile); 
    return this._http.post<any>(this._loginmember, userData)
      .pipe(catchError(this.errorHandler))
    }
    else {
      let formData: FormData = new FormData(); 
   formData.append('mobile', userData.mobile); 
    return this._http.post<any>(this._login, userData)
      .pipe(catchError(this.errorHandler))
    }
    
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error);
  }
}
