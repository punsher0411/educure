import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError, Observable ,forkJoin} from 'rxjs';
import { PathService } from '../path/path.service';
import { AuthService } from '../login/auth.service';

@Injectable({
  providedIn: 'root'
})
export class FormService {
  _pyshician = this._url.serverurl + 'vitalphysicanform';
  _dentalform = this._url.serverurl + 'dentalform';
  _dentalformimg = this._url.serverurl + 'uploadimage';
  _eyeform = this._url.serverurl + 'eyeform';
  _dietform = this._url.serverurl + 'dietform';
  _mentalform = this._url.serverurl + 'mentalform';
  _dietmealform = this._url.serverurl + 'dietmealform';
  httpOptions: any;
  constructor(private _http: HttpClient, private _url: PathService, private _auth: AuthService) { }
  getHearder() {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    this.httpOptions = { headers: headers };
  }
  vitalphysican(userData,id,consullent_id,event_id) {
    let formData: FormData = new FormData(); 
   formData.append('height', userData.height); 
   formData.append('weight', userData.weight); 
   formData.append('temp', userData.temp); 
   formData.append('bmi', userData.bmi); 
   formData.append('pulse', userData.pulse); 
   formData.append('bloodsugar', userData.bloodsugar); 
   formData.append('bloodgp', userData.bloodgp); 
   formData.append('resprate', userData.resprate); 
   formData.append('systolic', userData.systolic); 
   formData.append('diastolic', userData.diastolic); 
   formData.append('membercode', id); 
   formData.append('consultant_id', consullent_id); 
   formData.append('event_id', event_id); 
    return this._http.post<any>(this._pyshician, formData)
      .pipe(catchError(this.errorHandler))
  }

  updateLogo(data: FormData) {

    return this._http.post<any>(this._dentalformimg, data)
      .pipe(catchError(this.errorHandler))
  }
  dental(userData,id,consullent_id,event_id) {
    let formData: FormData = new FormData(); 
   formData.append('decayed', userData.decayed); 
   formData.append('missing', userData.missing); 
   formData.append('crown', userData.crown); 
   formData.append('givingteath', userData.givingteath); 
   formData.append('appearance', userData.appearance); 
   formData.append('Stain', userData.Stain); 
   formData.append('Stainsub', userData.Stainsub); 
   formData.append('calculusteath', userData.calculusteath); 
   formData.append('calculusteath2', userData.calculusteath2); 
   formData.append('Flurosis', userData.Flurosis); 
   formData.append('Malocclusion', userData.Malocclusion); 
   formData.append('brushingfrequency', userData.brushingfrequency); 
   formData.append('mouthwash', userData.mouthwash); 
   formData.append('toothtype', userData.toothtype); 
   formData.append('member_code', id); 
   formData.append('consultant_id', consullent_id); 
   formData.append('event_id', event_id);
   return this._http.post<any>(this._dentalform, formData)
      .pipe(catchError(this.errorHandler))
  }
  eyeform(userData,id,consullent_id,event_id) {
    console.log(consullent_id); 
    console.log(id); 
    console.log(event_id); 
    let formData: FormData = new FormData();  
   formData.append('Glasses', userData.Glasses); 
   formData.append('righteyeno', userData.righteyeno); 
   formData.append('righteyeno2', userData.righteyeno2); 
   formData.append('spherical', userData.spherical); 
   formData.append('cylindrical', userData.cylindrical); 
   formData.append('lefteyeno', userData.lefteyeno); 
   formData.append('lefteyeno2', userData.lefteyeno2); 
   formData.append('leftSpherical', userData.leftSpherical); 
   formData.append('leftcylindrical', userData.leftcylindrical); 
   formData.append('righteye', userData.righteye); 
   formData.append('lefteye', userData.lefteye); 
   formData.append('member_code', id); 
   formData.append('consultant_id', consullent_id); 
   formData.append('event_id', event_id);
   formData.append('remark', userData.remark); 
    return this._http.post<any>(this._eyeform, formData)
      .pipe(catchError(this.errorHandler))
  }
  diet(userData,id,quecode,consullent_id,event_id): Observable<any> {
    let formData: FormData = new FormData();  
    formData.append('member_code', id); 
    formData.append('Breakfast', userData.Breakfast); 
    formData.append('lunch', userData.lunch); 
    formData.append('Evening', userData.Evening); 
    formData.append('Dinner', userData.Dinner); 
    let formData1: FormData = new FormData();  
    formData1.append('member_code', id); 
    formData1.append('ansdetailQ120', userData.ansdetailQ120); 
    formData1.append('ansdetailQ121', userData.ansdetailQ121); 
    formData1.append('ansdetailQ122', userData.ansdetailQ122); 
    formData1.append('ansdetailQ123', userData.ansdetailQ123); 
    formData1.append('ansdetailQ124', userData.ansdetailQ124); 
    formData1.append('ansdetailQ125', userData.ansdetailQ125); 
    formData1.append('ansdetailQ126', userData.ansdetailQ126); 
    formData1.append('ansdetailQ127', userData.ansdetailQ127); 
    formData1.append('ansdetailQ128', userData.ansdetailQ128); 
    formData1.append('ansdetailQ129', userData.ansdetailQ129); 
    formData1.append('ansdetailQ30', userData.ansdetailQ30); 
    formData1.append('ansdetailQ31', userData.ansdetailQ31); 
    formData1.append('ansdetailQ225', userData.ansdetailQ225); 
    formData1.append('ansdetailQ90', userData.ansdetailQ90);  
    formData1.append('ansdetailQ89', userData.ansdetailQ89); 
    formData1.append('ansdetailQ400', userData.ansdetailQ400); 
    formData1.append('que_code', quecode); 
    formData1.append('consultant_id', consullent_id); 
   formData1.append('event_id', event_id);
    let response1 =  this._http.post(this._dietmealform,formData);
      let response2 =  this._http.post(this._dietform,formData1);
      return  forkJoin([response1, response2]);
  }

  mentalhealth(userData,id,quecode,consullent_id,event_id): Observable<any> {
    let formData1: FormData = new FormData();  
    formData1.append('member_code', id); 
    formData1.append('ansdetailQ135', userData.ansdetailQ135); 
    formData1.append('ansdetailQ136', userData.ansdetailQ136); 
    formData1.append('ansdetailQ137', userData.ansdetailQ137); 
    formData1.append('ansdetailQ138', userData.ansdetailQ138); 
    formData1.append('ansdetailQ139', userData.ansdetailQ139); 
    formData1.append('ansdetailQ140', userData.ansdetailQ140); 
    formData1.append('ansdetailQ141', userData.ansdetailQ141); 
    formData1.append('ansdetailQ142', userData.ansdetailQ142); 
    formData1.append('ansdetailQ143', userData.ansdetailQ143); 
    formData1.append('ansdetailQ144', userData.ansdetailQ144); 
    formData1.append('que_code', quecode); 
    formData1.append('consultant_id', consullent_id); 
   formData1.append('event_id', event_id);
    return this._http.post<any>(this._mentalform, formData1)
    .pipe(catchError(this.errorHandler))
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error);
  }
}
