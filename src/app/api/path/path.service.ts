import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PathService {
  url: string = 'https://cors-anywhere.herokuapp.com/http://www.planmyhealth.in/planhealthapi/api/';
  constructor() { }
  get serverurl(): string {
    return this.url;
  }
}
