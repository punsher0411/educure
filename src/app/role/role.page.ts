import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import {  Router } from '@angular/router';
@Component({
  selector: 'app-role',
  templateUrl: './role.page.html',
  styleUrls: ['./role.page.scss'],
})
export class RolePage implements OnInit {

  constructor(private storage: Storage,private router: Router) { }

  ngOnInit() {
  }
  roleapp(value) {
    this.storage.set('roleapp', value);
    this.router.navigate(['/login']);
  }
}
