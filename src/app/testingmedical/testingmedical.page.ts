import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { MedicalcampService } from '../api/medicalcamp/medicalcamp.service';
import { ToastController , MenuController, LoadingController} from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-testingmedical',
  templateUrl: './testingmedical.page.html',
  styleUrls: ['./testingmedical.page.scss'],
})
export class TestingmedicalPage implements OnInit {
  errorMsg: any;
  schoollistdata: any;
  schoolliststatus: boolean;
  specilitycode: any;
  getschoolcode: any;
  studentlistdata: any;
  studentid: any;
  consulentid: any;
  studentdataid: any;
  studentfirstname: any;
  studentlastname: any;
  event_id: any;
  studentclass: any;
  studentrollno: any;
  studentdiv: any;
  jsonData: any;
  studentliststatus: boolean;
  submitbutton: boolean;

  constructor(
		private _medicalcamp: MedicalcampService,private formBuilder: FormBuilder, public toastCtrl: ToastController,
		private router: Router, private storage: Storage, public menuCtrl: MenuController,public loadingCtrl: LoadingController) { 
      this.inislizejsondata();
    }

  ngOnInit() {

  }
  FilterJsonData(ev:any){
const val = ev.target.value;
if(val && val.trim() != ''){
  //this.schoollistdatavalue();
this.studentlistdata = this.studentlistdata.filter((item)=>{
  return (item.fname.toLowerCase().indexOf(val.toLowerCase())>-1 || item.mid.toLowerCase().indexOf(val.toLowerCase())>-1);
})
}
  }

  inislizejsondata(){
    this.jsonData=[
      {
        "id":"123",
        "name":"abc",
      },
      {
        "id":"456",
        "name":"test",
      },
      {
        "id":"789",
        "name":"example",
      },
      {
        "id":"021",
        "name":"testing",
      },
    ]
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.MemberForm.reset();
    this.schoollist();
    this.specilatycode();
    
  }

  MemberForm = this.formBuilder.group({
    entityid: ['', [Validators.required]],
		memberid: [''
		]
	});

	get entityid() {
		return this.MemberForm.get("entityid");
	}
	get memberid() {
		return this.MemberForm.get("memberid");
	}

	public errorMessages = {
		entityid: [
			{ type: 'required', message: 'Entity Id is required' }
		],
		memberid: [
			{ type: 'required', message: 'Member Id is required' }
		]
	};
  specilatycode(){
    this.storage.get('speciality_code').then((val) => { 
      this.specilitycode = val;
    });
  }

  async schoollistdatavalue(){
    if(this.getschoolcode != ''){
      const loading =  await this.loadingCtrl.create({
        message: 'Please wait...',
        mode:"ios"
      });
      await loading.present();
      var nameArr = this.getschoolcode.split(',');
      this.consulentid =nameArr[2];
      this.event_id =nameArr[3];
      this.storage.get('speciality_code').then((val) => { 
        this.specilitycode = val;
      this._medicalcamp.studentlist(this.getschoolcode,this.specilitycode).subscribe(
        data => {
          loading.dismiss();
           if (data['success'] == '1') {
            this.studentlistdata = data['data']['schoollist'];
            this.studentliststatus = false;
            this.schoolliststatus = false;
           }
           else{
            this.schoolliststatus = true;
            this.studentliststatus = false;
           }
        },
        error => { this.errorMsg = error.success }
      )
    });    
  }
  }
  async getSubcat($event){
    this.getschoolcode =$event.target.value;
  if(this.getschoolcode != ''){
    const loading =  await this.loadingCtrl.create({
      message: 'Please wait...',
      mode:"ios"
    });
    await loading.present();
    var nameArr = this.getschoolcode.split(',');
    this.consulentid =nameArr[2];
    this.event_id =nameArr[3];
    this.storage.get('speciality_code').then((val) => { 
      this.specilitycode = val;
    this._medicalcamp.studentlist(this.getschoolcode,this.specilitycode).subscribe(
      data => {
        loading.dismiss();
         if (data['success'] == '1') {
          this.studentlistdata = data['data']['schoollist'];
          this.schoolliststatus = false;
          this.studentliststatus = true;
         }
         else{
          this.schoolliststatus = true;
          this.studentliststatus = false;
         }
      },
      error => { this.errorMsg = error.success }
    )
  });    
}
  }

  studentcat(student){
    this.studentid =student.mid;
    this.studentfirstname =student.fname;
    this.studentlastname =student.lname;
    this.studentclass =student.class;
    this.studentrollno =student.roll_no;
    this.studentdiv =student.division;
    this.submitbutton = true;
  }

  async schoollist() {
    const loading =  await this.loadingCtrl.create({
      message: 'Please wait...',
      mode:"ios"
    });
    await loading.present();
    this.storage.get('provider_no').then((val) => {    
    this._medicalcamp.schoollist(val).subscribe(
      data => {
        loading.dismiss();
         if (data['success'] == '1') {
          this.schoollistdata = data['data']['schoollist'];
          this.schoolliststatus = true;
         }
         else{
          this.schoolliststatus = true;
         }
      },
      error => { this.errorMsg = error.success }
    )
  });
  }

}
