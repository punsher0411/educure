import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TestingmedicalPage } from './testingmedical.page';

describe('TestingmedicalPage', () => {
  let component: TestingmedicalPage;
  let fixture: ComponentFixture<TestingmedicalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestingmedicalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TestingmedicalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
