import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestingmedicalPageRoutingModule } from './testingmedical-routing.module';

import { TestingmedicalPage } from './testingmedical.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TestingmedicalPageRoutingModule
  ],
  declarations: [TestingmedicalPage]
})
export class TestingmedicalPageModule {}
