import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestingmedicalPage } from './testingmedical.page';

const routes: Routes = [
  {
    path: '',
    component: TestingmedicalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestingmedicalPageRoutingModule {}
