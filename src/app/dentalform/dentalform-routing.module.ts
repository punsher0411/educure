import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DentalformPage } from './dentalform.page';

const routes: Routes = [
  {
    path: '',
    component: DentalformPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DentalformPageRoutingModule {}
