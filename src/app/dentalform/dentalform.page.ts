import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { ToastController, MenuController, LoadingController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FormService } from '../api/form/form.service';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { ActionSheetController } from '@ionic/angular';
import { FilePath } from '@ionic-native/file-path/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
@Component({
  selector: 'app-dentalform',
  templateUrl: './dentalform.page.html',
  styleUrls: ['./dentalform.page.scss'],
})
export class DentalformPage implements OnInit {
  errorMsg: any;
  id: string;
  firstname: string;
  lastname: string;
  consoluentid: string;
  event_id: string;
  studentclass: string;
  studentrollno: string;
  studentdiv: string;
  imagepathurl: any;
  formdataimage: any;
  imageformdata: any;
  constructor(private formBuilder: FormBuilder, public toastCtrl: ToastController,
		private router: Router, private storage: Storage,
		private _form: FormService, public menuCtrl: MenuController,private _Activatedroute: ActivatedRoute,public loadingCtrl: LoadingController,private file: File, private camera: Camera,
		public actionSheetController: ActionSheetController, private filePath: FilePath, private plt: Platform, private webview: WebView) { }

  ngOnInit() {
    this.id = this._Activatedroute.snapshot.paramMap.get('id');
    this.firstname = this._Activatedroute.snapshot.paramMap.get('firstname');
    this.lastname = this._Activatedroute.snapshot.paramMap.get('lastname');
    this.consoluentid = this._Activatedroute.snapshot.paramMap.get('consulentid');
    this.event_id = this._Activatedroute.snapshot.paramMap.get('event_id');
    this.studentclass = this._Activatedroute.snapshot.paramMap.get('studentclass');
      this.studentrollno = this._Activatedroute.snapshot.paramMap.get('studentrollno');
	  this.studentdiv = this._Activatedroute.snapshot.paramMap.get('studentdiv');
  }
  createRange(number){
    var items: number[] = [];
    for(var i = 1; i <= number; i++){
       items.push(i);
    }
    return items;
  }
  createparticularRange(start,number){
    var items: number[] = [];
    for(var i = start; i <= number; i++){
       items.push(i);
    }
    return items;
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  DentalForm = this.formBuilder.group({
    decayed: ['0'],
		missing: ['0'],
    crown: ['0'],
		givingteath: ['No'],
    appearance: ['No'],
		Stain: ['Absent'],
    Stainsub: ['GradeIII'],
		calculusteath: ['Absent'],
    calculusteath2: ['GradeIII'],
		Flurosis: ['No'],
    Malocclusion: ['No'],
    brushingfrequency: ['1'],
    mouthwash: ['No'],
    toothtype: ['Normal']
	});

	get decayed() {
		return this.DentalForm.get("decayed");
	}
	get missing() {
		return this.DentalForm.get("missing");
	}
	get crown() {
		return this.DentalForm.get("crown");
	}
	get givingteath() {
		return this.DentalForm.get("givingteath");
	}
	get appearance() {
		return this.DentalForm.get("appearance");
	}
	get Stain() {
		return this.DentalForm.get("Stain");
	}
	get Stainsub() {
		return this.DentalForm.get("Stainsub");
	}
	get calculusteath() {
		return this.DentalForm.get("calculusteath");
	}
	get calculusteath2() {
		return this.DentalForm.get("calculusteath2");
	}
	get Flurosis() {
		return this.DentalForm.get("Flurosis");
	}
	get Malocclusion() {
		return this.DentalForm.get("Malocclusion");
	}
	get brushingfrequency() {
		return this.DentalForm.get("brushingfrequency");
	}
	get mouthwash() {
		return this.DentalForm.get("mouthwash");
	}
	get toothtype() {
		return this.DentalForm.get("toothtype");
	}


  async selectImage() {

		const actionSheet = await this.actionSheetController.create({
			header: "Select Image source",
			buttons: [{
				text: 'Load from Library',
				handler: () => {
					this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
				}
			},
			{
				text: 'Use Camera',
				handler: () => {
					this.pickImage(this.camera.PictureSourceType.CAMERA);
				}
			},
			{
				text: 'Cancel',
				role: 'cancel'
			}
			]
		});
		await actionSheet.present();
  }

  pickImage(sourceType) {
		const options: CameraOptions = {
			quality: 50,
			sourceType: sourceType,
			destinationType: this.camera.DestinationType.FILE_URI,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE,
			correctOrientation: true
		}
		this.camera.getPicture(options).then((imagePath) => {
      this.imageformdata = imagePath;
      this.startUpload(imagePath);
      if (this.plt.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
            .then(filePath => {
                let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            });
    } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    }
		}, (err) => {
			alert("Error in reading file!!!")
		});
  }

  createFileName() {
      var d = new Date(),
          n = d.getTime(),
          newFileName = n + ".jpg";
      return newFileName;
  }
   
  copyFileToLocalDir(namePath, currentName, newFileName) {
      this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
        let filePath = this.file.dataDirectory + newFileName;
        this.imagepathurl = this.pathForImage(filePath);
      }, error => {
        
      });
  }

  pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      let converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }
  
  startUpload(imgEntry) {
		this.file.resolveLocalFilesystemUrl(imgEntry)
			.then(entry => {
				(<FileEntry>entry).file(file => this.readFile(file))
			})
			.catch(err => {
				alert('Error while reading file.');
			});
	}

	readFile(file: any) {
		const reader = new FileReader();
		reader.onload = () => {
			const formData = new FormData();
			const imgBlob = new Blob([reader.result], {
				type: file.type
			});
			formData.append('img', imgBlob, file.name);
			formData.append('event_id', this.event_id);
			formData.append('membercode', this.id);
      this.setLogoimg(formData);
			

		};
		reader.readAsArrayBuffer(file);
  }
  
  async setLogoimg(img: FormData) {
    const loading =  await this.loadingCtrl.create({
			message: 'Please wait...',
			mode:"ios"
		  });
      await loading.present();
    this._form.updateLogo(img)
    .subscribe(
      data => {
        loading.dismiss();
        if (data['status'] !== undefined && data['status'] === true) {
          
        } else {
          
        }
      },
      error => { this.errorMsg = error.success }
    )


  }
  

	public async submit() {
    const loading =  await this.loadingCtrl.create({
			message: 'Please wait...',
			mode:"ios"
		  });
      await loading.present();
    this._form.dental(this.DentalForm.value,this.id,this.consoluentid,this.event_id)
      .subscribe(
        data => {
          loading.dismiss();
          if (data[0]['success'] == '1') {
            alert("Data Enter Successfully");
            this.router.navigate(['/medicalcamp']);
          } else {
            alert(data['msg']);
            this.DentalForm.reset();
          }
        },
        error => { this.errorMsg = error.success }
      )

  }

}
