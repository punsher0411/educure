import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DentalformPage } from './dentalform.page';

describe('DentalformPage', () => {
  let component: DentalformPage;
  let fixture: ComponentFixture<DentalformPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DentalformPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DentalformPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
