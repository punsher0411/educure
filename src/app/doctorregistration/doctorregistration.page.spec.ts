import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DoctorregistrationPage } from './doctorregistration.page';

describe('DoctorregistrationPage', () => {
  let component: DoctorregistrationPage;
  let fixture: ComponentFixture<DoctorregistrationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorregistrationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DoctorregistrationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
