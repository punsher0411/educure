import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { ToastController, MenuController, LoadingController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FormService } from '../api/form/form.service';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { ActionSheetController } from '@ionic/angular';
import { FilePath } from '@ionic-native/file-path/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { DoctorregisterService } from '../api/doctorregister/doctorregister.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
@Component({
  selector: 'app-doctorregistration',
  templateUrl: './doctorregistration.page.html',
  styleUrls: ['./doctorregistration.page.scss'],
})
export class DoctorregistrationPage implements OnInit {
  errorMsg: any;
  imagepathurl: string;
  imageformdata: any;
  specilitylistdata: any;
  specilityliststatus: boolean;
  citylistdata: any;
  citylistliststatus: boolean;
  gettreatmentcode: any;
  qualificationlistdata: any;
  qualificationliststatus: boolean;
  getcityname: any;
  pincodelistdata: any;
  pincodeliststatus: boolean;
  cityliststatus: boolean;
  servicelistdata: any;
  imagebase6: string;
  DPF: any ;
  DPFblob: any ;
  DPFname: any;
  locationCoords: any;
  locationvalue: any;
  constructor(private _doctorregister: DoctorregisterService,private formBuilder: FormBuilder, public toastCtrl: ToastController,
		private router: Router, private storage: Storage,
		private _form: FormService, public menuCtrl: MenuController,private _Activatedroute: ActivatedRoute,public loadingCtrl: LoadingController,private file: File, private camera: Camera,
    public actionSheetController: ActionSheetController, private filePath: FilePath, private plt: Platform, private webview: WebView, private androidPermissions: AndroidPermissions, private geolocation: Geolocation, private locationAccuracy: LocationAccuracy) { 
      this.locationCoords = {
        latitude: "",
        longitude: "",
        accuracy: ""
      }
    }

  ngOnInit() {
    
  }




ionViewWillEnter() {
  this.menuCtrl.enable(true);
  this.specilitylist();
  this.citylist();
  this.servicelist();
  this.checkGPSPermission();
}


async specilitylist() {
  const loading =  await this.loadingCtrl.create({
    message: 'Please wait...',
    mode:"ios"
  });
  await loading.present();   
  this._doctorregister.specilitylist().subscribe(
    data => {
      loading.dismiss();
       if (data['success'] == '1') {
        this.specilitylistdata = data['data']['specilitylist'];
        this.specilityliststatus = true;
       }
       else{
        this.specilityliststatus = true;
       }
    },
    error => { this.errorMsg = error.success }
  )
}

async citylist() {
  const loading =  await this.loadingCtrl.create({
    message: 'Please wait...',
    mode:"ios"
  });
  await loading.present();   
  this._doctorregister.citylist().subscribe(
    data => {
      loading.dismiss();
       if (data['success'] == '1') {
        this.citylistdata = data['data']['citylist'];
        this.citylistliststatus = true;
       }
       else{
        this.citylistliststatus = true;
       }
    },
    error => { this.errorMsg = error.success }
  )
}
async servicelist() {
  const loading =  await this.loadingCtrl.create({
    message: 'Please wait...',
    mode:"ios"
  });
  await loading.present();   
  this._doctorregister.servicelist().subscribe(
    data => {
      loading.dismiss();
       if (data['success'] == '1') {
        this.servicelistdata = data['data']['servicelist'];
       
       }
       else{
    
       }
    },
    error => { this.errorMsg = error.success }
  )
}


  async getqualification($event){
    this.gettreatmentcode =$event.target.value;
  if(this.gettreatmentcode != ''){
    const loading =  await this.loadingCtrl.create({
      message: 'Please wait...',
      mode:"ios"
    });
    await loading.present();
    this._doctorregister.qualificationlist(this.gettreatmentcode).subscribe(
      data => {
        loading.dismiss();
         if (data['success'] == '1') {
          this.qualificationlistdata = data['data']['qualificationlist'];
          this.specilityliststatus = false;
          this.qualificationliststatus = true;
         }
         else{
           alert("Doesn't have qualification please select other specility");
          this.specilityliststatus = true;
          this.qualificationliststatus = false;
         }
      },
      error => { this.errorMsg = error.success }
    )
}
  }

  async getpincode(cityname){
    this.getcityname = cityname;
  if(this.getcityname != ''){
    const loading =  await this.loadingCtrl.create({
      message: 'Please wait...',
      mode:"ios"
    });
    await loading.present();
    this._doctorregister.pincodelist(this.getcityname).subscribe(
      data => {
        this.cityliststatus = false;
        loading.dismiss();
         if (data['success'] == '1') {
          this.pincodelistdata = data['data']['pincodelist'];
          this.citylistliststatus = false;
          this.pincodeliststatus = true;
         }
         else{
          this.citylistliststatus = true;
          this.pincodeliststatus = false;
         }
      },
      error => { this.errorMsg = error.success }
    )
}
  }

DoctorregisterForm = this.formBuilder.group({
  name: [''],
  age: [''],
  gender: [''],
  speciality: [''],
  city: [''],
  pincode: [''],
  mobile1: [''],
  mobile2: [''],
  Email: [''],
  experience: [''],
  qualification: [''],
  clinicname: [''],
  availablemulti: [''],
  bankholdername: [''],
  accountnumber: [''],
  ifsccode: [''],
  upipin: [''],
  remark: ['']

});

get name() {
  return this.DoctorregisterForm.get("name");
}
get age() {
  return this.DoctorregisterForm.get("age");
}
get gender() {
  return this.DoctorregisterForm.get("gender");
}
get speciality() {
  return this.DoctorregisterForm.get("speciality");
}
get city() {
  return this.DoctorregisterForm.get("city");
}
get pincode() {
  return this.DoctorregisterForm.get("pincode");
}
get mobile1() {
  return this.DoctorregisterForm.get("mobile1");
}
get mobile2() {
  return this.DoctorregisterForm.get("mobile2");
}
get Email() {
  return this.DoctorregisterForm.get("Email");
}
get experience() {
  return this.DoctorregisterForm.get("experience");
}
get qualification() {
  return this.DoctorregisterForm.get("qualification");
}
get clinicname() {
  return this.DoctorregisterForm.get("clinicname");
}
get availablemulti() {
  return this.DoctorregisterForm.get("availablemulti");
}
get bankholdername() {
  return this.DoctorregisterForm.get("bankholdername");
}
get accountnumber() {
  return this.DoctorregisterForm.get("accountnumber");
}
get ifsccode() {
  return this.DoctorregisterForm.get("ifsccode");
}
get upipin() {
  return this.DoctorregisterForm.get("upipin");
}
get remark() {
  return this.DoctorregisterForm.get("remark");
}

public errorMessages = {
  name: [
    { type: 'required', message: 'Name is required' },
    { type: 'maxlength', message: 'Name cant be longer than 10 characters' }
  ],
  age: [
    { type: 'required', message: 'Age is required' }
  ]
  ,
  gender: [
    { type: 'required', message: 'Gender is required' }
  ]
  ,
  speciality: [
    { type: 'required', message: 'Speciality is required' }
  ]
  ,
  city: [
    { type: 'required', message: 'City is required' }
  ]
  ,
  pincode: [
    { type: 'required', message: 'Pincode is required' }
  ]
  ,
  mobile1: [
    { type: 'required', message: 'Mobile1 is required' }
  ]
  ,
  mobile2: [
    { type: 'required', message: 'Mobile2 is required' }
  ]
  ,
  Email: [
    { type: 'required', message: 'Email is required' }
  ]
  ,
  experience: [
    { type: 'required', message: 'Experience is required' }
  ]
  ,
  qualification: [
    { type: 'required', message: 'Qualification is required' }
  ]
  ,
  clinicname: [
    { type: 'required', message: 'Clinic Name is required' }
  ]
  ,
  availablemulti: [
    { type: 'required', message: 'Available is required' }
  ]
  ,
  bankholdername: [
    { type: 'required', message: 'BankName is required' }
  ]
  ,
  accountnumber: [
    { type: 'required', message: 'AccountNumber is required' }
  ]
  ,
  ifsccode: [
    { type: 'required', message: 'Ifsc Code is required' }
  ]
  ,
  upipin: [
    { type: 'required', message: 'UpiPin is required' }
  ]
};

FilterJsonData(ev:any){
  this.cityliststatus = true;
  const val = ev.target.value;
  if(val && val.trim() != ''){
  this.citylistdata = this.citylistdata.filter((item)=>{
    return (item.city_name.toLowerCase().indexOf(val.toLowerCase())>-1);
  })
  }
    }

  

async selectImage() {

  const actionSheet = await this.actionSheetController.create({
    header: "Select Image source",
    buttons: [{
      text: 'Load from Library',
      handler: () => {
        this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
      }
    },
    {
      text: 'Use Camera',
      handler: () => {
        this.pickImage(this.camera.PictureSourceType.CAMERA);
      }
    },
    {
      text: 'Cancel',
      role: 'cancel'
    }
    ]
  });
  await actionSheet.present();
}

pickImage(sourceType) {
  const options: CameraOptions = {
    quality: 50,
    sourceType: sourceType,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    correctOrientation: true
  }
  this.camera.getPicture(options).then((imagePath) => {
    this.imageformdata = imagePath;
    //this.imagebase6 = 'data:image/png;base64,' + imagePath;
    this.startUpload(imagePath);
    if (this.plt.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
      this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
              let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
              let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
              this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
  } else {
      var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
      var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
      this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
  }
  }, (err) => {
    alert("Error in reading file!!!")
  });
}

createFileName() {
    var d = new Date(),
        n = d.getTime(),
        newFileName = n + ".jpg";
    return newFileName;
}
 
copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      let filePath = this.file.dataDirectory + newFileName;
      this.imagepathurl = this.pathForImage(filePath);
    }, error => {
      
    });
}

pathForImage(img) {
  if (img === null) {
    return '';
  } else {
    let converted = this.webview.convertFileSrc(img);
    return converted;
  }
}

startUpload(imgEntry) {
  this.file.resolveLocalFilesystemUrl(imgEntry)
    .then(entry => {
      (<FileEntry>entry).file(file => this.readFile(file))
    })
    .catch(err => {
      alert('Error while reading file.');
    });
}

 readFile(file: any) {
  const reader = new FileReader();
  reader.onload = () => {
   
    const imgBlob = new Blob([reader.result], {
      type: file.type
    });
    this.DPF = file;
      this.DPFblob = imgBlob;
      this.DPFname = file.name;
    
   
    

  };
  reader.readAsArrayBuffer(file);
}

// async setLogoimg(img: FormData) {
//   const loading =  await this.loadingCtrl.create({
//     message: 'Please wait...',
//     mode:"ios"
//     });
//     await loading.present();
//   this._form.updateLogo(img)
//   .subscribe(
//     data => {
//       loading.dismiss();
//       if (data['status'] !== undefined && data['status'] === true) {
        
//       } else {
        
//       }
//     },
//     error => { this.errorMsg = error.success }
//   )


// }



public async submit() {
  const loading =  await this.loadingCtrl.create({
    message: 'Please wait...',
    mode:"ios"
    });
    await loading.present();
  if (this.DPF != undefined) {
    this._doctorregister.doctorregister(this.DoctorregisterForm.value,this.DPF,this.DPFblob,this.DPFname,this.locationCoords.latitude,this.locationCoords.longitude)
    .subscribe(
      data => {
       loading.dismiss();
        if (data['success'] == '1') {
          alert("Data Enter Successfully");
        } else {
          alert(data['msg']);
          
        }
        this.DoctorregisterForm.reset();
      },
      error => { this.errorMsg = error.success }
    )
  }
  else {
    this._doctorregister.doctorregisterwithoutimage(this.DoctorregisterForm.value,this.locationCoords.latitude,this.locationCoords.longitude)
    .subscribe(
      data => {
       loading.dismiss();
        if (data['success'] == '1') {
          alert("Data Enter Successfully");
        } else {
          alert(data['msg']);
        
        }
          this.DoctorregisterForm.reset();
      },
      error => { this.errorMsg = error.success }
    )
  }
  

}
  
  
  //========================================= Geo Location ============================================

  //Check if application having GPS access permission  
  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {//If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
        } else { //If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => { alert(err); }
    );
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else { //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(() => { // call method to turn on GPS
            this.askToTurnOnGPS();
          },
            error => { //Show alert if user click on 'No Thanks'
              alert('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => { // When GPS Turned ON call method to get Accurate location coordinates
        this.getLocationCoordinates()
      },
      error => alert('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

  // Methos to get device accurate coordinates using device GPS
  getLocationCoordinates() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.locationCoords.latitude = resp.coords.latitude;
      this.locationCoords.longitude = resp.coords.longitude;
      this.locationCoords.accuracy = resp.coords.accuracy;
      console.log(this.locationCoords);
      //this.storage.set('location', JSON.stringify(this.locationCoords));
      this.locationvalue = JSON.stringify(this.locationCoords);
      console.log(this.locationCoords.latitude);
     
    }).catch((error) => {
      alert('Error getting location' + error);
    });
  }

  
  //======================================= Geo Location End ==========================================
   

}
