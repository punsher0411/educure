import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DoctorregistrationPage } from './doctorregistration.page';

const routes: Routes = [
  {
    path: '',
    component: DoctorregistrationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DoctorregistrationPageRoutingModule {}
