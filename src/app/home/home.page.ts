import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  roleapp: any;

  constructor( public menuCtrl: MenuController, private storage: Storage) {}
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.storage.get('roleapp').then((value) => {
      console.log(value,"hi");
      this.roleapp = value;
    });
  }
}
