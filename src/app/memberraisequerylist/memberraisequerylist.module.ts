import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MemberraisequerylistPageRoutingModule } from './memberraisequerylist-routing.module';

import { MemberraisequerylistPage } from './memberraisequerylist.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MemberraisequerylistPageRoutingModule
  ],
  declarations: [MemberraisequerylistPage]
})
export class MemberraisequerylistPageModule {}
