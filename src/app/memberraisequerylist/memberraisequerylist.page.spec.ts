import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MemberraisequerylistPage } from './memberraisequerylist.page';

describe('MemberraisequerylistPage', () => {
  let component: MemberraisequerylistPage;
  let fixture: ComponentFixture<MemberraisequerylistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberraisequerylistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MemberraisequerylistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
