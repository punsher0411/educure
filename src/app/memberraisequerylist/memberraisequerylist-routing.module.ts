import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MemberraisequerylistPage } from './memberraisequerylist.page';

const routes: Routes = [
  {
    path: '',
    component: MemberraisequerylistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MemberraisequerylistPageRoutingModule {}
